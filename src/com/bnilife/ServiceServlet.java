package com.bnilife;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = { "/services" }, name = "services")
public class ServiceServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ServiceServlet() {
	super();
	// TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	HttpSession session = request.getSession();
	String username = (String) session.getAttribute("user_id");  
	
	request.setAttribute("username", username);
	
	RequestDispatcher dispatcher = request.getRequestDispatcher("/mainform/services.jsp");
	dispatcher.forward(request, response);
    }
}
