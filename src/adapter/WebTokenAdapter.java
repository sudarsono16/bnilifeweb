package adapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.CachedRowSet;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import database.QueryExecuteAdapter;

public class WebTokenAdapter {
	public static model.mdlToken GetBearerToken(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpURLConnection conn = null;
		BufferedReader reader = null;
		model.mdlToken returnTokenObject = new model.mdlToken();
		try{
			conn = adapter.MultipartUtility.setRequestPropertyBasic();
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("HTTP GET Request Failed with Error code : "+ conn.getResponseCode());
			}
			
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
			
			String output = null;
			StringBuilder strBuf = new StringBuilder(); 
			while ((output = reader.readLine()) != null) {
				strBuf.append(output);
			}
			
			Gson gson = new Gson();
			model.mdlAPIObjectResult result = gson.fromJson(strBuf.toString(), model.mdlAPIObjectResult.class);
			//model.mdlAPIObjectResult result = new Gson().fromJson(new Gson().toJson(((LinkedTreeMap<String, Object>) result.output_schema)), model.mdlAPIObjectResult.class);
			//model.mdlToken output_schema_keys = (mdlToken) result.output_schema;
			JsonObject jsonObject = gson.toJsonTree(result.output_schema).getAsJsonObject();
			model.mdlToken output_schema_keys = gson.fromJson(jsonObject.toString(), model.mdlToken.class);
			returnTokenObject = output_schema_keys;
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally{
			if(reader!=null){
				try {
					reader.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			if(conn!=null){
				conn.disconnect();
			}
		}
		
		return returnTokenObject;
	}
	
	public static model.mdlKey GetBasicToken(){
		model.mdlKey mdlKey = new model.mdlKey();
		CachedRowSet crs = null;
		try {
			String sql = "SELECT AppName, ClientID, ClientSecret, APIKey, IsActive FROM clientid WHERE AppName = 'TestingApp' ";
			crs = QueryExecuteAdapter.QueryExecute(sql, "GetMenuMemberList");
			while (crs.next()) {
				mdlKey.ClientID = crs.getString("ClientID");
				mdlKey.ClientSecret = crs.getString("ClientSecret");
			}
		} catch (Exception ex) {
			//logger.error("FAILED. Function : GetMenuMemberList, Exception:" + ex.toString(), ex);
		}
		return mdlKey;
	}
}
