package com.bnilife;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = { "/termsandcondition" }, name = "termsandcondition")
public class TermsAndConditionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TermsAndConditionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/mainform/termsandcondition.jsp");
		dispatcher.forward(request, response);
	}
}
