package com.bnilife;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = { "/virtualcards" }, name = "virtualcards")
public class VirtualCardsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public VirtualCardsServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Context context = (Context) new InitialContext().lookup("java:comp/env");
			String image_url = (String) context.lookup("image_url");
			request.setAttribute("image_url", image_url);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/mainform/c-virtualcards.jsp");
			dispatcher.forward(request, response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
