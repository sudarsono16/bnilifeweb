<!DOCTYPE html>
<html lang="en">
<head>
<base href="./">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<meta name="description" content="BNI Life App CMS">
<meta name="author" content="BNI Life x MobileCom">
<title>BNI Life Mobile CMS</title>
<!-- Icons-->
<link href="mainform/css/flag-icon.min.css" rel="stylesheet">
<link href="mainform/vendors/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="mainform/css/simple-line-icons.css" rel="stylesheet">
<!-- Main styles for this application-->
<link href="mainform/css/style.css" rel="stylesheet">
<link href="mainform/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
<link href="mainform/css/custom.css" rel="stylesheet">
</head>
<body class="app flex-row mt-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-5">
				<div class="card-group">
					<div class="card p-4" style="box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.2);">
						<form action="${pageContext.request.contextPath}/login" method="post">
							<div class="card-body justify-content-center">
								<img src="mainform/img/BNI-Life-Logo.png" alt="BNI Life Mobile" class="img-fluid mx-auto d-block mb-5"
									style="width: 10rem;">
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-user"></i>
										</span>
									</div>
									<input class="form-control" name="username" type="text" placeholder="Username">
								</div>
								<div class="input-group mb-4">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-lock"></i>
										</span>
									</div>
									<input class="form-control" name="password" type="password" placeholder="Password">
								</div>
								<div class="row">
									<div class="col-4">
										<button class="btn btn-primary px-4" type="submit" name="btnLogin">Login</button>
									</div>
									<div class="col-8 text-right">
										<button class="btn btn-link px-0" type="button" data-toggle="modal" data-target="#forgotPass">Forgot
											password?</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Forgot Password Modal -->
	<div class="modal fade" id="forgotPass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Forgot Password</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" align="center">
					<p>Please enter your registered e-mail address to reset your password.</p>
					<div class="input-group mb-3" style="width: 80%;">
						<input type="text" class="form-control" placeholder="example@mail.com">
						<div class="input-group-append">
							<button class="btn btn-secondary" type="button" id="button-addon2" onClick="resetOK()" data-dismiss="modal">Reset
								Password</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- CoreUI and necessary plugins-->
	<script src="mainform/plugins/jquery-3.4.1.min.js"></script>
	<script src="mainform/plugins/popper.min.js"></script>
	<script src="mainform/plugins/bootstrap.min.js"></script>
	<script src="mainform/plugins/pace.min.js"></script>
	<script src="mainform/plugins/perfect-scrollbar.min.js"></script>
	<script src="mainform/plugins/coreui.min.js"></script>
	<script src="mainform/plugins/sweetalert2.all.min.js"></script>
	<script>
		function resetOK() {
			swal.fire('Sent', 'Please click the link in your email to continue', 'success')
		}
	</script>
</body>
</html>
