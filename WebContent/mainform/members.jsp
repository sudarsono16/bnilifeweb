<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
		<div class="container-fluid">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-6 col-md-2">
						<div class="card">
							<div class="card-body">
								<div class="text-value">87.500</div>
								<small class="text-muted text-uppercase font-weight-bold">Optima Group Health</small>
								<div class="progress progress-xs mt-3 mb-0">
									<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6 col-md-2">
						<div class="card">
							<div class="card-body">
								<div class="text-value">87.500</div>
								<small class="text-muted text-uppercase font-weight-bold">Optima Group Saving</small>
								<div class="progress progress-xs mt-3 mb-0">
									<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6 col-md-2">
						<div class="card">
							<div class="card-body">
								<div class="text-value">87.500</div>
								<small class="text-muted text-uppercase font-weight-bold">Individual Unit Link</small>
								<div class="progress progress-xs mt-3 mb-0">
									<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6 col-md-2">
						<div class="card">
							<div class="card-body">
								<div class="text-value">87.500</div>
								<small class="text-muted text-uppercase font-weight-bold">Individual Traditional</small>
								<div class="progress progress-xs mt-3 mb-0">
									<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6 col-md-2">
						<div class="card">
							<div class="card-body">
								<div class="text-value">87.500</div>
								<small class="text-muted text-uppercase font-weight-bold">Individual Sales Force</small>
								<div class="progress progress-xs mt-3 mb-0">
									<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6 col-md-2">
						<div class="card">
							<div class="card-body">
								<div class="text-value">87.500</div>
								<small class="text-muted text-uppercase font-weight-bold">Employee Benefit Sales Force</small>
								<div class="progress progress-xs mt-3 mb-0">
									<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"
										aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="search-container row">
					<div class="col-lg-6 col-md-6 col-12"></div>
					<div class="col-lg-3 col-md-3 col-12">
						<div class="searchbar input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fas fa-search"></i></span>
							</div>
							<input type="text" class="form-control" placeholder="Search Member" aria-label="Search Member"
								aria-describedby="basic-addon2">
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-12">
						<div class="searchbar input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><b>Filter</b></span>
							</div>
							<select class="custom-select">
								<option selected>All</option>
								<option value="1">Optima Group Health</option>
								<option value="2">Optima Group Saving</option>
								<option value="3">Individual Unit Link</option>
								<option value="4">Individual Traditional</option>
								<option value="5">Individual Sales Force</option>
								<option value="6">Employee Benefit Sales Force</option>
							</select>
						</div>
					</div>
				</div>
				<!-- SEARCH-CONTAINER -->
			</div>
		</div>
		<div class="container-fluid">
			<div class="animated fadeIn">
				<div class="row pt-3">
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#memberModal">
							<div class="claim-num">florencelit</div>
							<div class="claim-sum">
								<div class="claim-sum-name">
									<i class="fas fa-user-circle"></i>Florence Lit
								</div>
								<div>
									<i class="far fa-calendar-alt"></i>Member since <b>2 Jun 2019</b>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#memberModal">
							<div class="claim-num">florencelit</div>
							<div class="claim-sum">
								<div class="claim-sum-name">
									<i class="fas fa-user-circle"></i>Florence Lit
								</div>
								<div>
									<i class="far fa-calendar-alt"></i>Member since <b>2 Jun 2019</b>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#memberModal">
							<div class="claim-num">florencelit</div>
							<div class="claim-sum">
								<div class="claim-sum-name">
									<i class="fas fa-user-circle"></i>Florence Lit
								</div>
								<div>
									<i class="far fa-calendar-alt"></i>Member since <b>2 Jun 2019</b>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#memberModal">
							<div class="claim-num">florencelit</div>
							<div class="claim-sum">
								<div class="claim-sum-name">
									<i class="fas fa-user-circle"></i>Florence Lit
								</div>
								<div>
									<i class="far fa-calendar-alt"></i>Member since <b>2 Jun 2019</b>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#memberModal">
							<div class="claim-num">florencelit</div>
							<div class="claim-sum">
								<div class="claim-sum-name">
									<i class="fas fa-user-circle"></i>Florence Lit
								</div>
								<div>
									<i class="far fa-calendar-alt"></i>Member since <b>2 Jun 2019</b>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#memberModal">
							<div class="claim-num">florencelit</div>
							<div class="claim-sum">
								<div class="claim-sum-name">
									<i class="fas fa-user-circle"></i>Florence Lit
								</div>
								<div>
									<i class="far fa-calendar-alt"></i>Member since <b>2 Jun 2019</b>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#memberModal">
							<div class="claim-num">florencelit</div>
							<div class="claim-sum">
								<div class="claim-sum-name">
									<i class="fas fa-user-circle"></i>Florence Lit
								</div>
								<div>
									<i class="far fa-calendar-alt"></i>Member since <b>2 Jun 2019</b>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 col-md-4 col-12">
						<div class="claim-card card-shadow" data-toggle="modal" data-target="#memberModal">
							<div class="claim-num">florencelit</div>
							<div class="claim-sum">
								<div class="claim-sum-name">
									<i class="fas fa-user-circle"></i>Florence Lit
								</div>
								<div>
									<i class="far fa-calendar-alt"></i>Member since <b>2 Jun 2019</b>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="gigantic pagination justify-content-center">
							<a href="#" class="first" data-action="first"><i class="fas fa-angle-double-left"></i></a> <a href="#"
								class="previous" data-action="previous"><i class="fas fa-angle-left"></i></a> <input type="text" readonly /> <a
								href="#" class="next" data-action="next"><i class="fas fa-angle-right"></i></a> <a href="#" class="last"
								data-action="last"><i class="fas fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		</main>
		<!-- Modal -->
		<div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title claim-num" id="memberModalLabel">florencelit</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col">
								<div class="modal-row">
									<label>Name</label>
									<div>Florence Lit</div>
								</div>
								<div class="modal-row">
									<label>Email Address</label>
									<div>
										<a href="mailto:florence_lit@mail.com">florence_lit@mail.com</a>
									</div>
								</div>
								<div class="modal-row">
									<label>Phone Number</label>
									<div>
										<a href="tel:+6281234567890">+62 812 3456 7890</a>
									</div>
								</div>
								<div class="modal-row">
									<label>Date of Birth</label>
									<div>30 Jan 1988</div>
								</div>
								<div class="modal-row">
									<button class="btn btn-warning" data-toggle="modal" data-target="#confirmPassword" data-dismiss="modal"
										role="button" aria-expanded="false" aria-controls="claimAttachment">Update Information</button>
								</div>
							</div>
							<div class="col">
								<div class="modal-row">
									<label>Optima Group Health</label>
									<div>1234567890</div>
								</div>
								<div class="modal-row">
									<label>Optima Group Saving</label>
									<div>2345678901</div>
								</div>
								<div class="modal-row">
									<label>Individual Sales Force</label>
									<div>3456789012</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- MODAL -->
		<!-- Confirm Password Modal -->
		<div class="modal fade" id="confirmPassword" tabindex="-1" role="dialog" aria-labelledby="confirmPasswordLabel"
			aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title claim-num" id="confirmPasswordLabel">Password Confirmation</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col">
								<div class="input-group">
									<input type="password" class="form-control" placeholder="Enter Your Password to Continue">
									<div class="input-group-append">
										<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#editInfo" data-dismiss="modal">Confirm</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- MODAL -->
		<!-- Edit Information Modal -->
		<div class="modal fade" id="editInfo" tabindex="-1" role="dialog" aria-labelledby="editInfoLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title claim-num" id="editInfoLabel">Update Member Information</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col">
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">Userame</span>
									</div>
									<input type="text" class="form-control" placeholder="florencelit" aria-label="Username" disabled>
								</div>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">Name</span>
									</div>
									<input type="text" class="form-control" placeholder="Florence Lit" aria-label="Name">
								</div>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">Email Address</span>
									</div>
									<input type="email" class="form-control" placeholder="florence_lit@mail.com" aria-label="Email Address">
								</div>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">Phone Number</span>
									</div>
									<input type="tel" class="form-control" placeholder="+62 812 3456 7890" aria-label="Phone Number">
								</div>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">Date of Birth</span>
									</div>
									<input type="date" class="form-control" placeholder="30 Jan 1988" aria-label="Date of Birth">
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" type="button" onClick="updateOK()" data-dismiss="modal">Save changes</button>
					</div>
				</div>
			</div>
		</div>
		<!-- MODAL -->
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<!-- Plugins and scripts required by this view-->
	<script src="mainform/plugins/Chart.min.js"></script>
	<script src="js/charts.js"></script>
	<script src="mainform/plugins/custom-tooltips.min.js"></script>
	<script src="js/main.js"></script>
	<!-- Pagination -->
	<script src="js/jquery.jqpagination.min.js"></script>
	<script>
		$(document).ready(function() {

			$('.pagination').jqPagination({
			link_string : '/?page={page_number}',
			max_page : 40,
			paged : function(page) {
				$('.log').prepend('<li>Requested page ' + page + '</li>');
			}
			});
			$('.show-log').click(function(event) {
				event.preventDefault();
				$('.log').slideToggle();
			});

		});
	</script>
	<script>
		function updateOK() {
			swal.fire('Success', 'Member Information Has Been Updated', 'success')
		}
	</script>
</body>
</html>
