package com.bnilife;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.core.MediaType;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;

import adapter.HeaderAdapter;
import adapter.TokenWebAdapter;
import helper.DateHelper;

@MultipartConfig
@WebServlet(urlPatterns = { "/APICall" }, name = "APICall")
public class APIServlet extends HttpServlet {
    final static Logger logger = Logger.getLogger(APIServlet.class);
    private static final long serialVersionUID = 1L;
    static Client client = Client.create();
    static Gson gson = new Gson();

    public APIServlet() {
    	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	handleRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	handleRequest(request, response);
    }

	public void handleRequest(HttpServletRequest request, HttpServletResponse response) {
		String authorization = "";
		String jsonIn = "";
		String urlFinal = "";
		String jsonResult = "";
		model.mdlHeader mdlHeader = new model.mdlHeader();
		long startTime = System.currentTimeMillis();
		long stopTime = 0;
		long elapsedTime = 0;
		
		String urlAPI = "";
		String methodAPI = "";
		String contentAPI = "";
		String fileLocation = "";
		String urlAPI_header = "";
		
		boolean isFileExist = false;
		
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if(isMultipart) {
			try {
				String jsonPart = request.getParameter("json");
				
				JSONObject jsonObject = (JSONObject) new JSONParser().parse(jsonPart);
				contentAPI = jsonPart;
				methodAPI = jsonObject.get("method").toString();
				urlAPI = jsonObject.get("APIUrl").toString();
				urlAPI_header = urlAPI;
				
				Part filePart = request.getPart("file");
				if(filePart != null) {
					isFileExist = true;
					String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
					
					int IndexOf = fileName.indexOf(".");
					String domainName = fileName.substring(IndexOf);
					String finalimage = "NEWS_" + DateHelper.GetDateTimeNowCustomFormat("yyyyMMddHHmmssSSS") + domainName;
					String paramFilePath = "C://Program Files/Apache Software Foundation/Tomcat 8.0/webapps/Images/bnilife";
					File savedFile = new File(paramFilePath + "/temp/" + finalimage);
					fileLocation = paramFilePath + "/temp/" + finalimage;
					InputStream originalFile = filePart.getInputStream();
					FileUtils.copyInputStreamToFile(originalFile, savedFile );
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
//			urlAPI = request.getParameter("APIUrl");
//			methodAPI = request.getParameter("method");
//			contentAPI = request.getParameter("content") == null ? "" : request.getParameter("content");
//			fileLocation = request.getParameter("file") == null ? "" : request.getParameter("file");
		}
		else {
			response.setContentType("application/json");
			
			try {
				StringBuilder sb = new StringBuilder();
				String s;
				while ((s = request.getReader().readLine()) != null) {
					sb.append(s);
				}
				
				JSONObject jsonObject = (JSONObject) new JSONParser().parse(sb.toString());
				contentAPI = sb.toString();
				methodAPI = jsonObject.get("method").toString();
				urlAPI = jsonObject.get("APIUrl").toString();
				
				if(methodAPI.equals("GET") && urlAPI.contains("?")) {
					urlAPI_header = urlAPI.substring(0,urlAPI.lastIndexOf("?"));
				}
				else {
					urlAPI_header = jsonObject.get("APIUrl").toString();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try {
			Context context = (Context) new InitialContext().lookup("java:comp/env");
			String urlWebServer = (String) context.lookup("base_url");
			urlFinal = urlWebServer + urlAPI;
			String apiKey = (String) context.lookup("api_key");
			model.mdlToken mdlToken = TokenWebAdapter.GetTokenWeb();
			
			model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
			
			WebResource webResource = client.resource(urlFinal);
			ClientResponse apiResponse = null;
			String bodyToHash = contentAPI.replaceAll("\\s+", "");
			mdlHeader = HeaderAdapter.GetHeaders(methodAPI, urlAPI_header, mdlToken.access_token, bodyToHash, apiKey);
			authorization = "Bearer " + mdlToken.access_token;

			if (isMultipart) {
				Builder builder = webResource.type(MediaType.MULTIPART_FORM_DATA_TYPE)
					.header("Authorization", authorization)
					.header("Key", mdlHeader.Key)
					.header("Timestamp", mdlHeader.Timestamp)
					.header("Signature", mdlHeader.Signature);
				
				FormDataMultiPart formData = new FormDataMultiPart();
				
				if(isFileExist == true) {
					FileDataBodyPart filePart = new FileDataBodyPart("file", new File(fileLocation));
					formData.bodyPart(filePart);
				}
				
				formData.field("json", contentAPI);
				
				apiResponse = builder.post(ClientResponse.class, formData);
				jsonResult = apiResponse.getEntity(String.class);
			}
			else {
				Builder builder = webResource.type("application/json")
					.header("Authorization", authorization)
					.header("Key", mdlHeader.Key)
					.header("Timestamp", mdlHeader.Timestamp)
					.header("Signature", mdlHeader.Signature);
		
				if (methodAPI.equalsIgnoreCase("POST")) {
					apiResponse = builder.post(ClientResponse.class, contentAPI);
				} else if (methodAPI.equalsIgnoreCase("GET")) {
					apiResponse = builder.get(ClientResponse.class);
				} else if (methodAPI.equalsIgnoreCase("PUT")) {
					apiResponse = builder.put(ClientResponse.class, contentAPI);
				} else if (methodAPI.equalsIgnoreCase("DELETE")) {
					apiResponse = builder.delete(ClientResponse.class, contentAPI);
				}
				jsonResult = apiResponse.getEntity(String.class);
			}
	
			String responseStatus = Integer.toString(apiResponse.getStatus());
	
			if (responseStatus.equalsIgnoreCase("401")) {
				try {
					mdlErrorSchema = gson.fromJson(jsonResult, model.mdlErrorSchema.class);
				} catch (Exception e) {
					stopTime = System.currentTimeMillis();
					elapsedTime = stopTime - startTime;
					logger.error("FAILED = ResponseTime : " + elapsedTime + ", URL :" + urlFinal + ", method: " + methodAPI + ", jsonIn:" + contentAPI + ", Authorization : " + authorization + ", Key:" + mdlHeader.Key + ", Timestamp:" + mdlHeader.Timestamp + ", Signature:" + mdlHeader.Signature + ", jsonOut:" + jsonResult + "Exception:" + e.toString(), e);
					jsonResult = "{\"Error\":\"" + e.toString() + "\"}";
				}
		
				if (mdlErrorSchema.error_code != null && (mdlErrorSchema.error_code.equalsIgnoreCase("ERR-99-005") || mdlErrorSchema.error_code.equalsIgnoreCase("ERR-99-006"))) {
					mdlToken = TokenWebAdapter.GetNewToken();
					mdlHeader = HeaderAdapter.GetHeaders(methodAPI, urlAPI_header, mdlToken.access_token, bodyToHash, apiKey);
					authorization = "Bearer " + mdlToken.access_token;
					
					if (isMultipart) {
						Builder builder = webResource.type(MediaType.MULTIPART_FORM_DATA_TYPE)
							.header("Authorization", authorization)
							.header("Key", mdlHeader.Key)
							.header("Timestamp", mdlHeader.Timestamp)
							.header("Signature", mdlHeader.Signature);
						
						FormDataMultiPart formData = new FormDataMultiPart();
						if(isFileExist == true) {
							FileDataBodyPart filePart = new FileDataBodyPart("file", new File(fileLocation));
							formData.bodyPart(filePart);
						}
						formData.field("json", contentAPI);
						
						apiResponse = builder.post(ClientResponse.class, formData);
						jsonResult = apiResponse.getEntity(String.class);
					}
					else{
						Builder builder = webResource.type("application/json")
							.header("Authorization", authorization)
							.header("Key", mdlHeader.Key)
							.header("Timestamp", mdlHeader.Timestamp)
							.header("Signature", mdlHeader.Signature);
			
						if (methodAPI.equalsIgnoreCase("POST")) {
							apiResponse = builder.post(ClientResponse.class, contentAPI);
						} else if (methodAPI.equalsIgnoreCase("GET")) {
							apiResponse = builder.get(ClientResponse.class);
						} else if (methodAPI.equalsIgnoreCase("PUT")) {
							apiResponse = builder.put(ClientResponse.class, contentAPI);
						} else if (methodAPI.equalsIgnoreCase("DELETE")) {
							apiResponse = builder.delete(ClientResponse.class, contentAPI);
						}
						jsonResult = apiResponse.getEntity(String.class);
					}
					responseStatus = Integer.toString(response.getStatus());
				}
			}
			stopTime = System.currentTimeMillis();
			elapsedTime = stopTime - startTime;
			logger.info("SUCCESS = ResponseTime : " + elapsedTime + ", ResponseStatus : " + responseStatus + ", callAPI = URL :" + urlFinal + ", method: " + methodAPI + ", Key:" + mdlHeader.Key + ", Timestamp:" + mdlHeader.Timestamp + ", Signature:" + mdlHeader.Signature + ", jsonIn:" + jsonIn + ", jsonOut:" + jsonResult);
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(jsonResult);
		} catch (Exception ex) {
			stopTime = System.currentTimeMillis();
			elapsedTime = stopTime - startTime;
			logger.error("FAILED = ResponseTime : " + elapsedTime + ", URL :" + urlFinal + ", method: " + methodAPI + ", jsonIn:" + jsonIn + ", Authorization : " + authorization + ", Key:" + mdlHeader.Key + ", Timestamp:" + mdlHeader.Timestamp + ", Signature:" + mdlHeader.Signature + ", jsonOut:" + jsonResult + "Exception:" + ex.toString(), ex);
		}

	}
}
