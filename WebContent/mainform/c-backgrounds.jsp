<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
		<div class="customization container-fluid">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-12 col-md-4 col-lg-3 mb-5">
						<h4>Splash Screen</h4>
						<p>
							Resolution <b>1440 x 2560 px</b>
							<br>
							File Format <b>PNG</b>
						</p>
						<div class="icon-card d-block" style="width: 100%;">
							<img class="background_image" src="${image_url}/splashscreen/splashscreen.png" data-filename="splashscreen.png" alt="Splash Screen">
							<input type='file' onchange="change_image(this,1440,2560)" />
							<button type="button" class="btn btn-primary d-block" data-filename="splashscreen.png">Save Changes</button>
						</div>
					</div>
					<div class="col-12 col-md-4 col-lg-3 mb-5">
						<h4>Login Background Indonesia</h4>
						<p>
							Resolution <b>1080 x 594 px</b>
							<br>
							File Format <b>PNG</b>
						</p>
						<div class="icon-card d-block" style="width: 100%;">
							<img class="background_image" src="${image_url}/splashscreen/login-background-indonesia.png" data-filename="login-background-indonesia.png" alt="Login Background Indonesia">
							<input type='file' onchange="change_image(this,1080,594)" />
							<button type="button" class="btn btn-primary d-block" data-filename="login-background-indonesia.png">Save Changes</button>
						</div>
					</div>
					<div class="col-12 col-md-4 col-lg-3 mb-5">
						<h4>Login Background English</h4>
						<p>
							Resolution <b>1080 x 594 px</b>
							<br>
							File Format <b>PNG</b>
						</p>
						<div class="icon-card d-block" style="width: 100%;">
							<img class="background_image" src="${image_url}/splashscreen/login-background-english.png" data-filename="login-background-english.png" alt="Login Background English">
							<input type='file' onchange="change_image(this,1080,594)" />
							<button type="button" class="btn btn-primary d-block" data-filename="login-background-english.png">Save Changes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		</main>
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script>
	
	$("input").val(null);
	$('button').prop('disabled', true);
	
	$('button').on( "click", function() {
		if($(this).prevAll('input:first').get(0).files.length > 0){
			var input_parameter = {};
			input_parameter.folder = "splashscreen";
			input_parameter.filename = $(this).data("filename");
			
			var url = url_local_web+"/APICall";
			input_parameter.APIUrl = "/update-image";
			input_parameter.method = "POST";
			
			var formData = new FormData();
			formData.append('json', JSON.stringify(input_parameter));
			formData.append('file', $(this).prevAll('input:first')[0].files[0] );
			
			$.ajax({
				url : url,
				type: 'POST',
				enctype: 'multipart/form-data',
				cache: false,
				contentType: false,
				processData: false,
				data: formData,
				dataType: "json",
				beforeSend: function() {
				},
			}).done(function (data) {
				console.log(data);
				if(data.error_schema.error_code == "ERR-00-000"){
					Swal.fire({
						allowOutsideClick: false,
						type: 'success',
						title: 'Alert',
						text: "Proses data sukses"
					}).then((result) => {
						if (result.value) {
							location.reload();
						}
					});
				}
				else{
					Swal.fire({
						allowOutsideClick: false,
						type: 'error',
						title: 'Alert',
						text: "Proses data gagal"
					}).then((result) => {
						if (result.value) {
							location.reload();
						}
					});
				}
				
			}).fail(function (jqXHR, textStatus) {
				console.log("failed " + jqXHR + " " + textStatus);
				Swal.fire({
					type: 'error',
					title: 'Alert',
					text: "Gagal memulai proses pengiriman data"
				});
			});
		}
		else{
			alert("Gambar belum dimasukkan");
		}
	});
	
	function change_image(input, param_width, param_height) {
		var image_base_url = "${image_url}/splashscreen/";
		
		var file = input.files && input.files[0];
		if (file) {
			var img = new Image();
	        img.src = window.URL.createObjectURL( file );
// 			console.log(file.type);
	        img.onload = function() {
	            var width = img.naturalWidth, height = img.naturalHeight;
				//console.log("width: "+width+" height: "+height);
				
	            window.URL.revokeObjectURL( img.src );

	            if( width <= param_width && height <= param_height ) {
	            	loadMime(file, function(mime) {
	            		//console.log("image type: "+mime);
	            		if(mime == "image/png"){
	            			var reader = new FileReader();
	    	    			reader.onload = function (e) {
	    	    				$(input).prevAll('.background_image').attr('src', e.target.result);
	    	    				//console.log(e.target.result);
	    	    			};
	    	    			reader.readAsDataURL(file);
	    	    			
	    	    			$(input).nextAll('button:first').prop('disabled',false);
	            		}
	            		else{
	            			alert("Format gambar tidak sesuai dengan persyaratan yang ada ");
	            		}
	                });
	            	
	            	
	            }
	            else {
	                alert("Resolusi gambar tidak sesuai dengan persyaratan yang ada ");
	            }
	        };
			
		}
	}
	
	function loadMime(file, callback) {
	    
	    //List of known mimes
	    var mimes = [
	        {
	            mime: 'image/jpeg',
	            pattern: [0xFF, 0xD8, 0xFF],
	            mask: [0xFF, 0xFF, 0xFF],
	        },
	        {
	            mime: 'image/png',
	            pattern: [0x89, 0x50, 0x4E, 0x47],
	            mask: [0xFF, 0xFF, 0xFF, 0xFF],
	        }
	        // you can expand this list @see https://mimesniff.spec.whatwg.org/#matching-an-image-type-pattern
	    ];

	    function check(bytes, mime) {
	        for (var i = 0, l = mime.mask.length; i < l; ++i) {
	            if ((bytes[i] & mime.mask[i]) - mime.pattern[i] !== 0) {
	                return false;
	            }
	        }
	        return true;
	    }

	    var blob = file.slice(0, 4); //read the first 4 bytes of the file

	    var reader = new FileReader();
	    reader.onloadend = function(e) {
	        if (e.target.readyState === FileReader.DONE) {
	            var bytes = new Uint8Array(e.target.result);

	            for (var i=0, l = mimes.length; i<l; ++i) {
	                //if (check(bytes, mimes[i])) return callback("Mime: " + mimes[i].mime + " <br> Browser:" + file.type);
	            	if (check(bytes, mimes[i])) return callback(file.type);
	            }

	            //return callback("Mime: unknown <br> Browser:" + file.type);
	            return file.type;
	        }
	    };
	    reader.readAsArrayBuffer(blob);
	}
	</script>
</body>
</html>
