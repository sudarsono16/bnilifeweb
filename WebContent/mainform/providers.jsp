<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>

<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="mainform/plugins/datatables/extensions/Responsive/css/responsive.bootstrap4.min.css">
<style>
table.dataTable tbody td {
	word-break: break-word;
	vertical-align: top;
}
</style>

</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row justify-content-center">
						<div class="col-6 col-md-2">
							<div class="card" id="clinic_card">
								<div class="card-body">
									<div class="text-value">0</div>
									<small class="text-muted text-uppercase font-weight-bold">Clinic</small>
<!-- 									<div class="progress progress-xs mt-3 mb-0"> -->
<!-- 										<div class="progress-bar bg-info" role="progressbar" style="width: 25%" ></div> -->
<!-- 									</div> -->
								</div>
							</div>
						</div>
						<div class="col-6 col-md-2">
							<div class="card" id="hospital_card">
								<div class="card-body">
									<div class="text-value">0</div>
									<small class="text-muted text-uppercase font-weight-bold">Hospital</small>
<!-- 									<div class="progress progress-xs mt-3 mb-0"> -->
<!-- 										<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" -->
<!-- 											aria-valuemax="100"></div> -->
<!-- 									</div> -->
								</div>
							</div>
						</div>
						<div class="col-6 col-md-2">
							<div class="card" id="laboratory_card">
								<div class="card-body">
									<div class="text-value">0</div>
									<small class="text-muted text-uppercase font-weight-bold">Laboratorium</small>
<!-- 									<div class="progress progress-xs mt-3 mb-0"> -->
<!-- 										<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0 -->
<!-- 											aria-valuemax="100"></div> -->
<!-- 									</div> -->
								</div>
							</div>
						</div>
						<div class="col-6 col-md-2">
							<div class="card" id="optic_card">
								<div class="card-body">
									<div class="text-value">0</div>
									<small class="text-muted text-uppercase font-weight-bold">Optic</small>
<!-- 									<div class="progress progress-xs mt-3 mb-0"> -->
<!-- 										<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0 -->
<!-- 											aria-valuemax="100"></div> -->
<!-- 									</div> -->
								</div>
							</div>
						</div>
					</div>
					<div class="search-container row">
						<div class="col-lg-3 col-md-3 col-12">
							<button class="btn btn-block btn-warning" id="SubmitNew" type="button" data-toggle="modal" data-target="#addProvider">
								<i class="fa fa-plus-circle"></i> Add Provider
							</button>
						</div>
						<div class="col-lg-3 col-md-3 col-12"></div>
<!-- 						<div class="col-lg-3 col-md-3 col-12"> -->
<!-- 							<div class="searchbar input-group"> -->
<!-- 								<div class="input-group-prepend"> -->
<!-- 									<span class="input-group-text"><i class="fa fa-search"></i></span> -->
<!-- 								</div> -->
<!-- 								<input type="text" class="form-control" placeholder="`vider"> -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 						<div class="col-lg-3 col-md-3 col-12"> -->
<!-- 							<div class="searchbar input-group"> -->
<!-- 								<div class="input-group-prepend"> -->
<!-- 									<span class="input-group-text"><b>Filter</b></span> -->
<!-- 								</div> -->
<!-- 								<select class="custom-select"> -->
<!-- 									<option selected>All</option> -->
<!-- 									<option value="1">Clinic</option> -->
<!-- 									<option value="2">Hospital</option> -->
<!-- 									<option value="3">Laboratorium</option> -->
<!-- 									<option value="4">Optic</option> -->
<!-- 								</select> -->
<!-- 							</div> -->
<!-- 						</div> -->
					</div>
					<!-- SEARCH-CONTAINER -->
				</div>
			</div>
			<div class="container-fluid">
				<div class="animated fadeIn">
					<table class="table table-striped table-bordered sortable" id="main_table">
						<thead>
							<tr>
								<th>Type</th>
								<th>Rekanan</th>
								<th>Name</th>
								<th>City</th>
								<th>Province</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</main>
		<!-- Edit Information Modal -->
		<div class="modal fade" id="addProvider" tabindex="-1" role="dialog" aria-labelledby="addProviderLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form id="FormSubmit" >
					<div class="modal-content">
						<div class="modal-header">
	<!-- 						<h5 class="modal-title claim-num" id="addProviderLabel">Add New Provider</h5> -->
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col">
									<input type="hidden" id="provider_id" name="provider_id" readonly>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<span class="input-group-text">Tipe Provider</span>
										</div>
										<select class="custom-select form-control" name="tipe_provider" id="tipe_provider"></select>
									</div>
									<br>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<span class="input-group-text">Tipe Rekanan</span>
										</div>
										<select class="custom-select form-control" name="tipe_rekanan" id="tipe_rekanan"></select>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Name</span>
										</div>
										<input type="text" id="nama_provider" name="nama_provider" class="form-control" style="text-transform: capitalize;">
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Address</span>
										</div>
										<input type="text" id="alamat" name="alamat" class="form-control" style="text-transform: capitalize;">
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Province</span>
										</div>
										<input type="text" id="provinsi" name="provinsi" class="form-control" style="text-transform: capitalize;">
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">City</span>
										</div>
										<input type="text" id="kota" name="kota" class="form-control" style="text-transform: capitalize;">
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Location</span>
										</div>
										<input type="text" id="latitude" name="latitude" class="form-control" placeholder="Latitude">
										<input type="text" id="longitude" name="longitude" class="form-control" placeholder="Longitude">
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Phone Number</span>
										</div>
										<input type="tel" id="no_telp1" name="no_telp1" class="form-control" placeholder="Nomor telepon" aria-label="Phone Number">
										<input type="tel" id="no_telp2" name="no_telp2" class="form-control" placeholder="Nomor telepon" aria-label="Phone Number">
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Phone Number</span>
										</div>
										<input type="tel" id="no_telp3" name="no_telp3" class="form-control" placeholder="Nomor telepon" aria-label="Phone Number">
										<input type="tel" id="no_telp4" name="no_telp4" class="form-control" placeholder="Nomor telepon" aria-label="Phone Number">
									</div>
<!-- 									<div class="input-group input-group-sm mt-3"> -->
<!-- 										<div class="input-group-prepend"> -->
<!-- 											<span class="input-group-text">Website</span> -->
<!-- 										</div> -->
<!-- 										<input type="url" id="website" name="website" class="form-control" placeholder="www.site.com"> -->
<!-- 									</div> -->
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="col">
									<div id="accordion" role="tablist">
										<!-- Start Rawat Inap -->
										<div class="card mb-0">
											<div class="card-header" role="tab">
												<h5 class="mb-0">
													<a data-toggle="collapse" href="#rawatInap" aria-expanded="true" aria-controls="rawatInap">Rawat Inap</a>
												</h5>
											</div>
											<div class="collapse" id="rawatInap" role="tabpanel" aria-labelledby="rawatInap" data-parent="#accordion">
												<div class="row mx-1 mt-3">
													<div class="col">
														<div class="input-group input-group-sm">
															<div class="input-group-prepend">
																<span class="input-group-text">President</span>
															</div>
															<input type="text" id="rawat_inap_presiden" name="rawat_inap_presiden"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm">
															<div class="input-group-prepend">
																<span class="input-group-text">SVIP</span>
															</div>
															<input type="text" id="rawat_inap_svip" name="rawat_inap_svip"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">VVIP</span>
															</div>
															<input type="text" id="rawat_inap_vvip" name="rawat_inap_vvip"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">VIP</span>
															</div>
															<input type="text" id="rawat_inap_vip" name="rawat_inap_vip"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Utama</span>
															</div>
															<input type="text" id="rawat_inap_utama" name="rawat_inap_utama"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 1</span>
															</div>
															<input type="text" id="rawat_inap_kelas1" name="rawat_inap_kelas1"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 2</span>
															</div>
															<input type="text" id="rawat_inap_kelas2" name="rawat_inap_kelas2"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 3</span>
															</div>
															<input type="text" id="rawat_inap_kelas3" name="rawat_inap_kelas3"  class="form-control">
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- End Rawat Inap -->
										<!-- Start Maternity -->
										<div class="card mb-0">
											<div class="card-header" role="tab">
												<h5 class="mb-0">
													<a data-toggle="collapse" href="#maternity" aria-expanded="true" aria-controls="maternity">Maternity</a>
												</h5>
											</div>
											<div class="collapse" id="maternity" role="tabpanel" aria-labelledby="maternity" data-parent="#accordion">
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm ">
															<div class="input-group-prepend">
																<span class="input-group-text">President</span>
															</div>
															<input type="text" id="persalinan_presiden" name="persalinan_presiden"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm ">
															<div class="input-group-prepend">
																<span class="input-group-text">SVIP</span>
															</div>
															<input type="text" id="persalinan_svip" name="persalinan_svip"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">VVIP</span>
															</div>
															<input type="text" id="persalinan_vvip" name="persalinan_vvip"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">VIP</span>
															</div>
															<input type="text" id="persalinan_vip" name="persalinan_vip"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Utama</span>
															</div>
															<input type="text" id="persalinan_utama" name="persalinan_utama"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 1</span>
															</div>
															<input type="text" id="persalinan_kelas1" name="persalinan_kelas1"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 2</span>
															</div>
															<input type="text" id="persalinan_kelas2" name="persalinan_kelas2"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 3</span>
															</div>
															<input type="text" id="persalinan_kelas3" name="persalinan_kelas3"  class="form-control">
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- End Maternity -->
										<!-- Start Maternity (Sectio) -->
										<div class="card mb-0">
											<div class="card-header" role="tab">
												<h5 class="mb-0">
													<a data-toggle="collapse" href="#maternity-sectio" aria-expanded="true" aria-controls="maternity-sectio">Maternity
														(Sectio)</a>
												</h5>
											</div>
											<div class="collapse" id="maternity-sectio" role="tabpanel" aria-labelledby="maternity-sectio"
												data-parent="#accordion">
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm ">
															<div class="input-group-prepend">
																<span class="input-group-text">President</span>
															</div>
															<input type="text" id="persalinan_sectio_presiden" name="persalinan_sectio_presiden"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm ">
															<div class="input-group-prepend">
																<span class="input-group-text">SVIP</span>
															</div>
															<input type="text" id="persalinan_sectio_svip" name="persalinan_sectio_svip"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">VVIP</span>
															</div>
															<input type="text" id="persalinan_sectio_vvip" name="persalinan_sectio_vvip"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">VIP</span>
															</div>
															<input type="text" id="persalinan_sectio_vip" name="persalinan_sectio_vip"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Utama</span>
															</div>
															<input type="text" id="persalinan_sectio_utama" name="persalinan_sectio_utama"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 1</span>
															</div>
															<input type="text" id="persalinan_sectio_kelas1" name="persalinan_sectio_kelas1"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 2</span>
															</div>
															<input type="text" id="persalinan_sectio_kelas2" name="persalinan_sectio_kelas2"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 3</span>
															</div>
															<input type="text" id="persalinan_sectio_kelas3" name="persalinan_sectio_kelas3"  class="form-control">
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- End Maternity (Sectio) -->
										<!-- Start Doctor (Out-Patient) -->
										<div class="card mb-0">
											<div class="card-header" role="tab">
												<h5 class="mb-0">
													<a data-toggle="collapse" href="#doctor-out" aria-expanded="true" aria-controls="doctor-out">Doctor
														(Out-Patient)</a>
												</h5>
											</div>
											<div class="collapse" id="doctor-out" role="tabpanel" aria-labelledby="doctor-out" data-parent="#accordion">
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm ">
															<div class="input-group-prepend">
																<span class="input-group-text">General</span>
															</div>
															<input type="text" id="rawat_jalan_dokter_umum" name="rawat_jalan_dokter_umum"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm ">
															<div class="input-group-prepend">
																<span class="input-group-text">Specialist</span>
															</div>
															<input type="text" id="rawat_jalan_dokter_spesialis" name="rawat_jalan_dokter_spesialis" class="form-control">
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- End Doctor (Out-Patient) -->
										<!-- Start Doctor (In-Patient) -->
										<div class="card mb-0">
											<div class="card-header" role="tab">
												<h5 class="mb-0">
													<a data-toggle="collapse" href="#doctor-in" aria-expanded="true" aria-controls="doctor-in">Doctor
														(In-Patient)</a>
												</h5>
											</div>
											<div class="collapse" id="doctor-in" role="tabpanel" aria-labelledby="doctor-in" data-parent="#accordion">
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm ">
															<div class="input-group-prepend">
																<span class="input-group-text">President</span>
															</div>
															<input type="text" id="perawatan_presiden" name="perawatan_presiden"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm ">
															<div class="input-group-prepend">
																<span class="input-group-text">SVIP</span>
															</div>
															<input type="text" id="perawatan_svip" name="perawatan_svip"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">VVIP</span>
															</div>
															<input type="text" id="perawatan_vvip" name="perawatan_vvip"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">VIP</span>
															</div>
															<input type="text" id="perawatan_vip" name="perawatan_vip"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Utama</span>
															</div>
															<input type="text" id="perawatan_utama" name="perawatan_utama"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 1</span>
															</div>
															<input type="text" id="perawatan_kelas1" name="perawatan_kelas1"  class="form-control">
														</div>
													</div>
												</div>
												<div class="row mx-1 my-3">
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 2</span>
															</div>
															<input type="text" id="perawatan_kelas2" name="perawatan_kelas2"  class="form-control">
														</div>
													</div>
													<div class="col">
														<div class="input-group input-group-sm mt-3">
															<div class="input-group-prepend">
																<span class="input-group-text">Kelas 3</span>
															</div>
															<input type="text" id="perawatan_kelas3" name="perawatan_kelas3"  class="form-control">
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- End Doctor (In-Patient) -->
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button class="btn btn-primary" id="SubmitButton" type="submit" >Save changes</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- MODAL -->
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script>
	var datatable = {};
	$(document).ready(function() {
		
		$.ajax({
			url : url_local_web+"/APICall",
			type: 'POST',
			data: JSON.stringify({"APIUrl":"/provider/get-provider-count", "method":"GET"}),
			dataType: "json",
			headers: {
				'Content-Type': 'application/json'
			},
		}).done(function (data) {
			if(data.error_schema.error_code == "ERR-00-000"){
				var clinic_count = parseInt(data.output_schema.klinik_count);
				var laboratory_count = parseInt(data.output_schema.lab_count);
				var optic_count = parseInt(data.output_schema.optik_count);
				var hospital_count = parseInt(data.output_schema.rumah_sakit_count);
				
				$('#clinic_card').find('.text-value').empty().text(clinic_count);				
				$('#hospital_card').find('.text-value').empty().text(hospital_count);
				$('#laboratory_card').find('.text-value').empty().text(laboratory_count);
				$('#optic_card').find('.text-value').empty().text(optic_count);
				
			}
		}).fail(function (jqXHR, textStatus) {
			console.log("failed " + jqXHR + " " + textStatus);
		});
		
		var draw_num = 0;
		datatable = $('#main_table').DataTable({
			language: {
				"processing": "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
			},
			autoWidth: true,
			processing: true,
			searching: true,
			searchDelay: 500,
			destroy: true,
			cache: true,
			contentType: "application/json; charset=utf-8",
// 			dom: 'lfrtip',
			PaginationType: "full",
			responsive: true,
			serverSide: true,
			ajax: {
				"url": url_local_web+"/APICall",
				"timeout": 60000,
				"type": "POST",
				"headers": {
					'Content-Type': 'application/json'
				},
				"data": function (param) {
					var pageNumber = $('#main_table').DataTable().page.info().page + 1;
					var pageSize = $('#main_table').DataTable().page.info().length;
					var input_parameter = {};
					var searchstring = "";
					if (param.search.value.trim()) {
						searchstring = '&search='+encodeURIComponent(param.search.value);
					}
					
					input_parameter.APIUrl = "/provider/get-provider?pageNumber="+pageNumber+"&pageSize="+pageSize+'&draw='+param.draw+searchstring;
					input_parameter.method = "GET";
					return JSON.stringify(input_parameter);
				},
				"dataSrc": "data"
			},
// 			preDrawCallback: function( settings ) {
				
// 			},
			initComplete: function(settings, json){
// 				console.log(settings);
// 				console.log(json);
			},
			responsive: {
				breakpoints: [
					{ name: 'desktop',  width: Infinity },
					{ name: 'tablet-l', width: 1024 },
					{ name: 'tablet-p', width: 768 },
					{ name: 'mobile-l', width: 480 },
					{ name: 'mobile-p', width: 320 }
				]
			},
			columns: [
				{
					"data": "tipe_provider",
					"render": function (data, type, row) {
						return data.trim();
					}
				},
				{
					"data": "tipe_rekanan",
					"render": function (data, type, row) {
						if(data == 1){
							return "BNI Life";
						}
						else if(data == 2 ){
							return "Ad Medika";
						}
						else{
							return "All";
						}
					}
				},
				{
					"data": "nama_provider",
					"render": function (data, type, row) {
						return data.trim();
					}
				},
				{
					"data": "kota",
					"render": function (data, type, row) {
						return data.trim();
					}
				},
				{
					"data": "provinsi",
					"render": function (data, type, row) {
						return data.trim();
					}
				},
				{
					"data": "provider_id",
					"render": function (data, type, row) {
						var button_string = "";
						button_string += '<span class="input-group-btn">';
						button_string += '<button type="button" class="btn btn-success btn-sm provider_detail" style="width: 35px;" data-id="'+data+'"><i class="fa fa-search-plus"></i></button>';
						button_string += '<button type="button" class="btn btn-info btn-sm provider_update" style="width: 35px;" data-id="'+data+'"><i class="fa fa-edit"></i></button>';
						button_string += '<button type="button" class="btn btn-danger btn-sm provider_delete" style="width: 35px;" data-id="'+data+'"><i class="fa fa-trash-o"></i></button>';
						button_string += '</span>';
						return button_string;
					}
				},
				{"data": "provider_id"}, 
				{"data": "alamat"}, 
				{"data": "latitude"}, 
				{"data": "longitude"}, 
				
				{"data": "no_telp1"}, 
				{"data": "no_telp2"}, 
				{"data": "no_telp3"}, 
				{"data": "no_telp4"}, 
				
				{"data": "perawatan_kelas1"}, 
				{"data": "perawatan_kelas2"}, 
				{"data": "perawatan_kelas3"}, 
				{"data": "perawatan_presiden"}, 
				{"data": "perawatan_svip"}, 
				{"data": "perawatan_utama"}, 
				{"data": "perawatan_vip"}, 
				{"data": "perawatan_vvip"}, 
				
				{"data": "persalinan_kelas1"}, 
				{"data": "persalinan_kelas2"}, 
				{"data": "persalinan_kelas3"}, 
				{"data": "persalinan_presiden"}, 
				{"data": "persalinan_svip"}, 
				{"data": "persalinan_utama"}, 
				{"data": "persalinan_vip"}, 
				{"data": "persalinan_vvip"}, 
				
				{"data": "persalinan_sectio_kelas1"}, 
				{"data": "persalinan_sectio_kelas2"}, 
				{"data": "persalinan_sectio_kelas3"}, 
				{"data": "persalinan_sectio_presiden"}, 
				{"data": "persalinan_sectio_svip"}, 
				{"data": "persalinan_sectio_utama"}, 
				{"data": "persalinan_sectio_vip"}, 
				{"data": "persalinan_sectio_vvip"}, 
				
				{"data": "rawat_inap_kelas1"}, 
				{"data": "rawat_inap_kelas2"}, 
				{"data": "rawat_inap_kelas3"}, 
				{"data": "rawat_inap_presiden"}, 
				{"data": "rawat_inap_svip"}, 
				{"data": "rawat_inap_utama"}, 
				{"data": "rawat_inap_vip"}, 
				{"data": "rawat_inap_vvip"}, 
				
				{"data": "rawat_jalan_dokter_spesialis"}, 
				{"data": "rawat_jalan_dokter_umum"}
			],
			columnDefs: [
				{
					targets: [0, 1, 2, 3, 4],
					searchable: true,
					sortable: true,
					visible: true,
					defaultContent: ""
				},
				{
					targets: [5],
					searchable: false,
					sortable: false,
					visible: true
				},
				{
					targets: '_all',
					searchable: false,
					sortable: false,
					visible: false,
					defaultContent: ""
				}
			],
			preDrawCallback: function( settings ) {	}
		}).on('xhr.dt', function (e, settings, json, xhr) {
			if(json.output_schema != null || typeof json.output_schema != 'undefined'){
				json.recordsTotal = json.output_schema.recordsTotal;
			    json.recordsFiltered = json.output_schema.recordsFiltered;
			    json.draw = json.output_schema.draw;
			    json.data = json.output_schema.items;
			}
			else{
				json.recordsTotal = 0;
			    json.recordsFiltered = 0;
			    json.data = [];
			}
		});
		
		
	});
	
	$('#main_table tbody').on('click', '.provider_detail',function () {
		$('#SubmitButton').hide();
		
		var current_data = "";
		if ($(this).parent().hasClass("dtr-data")) {
			current_data = datatable.row($(this)).data();
		}
		else {
			current_data = datatable.row($(this).closest('tr')).data();
		}
		populate_select_provider(current_data.tipe_provider);
		populate_select_rekanan(current_data.tipe_rekanan);
		fillData(current_data);
		
		$('#addProvider').modal({
			backdrop: 'static',
			keyboard: false, 
			show: true
		});
		
	});
	
	$('#main_table tbody').on('click', '.provider_update',function () {
		$('#SubmitButton').show();
		validate_start_function('update');
		
		var current_data = "";
		if ($(this).parent().hasClass("dtr-data")) {
			current_data = datatable.row($(this)).data();
		}
		else {
			current_data = datatable.row($(this).closest('tr')).data();
		}
		
		$('#provider_id').val(current_data.provider_id);
		
		populate_select_provider(current_data.tipe_provider);
		populate_select_rekanan(current_data.tipe_rekanan);
		fillData(current_data);
		
		$('#addProvider').modal({
			backdrop: 'static',
			keyboard: false, 
			show: true
		});
		
	});
	
	$('#main_table tbody').on('click', '.provider_delete',function () {
		var ID = $(this).data('id');
		Swal.fire({
			title: 'Are you sure?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete Provider'
		}).then((result) => {
			if (result.value) {
				var input_parameter = {};
				input_parameter.APIUrl = "/provider/delete-provider?id="+ID;
				input_parameter.method = "GET";
				
				$.ajax({
					url : url_local_web+"/APICall",
					type: 'POST',
					data: JSON.stringify(input_parameter),
					dataType: "json",
					headers: {
						'Content-Type': 'application/json'
					},
					beforeSend: function() {
					},
				}).done(function (data) {
					if(data.error_schema.error_code == "ERR-00-000"){
						Swal.fire({
							type: 'success',
							title: 'Alert',
							text: "Delete success"
						});
					}
					else{
						Swal.fire({
							type: 'error',
							title: 'Alert',
							text: "Delete failed"
						});
					}
					datatable.ajax.reload();
				}).fail(function (jqXHR, textStatus) {
					console.log("failed " + jqXHR + " " + textStatus);
					Swal.fire({
						type: 'error',
						title: 'Alert',
						text: "Delete error"
					});
				});
				
			}
		});
	});
	
	function fillData(current_data){
		console.log(current_data);
		$('#alamat').val(current_data.alamat);
		$('#kota').val(current_data.kota);
		$('#provinsi').val(current_data.provinsi);
		$('#latitude').val(current_data.latitude);
		$('#longitude').val(current_data.longitude);
		$('#nama_provider').val(current_data.nama_provider);
		
		$('#no_telp1').val(current_data.no_telp1);
		$('#no_telp2').val(current_data.no_telp2);
		$('#no_telp3').val(current_data.no_telp3);
		$('#no_telp4').val(current_data.no_telp4);
		
// 		$('#website').val(current_data.website);
		
		$('#perawatan_kelas1').val(current_data.perawatan.perawatan_kelas1);
		$('#perawatan_kelas2').val(current_data.perawatan.perawatan_kelas2);
		$('#perawatan_kelas3').val(current_data.perawatan.perawatan_kelas3);
		$('#perawatan_presiden').val(current_data.perawatan.perawatan_presiden);
		$('#perawatan_svip').val(current_data.perawatan.perawatan_svip);
		$('#perawatan_utama').val(current_data.perawatan.perawatan_utama);
		$('#perawatan_vip').val(current_data.perawatan.perawatan_vip);
		$('#perawatan_vvip').val(current_data.perawatan.perawatan_vvip);
		
		$('#persalinan_kelas1').val(current_data.persalinan.persalinan_kelas1);
		$('#persalinan_kelas2').val(current_data.persalinan.persalinan_kelas2);
		$('#persalinan_kelas3').val(current_data.persalinan.persalinan_kelas3);
		$('#persalinan_presiden').val(current_data.persalinan.persalinan_presiden);
		$('#persalinan_svip').val(current_data.persalinan.persalinan_svip);
		$('#persalinan_utama').val(current_data.persalinan.persalinan_utama);
		$('#persalinan_vip').val(current_data.persalinan.persalinan_vip);
		$('#persalinan_vvip').val(current_data.persalinan.persalinan_vvip);
		
		$('#persalinan_sectio_kelas1').val(current_data.persalinan_sectio.persalinan_sectio_kelas1);
		$('#persalinan_sectio_kelas2').val(current_data.persalinan_sectio.persalinan_sectio_kelas2);
		$('#persalinan_sectio_kelas3').val(current_data.persalinan_sectio.persalinan_sectio_kelas3);
		$('#persalinan_sectio_presiden').val(current_data.persalinan_sectio.persalinan_sectio_presiden);
		$('#persalinan_sectio_svip').val(current_data.persalinan_sectio.persalinan_sectio_svip);
		$('#persalinan_sectio_utama').val(current_data.persalinan_sectio.persalinan_sectio_utama);
		$('#persalinan_sectio_vip').val(current_data.persalinan_sectio.persalinan_sectio_vip);
		$('#persalinan_sectio_vvip').val(current_data.persalinan_sectio.persalinan_sectio_vvip);
		
		$('#rawat_inap_kelas1').val(current_data.rawat_inap.rawat_inap_kelas1);
		$('#rawat_inap_kelas2').val(current_data.rawat_inap.rawat_inap_kelas2);
		$('#rawat_inap_kelas3').val(current_data.rawat_inap.rawat_inap_kelas3);
		$('#rawat_inap_presiden').val(current_data.rawat_inap.rawat_inap_presiden);
		$('#rawat_inap_svip').val(current_data.rawat_inap.rawat_inap_svip);
		$('#rawat_inap_utama').val(current_data.rawat_inap.rawat_inap_utama);
		$('#rawat_inap_vip').val(current_data.rawat_inap.rawat_inap_vip);
		$('#rawat_inap_vvip').val(current_data.rawat_inap.rawat_inap_vvip);
		
		$('#rawat_jalan_dokter_spesialis').val(current_data.rawat_jalan.rawat_jalan_dokter_spesialis);
		$('#rawat_jalan_dokter_umum').val(current_data.rawat_jalan.rawat_jalan_dokter_umum);
		
		$('#accordion input').val(function(index, value) {
			return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		});
	}
	
	$('#addProvider').on('hide.bs.modal', function () {
		reset_form();
	});
	
	$('#addProvider').on('shown.bs.modal', function () {
		$('#accordion input').keyup(function(event) {
			// skip for arrow keys
			if (event.which >= 37 && event.which <= 40)
				return;

			// format number
			$(this).val(function(index, value) {
				return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
			});
		});
	});
	
	$('#SubmitNew').on('click', function () {
		reset_form();
		validate_start_function('new');
	});
	
	function reset_form(){
		$('#SubmitButton').show();
		$("#FormSubmit")[0].reset();
		$form = $('form');
		$form.find('.control-group').removeClass('.error');
		$form.find('.has-error').removeClass("has-error");
		$form.find('.has-success').removeClass("has-success");
		$form.find('.form-control-feedback').remove();
		$('#accordion input').val('0');
		populate_select_provider();
		populate_select_rekanan();
	}
	
	function populate_select_provider(tipe_provider){
		$('#tipe_provider').removeAttr('selected').find('[value=""]').attr('selected','selected').prop("selected",true);
		if(!$("#tipe_provider").find("option:selected").length){
			var providers = [];
			providers.push({"ID":"","Text":"Pilih provider"});
			providers.push({"ID":"Klinik","Text":"Klinik"});
			providers.push({"ID":"Rumah Sakit","Text":"Rumah Sakit"});
			providers.push({"ID":"Laboratorium","Text":"Laboratorium"});
			providers.push({"ID":"Optik","Text":"Optik"});
			
			$.each(providers, function (key, value) {
				$('#tipe_provider').append($('<option>', {
					value: value.ID,
					text: value.Text
				}));
			});
		}
		if(typeof tipe_provider != 'undefined' && tipe_provider != null){
			$('#tipe_provider').removeAttr('selected').find('[value="'+tipe_provider+'"]').attr('selected','selected').prop("selected",true);
		}
	}
	
	function populate_select_rekanan(tipe_rekanan){
		$('#tipe_rekanan').removeAttr('selected').find('[value=""]').attr('selected','selected').prop("selected",true);
		if(!$("#tipe_rekanan").find("option:selected").length){
			var providers = [];
			providers.push({"ID":"","Text":"Pilih rekanan"});
			providers.push({"ID":"1","Text":"BNI Life"});
			providers.push({"ID":"2","Text":"Ad Medika"});
			providers.push({"ID":"3","Text":"All"});
			
			$.each(providers, function (key, value) {
				$('#tipe_rekanan').append($('<option>', {
					value: value.ID,
					text: value.Text
				}));
			});
		}
		if(typeof tipe_provider != 'undefined' && tipe_provider != null){
			$('#tipe_rekanan').removeAttr('selected').find('[value="'+tipe_rekanan+'"]').attr('selected','selected').prop("selected",true);
		}
	}
	
	function validate_start_function(SubmitType){
		$.validator.addMethod("customLetterValidation", function (value, element) {
			return this.optional(element) || /^[a-zA-Z0-9 ]+$/i.test(value);
		}, "Kolom ini hanya boleh diisi alfanumerik dan spasi.");
		
		$.validator.addMethod("customPasswordValidation", function (value, element) {
			return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
		}, "Kolom ini hanya boleh diisi alfanumerik.");
		
		$.validator.addMethod("customAddressValidation", function (value, element) {
			return this.optional(element) || /^[a-zA-Z0-9.,/ ]+$/i.test(value);
		}, "Kolom ini hanya boleh diisi alfanumerik, tanda baca titik, koma, garis miring dan spasi.");
		
		$.validator.addMethod("phoneNumberValidation", function (value, element) {
			return this.optional(element) || /^\(?[0,62]+\)?[-.\s]?\d+[-.\s]?\d+$/i.test(value);
		}, "Kolom ini harus diisi nomor handphone yang benar.");
		
		$.validator.addMethod('latCoord', function(value, element) {
			//console.log(this.optional(element))
			return this.optional(element) || value.length >= 4 && /^(?=.)-?((8[0-5]?)|([0-7]?[0-9]))?(?:\.[0-9]{1,20})?$/.test(value);
		}, 'Format Latitude salah.');
		
		$.validator.addMethod('longCoord', function(value, element) {
			//console.log(this.optional(element))
			return this.optional(element) || value.length >= 4 && /^(?=.)-?((0?[8-9][0-9])|180|([0-1]?[0-7]?[0-9]))?(?:\.[0-9]{1,20})?$/.test(value);
		}, 'Format Longitude salah.');
		
		var validate_start = $("#FormSubmit").validate({
			ignore: "",
			rules: {
				tipe_provider: {
					required: true
				},
				tipe_rekanan: {
					required: true
				},
				alamat: {
					required: true,
					customAddressValidation: true
				},
				kota: {
					required: true
				},
				provinsi: {
					required: true
				},
				latitude: {
					required: true,
					latCoord: true
				},
				longitude: {
					required: true,
					longCoord: true
				},
				nama_provider: {
					required: true
				},
				no_telp1: {
					required: true,
					phoneNumberValidation: true
				},
// 				no_telp2: {
// 					required: true,
// 					phoneNumberValidation: true
// 				},
// 				no_telp3: {
// 					required: true,
// 					phoneNumberValidation: true
// 				},
// 				no_telp4: {
// 					required: true,
// 					phoneNumberValidation: true
// 				},
// 				website: {
// 					required: true,
// 					url: true
// 				},
				perawatan_kelas1: {
					required: true
				},
				perawatan_kelas2: {
					required: true
				},
				perawatan_kelas3: {
					required: true
				},
				perawatan_presiden: {
					required: true
				},
				perawatan_svip: {
					required: true
				},
				perawatan_utama: {
					required: true
				},
				perawatan_vip: {
					required: true
				},
				perawatan_vvip: {
					required: true
				},
				persalinan_kelas1: {
					required: true
				},
				persalinan_kelas2: {
					required: true
				},
				persalinan_kelas3: {
					required: true
				},
				persalinan_presiden: {
					required: true
				},
				persalinan_svip: {
					required: true
				},
				persalinan_utama: {
					required: true
				},
				persalinan_vip: {
					required: true
				},
				persalinan_vvip: {
					required: true
				},
				persalinan_sectio_kelas1: {
					required: true
				},
				persalinan_sectio_kelas2: {
					required: true
				},
				persalinan_sectio_kelas3: {
					required: true
				},
				persalinan_sectio_presiden: {
					required: true
				},
				persalinan_sectio_svip: {
					required: true
				},
				persalinan_sectio_utama: {
					required: true
				},
				persalinan_sectio_vip: {
					required: true
				},
				persalinan_sectio_vvip: {
					required: true
				},
				rawat_inap_kelas1: {
					required: true
				},
				rawat_inap_kelas2: {
					required: true
				},
				rawat_inap_kelas3: {
					required: true
				},
				rawat_inap_presiden: {
					required: true
				},
				rawat_inap_svip: {
					required: true
				},
				rawat_inap_utama: {
					required: true
				},
				rawat_inap_vip: {
					required: true
				},
				rawat_inap_vvip: {
					required: true
				},
				rawat_jalan_dokter_spesialis: {
					required: true
				},
				rawat_jalan_dokter_umum: {
					required: true
				}
			},
			messages: {},
			highlight: function (element, errorClass) {
				$(element).closest(".form-group").addClass("has-error").css("color", "");
			},
			unhighlight: function (element, errorClass) {
				$(element).closest(".form-group").removeClass("has-error").css("color", "");
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function (error, element) {
				error.css({
					'font-size': '10px',
					'display': 'inline',
					'color': 'red'
				});
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				}
				else {
					error.insertAfter(element);
				}
			},
			submitHandler: function (form) {
				var input_parameter = {};
				input_parameter.tipe_provider = $('#tipe_provider').val();
				input_parameter.tipe_rekanan = $('#tipe_rekanan').val();
				if(SubmitType == 'update' || (typeof SubmitType != 'undefined' && SubmitType != null) ){
					input_parameter.provider_id = $('#provider_id').val();
				}
				
				input_parameter.alamat = $('#alamat').val();
				input_parameter.provinsi = $('#provinsi').val();
				input_parameter.kota = $('#kota').val();
				input_parameter.latitude = $('#latitude').val();
				input_parameter.longitude = $('#longitude').val();
				input_parameter.nama_provider = $('#nama_provider').val();
				
				input_parameter.no_telp1 = $('#no_telp1').val();
				input_parameter.no_telp2 = $('#no_telp2').val();
				input_parameter.no_telp3 = $('#no_telp3').val();
				input_parameter.no_telp4 = $('#no_telp4').val();
				
				var perawatan = {};
				perawatan.perawatan_kelas1 = $('#perawatan_kelas1').val().replace(/\./g, '');
				perawatan.perawatan_kelas2 = $('#perawatan_kelas2').val().replace(/\./g, '');
				perawatan.perawatan_kelas3 = $('#perawatan_kelas3').val().replace(/\./g, '');
				perawatan.perawatan_presiden = $('#perawatan_presiden').val().replace(/\./g, '');
				perawatan.perawatan_svip = $('#perawatan_svip').val().replace(/\./g, '');
				perawatan.perawatan_utama = $('#perawatan_utama').val().replace(/\./g, '');
				perawatan.perawatan_vip = $('#perawatan_vip').val().replace(/\./g, '');
				perawatan.perawatan_vvip = $('#perawatan_vvip').val().replace(/\./g, '');
				input_parameter.perawatan = perawatan;
				
				var persalinan = {};
				persalinan.persalinan_kelas1 = $('#persalinan_kelas1').val().replace(/\./g, '');
				persalinan.persalinan_kelas2 = $('#persalinan_kelas2').val().replace(/\./g, '');
				persalinan.persalinan_kelas3 = $('#persalinan_kelas3').val().replace(/\./g, '');
				persalinan.persalinan_presiden = $('#persalinan_presiden').val().replace(/\./g, '');
				persalinan.persalinan_svip = $('#persalinan_svip').val().replace(/\./g, '');
				persalinan.persalinan_utama = $('#persalinan_utama').val().replace(/\./g, '');
				persalinan.persalinan_vip = $('#persalinan_vip').val().replace(/\./g, '');
				persalinan.persalinan_vvip = $('#persalinan_vvip').val().replace(/\./g, '');
				input_parameter.persalinan = persalinan;
				
				var persalinan_sectio = {};
				persalinan_sectio.persalinan_sectio_kelas1 = $('#persalinan_sectio_kelas1').val().replace(/\./g, '');
				persalinan_sectio.persalinan_sectio_kelas2 = $('#persalinan_sectio_kelas2').val().replace(/\./g, '');
				persalinan_sectio.persalinan_sectio_kelas3 = $('#persalinan_sectio_kelas3').val().replace(/\./g, '');
				persalinan_sectio.persalinan_sectio_presiden = $('#persalinan_sectio_presiden').val().replace(/\./g, '');
				persalinan_sectio.persalinan_sectio_svip = $('#persalinan_sectio_svip').val().replace(/\./g, '');
				persalinan_sectio.persalinan_sectio_utama = $('#persalinan_sectio_utama').val().replace(/\./g, '');
				persalinan_sectio.persalinan_sectio_vip = $('#persalinan_sectio_vip').val().replace(/\./g, '');
				persalinan_sectio.persalinan_sectio_vvip = $('#persalinan_sectio_vvip').val().replace(/\./g, '');
				input_parameter.persalinan_sectio = persalinan_sectio;
				
				var rawat_inap = {};
				rawat_inap.rawat_inap_kelas1 = $('#rawat_inap_kelas1').val().replace(/\./g, '');
				rawat_inap.rawat_inap_kelas2 = $('#rawat_inap_kelas2').val().replace(/\./g, '');
				rawat_inap.rawat_inap_kelas3 = $('#rawat_inap_kelas3').val().replace(/\./g, '');
				rawat_inap.rawat_inap_presiden = $('#rawat_inap_presiden').val().replace(/\./g, '');
				rawat_inap.rawat_inap_svip = $('#rawat_inap_svip').val().replace(/\./g, '');
				rawat_inap.rawat_inap_utama = $('#rawat_inap_utama').val().replace(/\./g, '');
				rawat_inap.rawat_inap_vip = $('#rawat_inap_vip').val().replace(/\./g, '');
				rawat_inap.rawat_inap_vvip = $('#rawat_inap_vvip').val().replace(/\./g, '');
				input_parameter.rawat_inap = rawat_inap;
				
				var rawat_jalan = {};
				rawat_jalan.rawat_jalan_dokter_spesialis = $('#rawat_jalan_dokter_spesialis').val().replace(/\./g, '');
				rawat_jalan.rawat_jalan_dokter_umum = $('#rawat_jalan_dokter_umum').val().replace(/\./g, '');
				input_parameter.rawat_jalan = rawat_jalan;
				
				input_parameter.APIUrl = "/provider/insert-update-provider";
				input_parameter.method = "POST";
				console.log(input_parameter);
				var message_type_success = "";
				var message_type_failed = "";
				if(SubmitType == 'new'){
					message_type_success = "Insert Provider berhasil.";
					message_type_failed = "Insert Provider gagal.";
					
					$.ajax({
						url : url_local_web+"/APICall",
						type: 'POST',
						dataType: 'JSON',
						data: JSON.stringify(input_parameter),
						headers: {
							"Content-Type": "application/json"
						},
						beforeSend: function() {},
					}).done(function (data) {
						reset_form();
						$('#addProvider').modal('hide');
						
						if(data.error_schema.error_code == "ERR-00-000"){
							Swal.fire({
								type: 'success',
								title: 'Alert',
								text: message_type_success
							});
						}
						else{
							Swal.fire({
								type: 'error',
								title: 'Alert',
								text: message_type_failed
							});
						}
						
						datatable.ajax.reload();
					}).fail(function (jqXHR, textStatus) {
						reset_form();
						console.log("failed " + jqXHR + " " + textStatus);
						datatable.ajax.reload();
					});
					
				}
				else{
					message_type_success = "Update Provider berhasil.";
					message_type_failed = "Update Provider gagal.";
					
					Swal.fire({
						title: 'Are you sure?',
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Save'
					}).then((result) => {
						if (result.value) {
							$.ajax({
								url : url_local_web+"/APICall",
								type: 'POST',
								dataType: 'JSON',
								data: JSON.stringify(input_parameter),
								headers: {
									"Content-Type": "application/json"
								},
								beforeSend: function() {},
							}).done(function (data) {
								reset_form();
								$('#addProvider').modal('hide');
								
								if(data.error_schema.error_code == "ERR-00-000"){
									Swal.fire({
										type: 'success',
										title: 'Alert',
										text: message_type_success
									});
								}
								else{
									Swal.fire({
										type: 'error',
										title: 'Alert',
										text: message_type_failed
									});
								}
								
								datatable.ajax.reload();
							}).fail(function (jqXHR, textStatus) {
								reset_form();
								console.log("failed " + jqXHR + " " + textStatus);
								datatable.ajax.reload();
							});
						}
					});
					
				}
				
				
			},
			invalidHandler: function (event, validator) {
				var errors = validator.numberOfInvalids();
				console.log("error " + errors);
			}
		});
		jQuery.extend(jQuery.validator.messages, {
			number: "Please enter only number."
		});
		validate_start.resetForm();
	}
	
	</script>
</body>
</html>
