package adapter;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import database.QueryExecuteAdapter;

public class UserAdapter {

    public static List<model.mdlMenu> LoadAllowedMenuforAccess(String lUserID) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
	String sql = "";
	CachedRowSet rowset = null;
	List<model.mdlMenu> mdlMenuList = new ArrayList<model.mdlMenu>();
	try {
	    sql = "SELECT a.MenuID, a.RoleID FROM ms_access_role a INNER JOIN ms_user_cms b ON b.RoleID=a.RoleID WHERE a.IsAccess = 1 AND b.username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", lUserID));

	    rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (rowset.next()) {
		model.mdlMenu mdlMenu = new model.mdlMenu();
		mdlMenu.setMenuID(rowset.getString("MenuID"));
		mdlMenu.setRole(rowset.getString("RoleID"));
		mdlMenuList.add(mdlMenu);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, sql, lUserID);
	}

	return mdlMenuList;
    }
    
    public static String GetUserRole(String username){
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
	String sql = "";
	CachedRowSet rowset = null;
	String role_id = "";
	try {
	    sql = "SELECT RoleID FROM ms_user_cms WHERE username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));

	    rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (rowset.next()) {
		role_id = rowset.getString("RoleID");
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, sql, username);
	}

	return role_id;
    }
}
