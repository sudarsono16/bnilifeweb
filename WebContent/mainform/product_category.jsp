<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="mainform/plugins/datatables/extensions/Responsive/css/responsive.bootstrap4.min.css">
<link href="mainform/css/quill.snow.css" rel="stylesheet">
<style>
table.dataTable tbody td {
	word-break: break-word;
	vertical-align: top;
}
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
		<div class="container-fluid">
			<div class="animated fadeIn">
				<div class="search-container row">
					<div class="col-lg-3 col-md-3 col-12">
						<button class="btn btn-block btn-warning" id="SubmitNew" type="button" data-toggle="modal" data-target="#addModal">
							<i class="fa fa-plus-circle"></i> Add Product Category
						</button>
					</div>
				</div>
				<!-- SEARCH-CONTAINER -->
			</div>
		</div>
		<div class="container-fluid">
			<div class="animated fadeIn">
				<table class="table table-striped table-bordered sortable" id="main_table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Name English</th>
							<th>Icon</th>
							<th>Order</th>
							<th></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
		</main>
		<!-- Edit Information Modal -->
		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<form id="FormSubmit">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<input type="hidden" id="product_category_id"> <input type="hidden" id="submit_type">
							<div class="card-body">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Product Category Name</span>
									</div>
									<input type="text" id="product_category_name" name="product_category_name" class="form-control"
										style="text-transform: capitalize;">
									<!-- 									<select class="custom-select form-control" name="product_category_name" id="product_category_name"></select> -->
								</div>
								<br>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Product Category Name English</span>
									</div>
									<input type="text" id="product_category_name_english" name="product_category_name_english" class="form-control"
										style="text-transform: capitalize;">
									<!-- 									<select class="custom-select form-control" name="product_category_name" id="product_category_name"></select> -->
								</div>
								<br>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Order</span>
									</div>
									<input type="number" id="product_category_order" name="product_category_order" class="form-control"
										style="text-transform: capitalize;">
								</div>
								<br>
								<div class="form-group">
									<label for="term_type">Product Category Icon</label>
								</div>
								<div class="form-group">
									<div class="container">
										<div class="row">
											<div class="col-md-4">
												<img id="image-template" class="rounded float-left img-fluid"
													src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_164e3f66702%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_164e3f66702%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274.4296875%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
													data-holder-rendered="true" style="margin: 5px;">
											</div>
											<div class="col-md-8 my-auto">
												<div class="input-group">
													<input type="file" class="custom-file-input " name="customFile" id="customFile" onchange="change_image(this)">
													<label class="custom-file-label" for="customFile">Choose file</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button class="btn btn-primary" id="SubmitButton" type="submit">Save changes</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- MODAL -->
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script src="mainform/plugins/quill.min.js"></script>
	<script>
	var datatable = {};
	$(document).ready(function() {
		datatable = $('#main_table').DataTable({
			language: {
				"processing": "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
			},
			autoWidth: false,
			processing: true,
			searching: true,
			destroy: true,
			cache: true,
			contentType: "application/json; charset=utf-8",
			PaginationType: "full",
			responsive: true,
			ajax: {
				"url": url_local_web+"/APICall",
				"type": "POST",
				"headers": {
					'Content-Type': 'application/json'
				},
				"data": function () {
					var input_parameter = {};
					input_parameter.APIUrl = "/product/get-product-category-list";
					input_parameter.method = "GET";
					return JSON.stringify(input_parameter);
				},
				"dataSrc": function (json) {
					var output_array_list = [];
					console.log(json);
					$.each(json.output_schema.items, function (index, value) {
						var output_array = {};

						output_array.product_category_id = value.product_category_id;
						output_array.product_category_name = value.product_category_name;
						output_array.product_category_name_english = value.product_category_name_english;
						output_array.product_category_icon = value.product_category_icon;
						output_array.product_category_order = value.product_category_order;
						output_array_list.push(output_array);
					});
					
					return output_array_list;
				}
			},
			responsive: {
				breakpoints: [
					{ name: 'desktop',  width: Infinity },
					{ name: 'tablet-l', width: 1024 },
					{ name: 'tablet-p', width: 768 },
					{ name: 'mobile-l', width: 480 },
					{ name: 'mobile-p', width: 320 }
				]
			},
			order: [[ 4, "asc" ]],
			columns: [
				{"data": "product_category_id"},
				{"data": "product_category_name"},
				{"data": "product_category_name_english"},
				{
					"data": "product_category_icon",
					"render": function(data, type, row){
						return '<img src=' + data +' align="middle" style="max-width:50px;">';
						
					}
				},
				{"data": "product_category_order"},
				{
					"data": "product_category_id",
					"render": function (data, type, row) {
						var button_string = "";
						button_string += '<span class="input-group-btn">';
						button_string += '<button type="button" class="btn btn-success btn-sm button_detail" style="width: 35px;" data-id="'+data+'"><i class="fa fa-search-plus"></i></button>';
						button_string += '<button type="button" class="btn btn-info btn-sm button_update" style="width: 35px;" data-id="'+data+'"><i class="fa fa-edit"></i></button>';
						button_string += '<button type="button" class="btn btn-danger btn-sm button_delete" style="width: 35px;" data-id="'+data+'"><i class="fa fa-trash-o"></i></button>';
						button_string += '</span>';
						return button_string;
					}
				}
			],
			columnDefs: [
				{
					targets: '_all',
					searchable: true,
					sortable: true,
					visible: true,
					defaultContent: ""
				},
				{
					targets: [5],
					searchable: false,
					sortable: false,
					visible: true
				}
			],
			preDrawCallback: function( settings ) {}
		});
		
		
	});
	
	function change_image(input) {
		var file = input.files && input.files[0];
		if (file) {
            $(input).next('.custom-file-label').html(file.name);
            
			var img = new Image();
	        img.src = window.URL.createObjectURL( file );
	        img.onload = function() {
            	loadMime(file, function(mime) {
            		if(mime == "image/jpeg" || mime == "image/png"){
            			var reader = new FileReader();
    	    			reader.onload = function (e) {
    	    				$(input).closest('.row').find('#image-template').attr('src', e.target.result);
    	    			};
    	    			reader.readAsDataURL(file);
            		}
            		else{
            			alert("Format gambar harus JPG / PNG");
            		}
                });
	        };
			
		}
	}
	
	function loadMime(file, callback) {
	    
	    //List of known mimes
	    var mimes = [
	        {
	            mime: 'image/jpeg',
	            pattern: [0xFF, 0xD8, 0xFF],
	            mask: [0xFF, 0xFF, 0xFF],
	        },
	        {
	            mime: 'image/png',
	            pattern: [0x89, 0x50, 0x4E, 0x47],
	            mask: [0xFF, 0xFF, 0xFF, 0xFF],
	        }
	        // you can expand this list @see https://mimesniff.spec.whatwg.org/#matching-an-image-type-pattern
	    ];

	    function check(bytes, mime) {
	        for (var i = 0, l = mime.mask.length; i < l; ++i) {
	            if ((bytes[i] & mime.mask[i]) - mime.pattern[i] !== 0) {
	                return false;
	            }
	        }
	        return true;
	    }

	    var blob = file.slice(0, 4); //read the first 4 bytes of the file

	    var reader = new FileReader();
	    reader.onloadend = function(e) {
	        if (e.target.readyState === FileReader.DONE) {
	            var bytes = new Uint8Array(e.target.result);

	            for (var i=0, l = mimes.length; i<l; ++i) {
	                //if (check(bytes, mimes[i])) return callback("Mime: " + mimes[i].mime + " <br> Browser:" + file.type);
	            	if (check(bytes, mimes[i])) return callback(file.type);
	            }

	            //return callback("Mime: unknown <br> Browser:" + file.type);
	            return file.type;
	        }
	    };
	    reader.readAsArrayBuffer(blob);
	}
	
	$('#main_table tbody').on('click', '.button_detail',function () {
		$('#SubmitButton').hide();
		
		var current_data = "";
		if ($(this).parent().hasClass("dtr-data")) {
			current_data = datatable.row($(this)).data();
		}
		else {
			current_data = datatable.row($(this).closest('tr')).data();
		}

		fillData(current_data);
		
		$('#addModal').modal({
			backdrop: 'static',
			keyboard: false, 
			show: true
		});
		
	});
	
	$('#main_table tbody').on('click', '.button_update',function () {
		$('#SubmitButton').show();
		validate_start_function('update');
		
		var current_data = "";
		if ($(this).parent().hasClass("dtr-data")) {
			current_data = datatable.row($(this)).data();
		}
		else {
			current_data = datatable.row($(this).closest('tr')).data();
		}
		
		$('#product_category_id').val(current_data.product_category_id);
		
		fillData(current_data);
		
		$('#addModal').modal({
			backdrop: 'static',
			keyboard: false, 
			show: true
		});
		
	});
	
	$('#main_table tbody').on('click', '.button_delete', function () {
		Swal.fire({
			title: 'Are you sure?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete'
		}).then((result) => {
			if (result.value) {
				var current_data = "";
				if ($(this).parent().hasClass("dtr-data")) {
					current_data = datatable.row($(this)).data();
				}
				else {
					current_data = datatable.row($(this).closest('tr')).data();
				}
				
				var input_parameter = {};
				input_parameter.APIUrl = "/product/delete-product-category?id="+current_data.product_category_id;
				input_parameter.method = "GET";
				
				$.ajax({
					url : url_local_web+"/APICall",
					type: 'POST',
					data: JSON.stringify(input_parameter),
					dataType: "json",
					headers: {
						'Content-Type': 'application/json'
					},
					beforeSend: function() {
					},
				}).done(function (data) {
					console.log(data);
					if(data.error_schema.error_code == "ERR-00-000"){						
						Swal.fire({
							allowOutsideClick: false,
							type: 'success',
							title: 'Alert',
							text: "Delete success"
						}).then((result) => {
							if (result.value) {
								location.reload();
							}
						});
					}
					else{
						Swal.fire({
							type: 'error',
							title: 'Alert',
							text: "Delete failed"
						});
					}
					datatable.ajax.reload();
				}).fail(function (jqXHR, textStatus) {
					console.log("failed " + jqXHR + " " + textStatus);
					Swal.fire({
						type: 'error',
						title: 'Alert',
						text: "Delete error"
					});
				});
				
			}
		});		
	});
	
	function fillData(current_data){
		console.log(current_data);
		$('#product_category_name').val(current_data.product_category_name);
		$('#product_category_name_english').val(current_data.product_category_name_english);
		$('#product_category_order').val(current_data.product_category_order);
		$('#image-template').attr('src', current_data.product_category_icon);
	}
	
	$('#addModal').on('hide.bs.modal', function () {
		reset_form();
	});
	
	$('#SubmitNew').on('click', function () {
		reset_form();
		validate_start_function('new');
	});
	
	function reset_form(){
		$('#SubmitButton').show();
		$("#FormSubmit")[0].reset();
		$form = $('form');
		$form.find('.control-group').removeClass('.error');
		$form.find('.has-error').removeClass("has-error");
		$form.find('.has-success').removeClass("has-success");
		$form.find('.form-control-feedback').remove();
	}
	
	function validate_start_function(SubmitType){
		$.validator.addMethod("customLetterValidation", function (value, element) {
			return this.optional(element) || /^[a-zA-Z0-9 ]+$/i.test(value);
		}, "Kolom ini hanya boleh diisi alfanumerik dan spasi.");
		
		var validate_start = $("#FormSubmit").validate({
			ignore: [],
			rules: {
				product_category_name: {
					required: true
				},
				product_category_name_english: {
					required: true
				},
				product_category_order: {
					required: true
				}
			},
			messages: {},
			highlight: function (element, errorClass) {
				$(element).closest(".form-group").addClass("has-error").css("color", "");
			},
			unhighlight: function (element, errorClass) {
				$(element).closest(".form-group").removeClass("has-error").css("color", "");
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function (error, element) {
				error.css({
					'font-size': '10px',
					'display': 'inline',
					'color': 'red'
				});
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				}
				else {
					error.insertAfter(element);
				}
			},
			submitHandler: function (form) {
				var input_parameter = {};
				if(SubmitType == 'update' || (typeof SubmitType != 'undefined' && SubmitType != null) ){
					input_parameter.product_category_id = $('#product_category_id').val();
				}
				
				input_parameter.product_category_name = $('#product_category_name').val();
				input_parameter.product_category_name_english = $('#product_category_name_english').val();
				input_parameter.product_category_order = $('#product_category_order').val();
				
				input_parameter.APIUrl = "/product/insert-update-product-category";
				input_parameter.method = "POST";
				
				var formData = new FormData();
				formData.append('json', JSON.stringify(input_parameter));
				if (($("#customFile"))[0].files.length > 0) {
					formData.append('file', $('#customFile')[0].files[0]);
				}
				
				var message_type_success = "";
				var message_type_failed = "";
				if(SubmitType == 'new'){
					message_type_success = "Insert data berhasil.";
					message_type_failed = "Insert data gagal.";
				}
				else{
					message_type_success = "Update data berhasil.";
					message_type_failed = "Update data gagal.";
				}
				
				$.ajax({
						url : url_local_web+"/APICall",
						type: 'POST',
						enctype: 'multipart/form-data',
						cache: false,
						contentType: false,
						processData: false,
						data: formData,
						dataType: "json",
						beforeSend: function() {
						},
					}).done(function (data) {
					reset_form();
					$('#addModal').modal('hide');
					
					if(data.error_schema.error_code == "ERR-00-000"){
						Swal.fire({
							allowOutsideClick: false,
							type: 'success',
							title: 'Alert',
							text: message_type_success
						}).then((result) => {
							if (result.value) {
								reset_form();
								location.reload();
							}
						});
					}
					else{
						Swal.fire({
							type: 'error',
							title: 'Alert',
							text: message_type_failed
						});
					}
					datatable.ajax.reload();
				}).fail(function (jqXHR, textStatus) {
					reset_form();
					console.log("failed " + jqXHR + " " + textStatus);
					datatable.ajax.reload();
				});
			},
			invalidHandler: function (event, validator) {
				var errors = validator.numberOfInvalids();
				console.log("error " + errors);
			}
		});
		jQuery.extend(jQuery.validator.messages, {
			number: "Please enter only number."
		});
		validate_start.resetForm();
		console.log("E");
	}
	
	</script>
</body>
</html>
