<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
<style>
.a
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
		<div class="customization container-fluid">
			<div class="animated fadeIn">
				<div class="row">
					<div class="col-12">
						<h4>Icons</h4>
						<p>
							Resolution <b>680 x 510 px</b>
							<br>
							File Format <b>PNG</b>
						</p>
						<div class="menu-icons d-flex flex-wrap justify-content-start">
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Activities</div>
								<img class="icon" src="${image_url}/buttons/activities.png" data-filename="activities.png" alt="Activities">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="activities.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Beneficiary</div>
								<img class="icon" src="${image_url}/buttons/beneficiary.png" data-filename="beneficiary.png" alt="Beneficiary">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="beneficiary.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Benefit</div>
								<img class="icon" src="${image_url}/buttons/benefit.png" data-filename="benefit.png" alt="Benefit">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="benefit.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Commission</div>
								<img class="icon" src="${image_url}/buttons/comission.png" data-filename="comission.png" alt="Commission">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="comission.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Contest</div>
								<img class="icon" src="${image_url}/buttons/contest.png" data-filename="contest.png" alt="Contest">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="contest.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Customer</div>
								<img class="icon" src="${image_url}/buttons/customer.png" data-filename="customer.png" alt="Customer">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="customer.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Help</div>
								<img class="icon" src="${image_url}/buttons/help.png" data-filename="help.png" alt="Help">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="help.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Insured</div>
								<img class="icon" src="${image_url}/buttons/insured.png" data-filename="insured.png" alt="Insured">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="insured.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Investment</div>
								<img class="icon" src="${image_url}/buttons/investment.png" data-filename="investment.png" alt="Investment">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="investment.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Membership</div>
								<img class="icon" src="${image_url}/buttons/membership.png" data-filename="membership.png" alt="Membership">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="membership.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Monthly Report</div>
								<img class="icon" src="${image_url}/buttons/monthly-report.png" data-filename="monthly-report.png" alt="Monthly Report">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="monthly-report.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">More</div>
								<img class="icon" src="${image_url}/buttons/more.png" data-filename="more.png" alt="More">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="more.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">My Claim</div>
								<img class="icon" src="${image_url}/buttons/myclaim.png" data-filename="myclaim.png" alt="My Claim">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="myclaim.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">NAB</div>
								<img class="icon" src="${image_url}/buttons/nab.png" data-filename="nab.png" alt="NAB">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="nab.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Pipelines</div>
								<img class="icon" src="${image_url}/buttons/pipelines.png" data-filename="pipelines.png" alt="Pipelines">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="pipelines.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Policy Details</div>
								<img class="icon" src="${image_url}/buttons/policy-details.png" data-filename="policy-details.png" alt="Policy Details">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="policy-details.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Policy Owner</div>
								<img class="icon" src="${image_url}/buttons/policy-owner.png" data-filename="policy-owner.png" alt="Policy Owner">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="policy-owner.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Policy Status</div>
								<img class="icon" src="${image_url}/buttons/policy-status.png" data-filename="policy-status.png" alt="Policy Status">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="policy-status.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Production</div>
								<img class="icon" src="${image_url}/buttons/production.png" data-filename="production.png" alt="Production">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="production.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Products</div>
								<img class="icon" src="${image_url}/buttons/products.png" data-filename="products.png" alt="Products">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="products.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Proposal Status</div>
								<img class="icon" src="${image_url}/buttons/proposal-status.png" data-filename="proposal-status.png" alt="Proposal Status">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="proposal-status.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Providers</div>
								<img class="icon" src="${image_url}/buttons/providers.png" data-filename="providers.png" alt="Providers">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="providers.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Quotation</div>
								<img class="icon" src="${image_url}/buttons/quotation.png" data-filename="quotation.png" alt="Quotation">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="quotation.png">Save Changes</button>
							</div>
							<div class="icon-card d-block mx-2">
								<div class="icon-card-title">Sales Force</div>
								<img class="icon" src="${image_url}/buttons/sales-force.png" data-filename="sales-force.png" alt="Sales Force">
								<input type='file' onchange="change_image(this)" />
								<button type="button" class="btn btn-primary d-block" data-filename="sales-force.png">Save Changes</button>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		</main>
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script>
	
	$("input").val(null);
	$('button').prop('disabled', true);
	
	$('button').on( "click", function() {
		if($(this).prevAll('input:first').get(0).files.length > 0){
			var input_parameter = {};
			input_parameter.folder = "buttons";
			input_parameter.filename = $(this).data("filename");
			
			var url = url_local_web+"/APICall";
			input_parameter.APIUrl = "/update-image";
			input_parameter.method = "POST";
			
			var formData = new FormData();
			formData.append('json', JSON.stringify(input_parameter));
			formData.append('file', $(this).prevAll('input:first')[0].files[0] );
			
			$.ajax({
				url : url,
				type: 'POST',
				enctype: 'multipart/form-data',
				cache: false,
				contentType: false,
				processData: false,
				data: formData,
				dataType: "json",
				beforeSend: function() {
				},
			}).done(function (data) {
				console.log(data);
				if(data.error_schema.error_code == "ERR-00-000"){
					Swal.fire({
						allowOutsideClick: false,
						type: 'success',
						title: 'Alert',
						text: "Proses data sukses"
					}).then((result) => {
						if (result.value) {
							location.reload();
						}
					});
				}
				else{
					Swal.fire({
						allowOutsideClick: false,
						type: 'error',
						title: 'Alert',
						text: "Proses data gagal"
					}).then((result) => {
						if (result.value) {
							location.reload();
						}
					});
				}
				
			}).fail(function (jqXHR, textStatus) {
				console.log("failed " + jqXHR + " " + textStatus);
				Swal.fire({
					type: 'error',
					title: 'Alert',
					text: "Gagal memulai proses pengiriman data"
				});
			});
		}
		else{
			alert("Gambar belum dimasukkan");
		}
	});
	
	function change_image(input) {
		var image_base_url = "${image_url}/buttons/";
		
		var file = input.files && input.files[0];
		if (file) {
			var img = new Image();
	        img.src = window.URL.createObjectURL( file );
// 			console.log(file.type);
	        img.onload = function() {
	            var width = img.naturalWidth, height = img.naturalHeight;
				//console.log("width: "+width+" height: "+height);
				
	            window.URL.revokeObjectURL( img.src );

	            if( width <= 680 && height <= 510 ) {
	            	loadMime(file, function(mime) {
	            		//console.log("image type: "+mime);
	            		if(mime == "image/png"){
	            			var reader = new FileReader();
	    	    			reader.onload = function (e) {
	    	    				$(input).prevAll('.icon').attr('src', e.target.result);
	    	    				//console.log(e.target.result);
	    	    			};
	    	    			reader.readAsDataURL(file);
	    	    			
	    	    			$(input).nextAll('button:first').prop('disabled',false);
	            		}
	            		else{
	            			alert("Format gambar tidak sesuai dengan persyaratan yang ada ");
	            		}
	                });
	            	
	            	
	            }
	            else {
	                alert("Resolusi gambar tidak sesuai dengan persyaratan yang ada ");
	            }
	        };
			
		}
	}
	
	function loadMime(file, callback) {
	    
	    //List of known mimes
	    var mimes = [
	        {
	            mime: 'image/jpeg',
	            pattern: [0xFF, 0xD8, 0xFF],
	            mask: [0xFF, 0xFF, 0xFF],
	        },
	        {
	            mime: 'image/png',
	            pattern: [0x89, 0x50, 0x4E, 0x47],
	            mask: [0xFF, 0xFF, 0xFF, 0xFF],
	        }
	        // you can expand this list @see https://mimesniff.spec.whatwg.org/#matching-an-image-type-pattern
	    ];

	    function check(bytes, mime) {
	        for (var i = 0, l = mime.mask.length; i < l; ++i) {
	            if ((bytes[i] & mime.mask[i]) - mime.pattern[i] !== 0) {
	                return false;
	            }
	        }
	        return true;
	    }

	    var blob = file.slice(0, 4); //read the first 4 bytes of the file

	    var reader = new FileReader();
	    reader.onloadend = function(e) {
	        if (e.target.readyState === FileReader.DONE) {
	            var bytes = new Uint8Array(e.target.result);

	            for (var i=0, l = mimes.length; i<l; ++i) {
	                //if (check(bytes, mimes[i])) return callback("Mime: " + mimes[i].mime + " <br> Browser:" + file.type);
	            	if (check(bytes, mimes[i])) return callback(file.type);
	            }

	            //return callback("Mime: unknown <br> Browser:" + file.type);
	            return file.type;
	        }
	    };
	    reader.readAsArrayBuffer(blob);
	}
	</script>
</body>
</html>
