package adapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;

public class MultipartUtility {
	private static String boundary;
	private static final String LINE_FEED = "\r\n";
	private static HttpURLConnection httpConn;
	private String charset;
	private OutputStream outputStream;
	private PrintWriter writer;
	
	public static HttpURLConnection setRequestPropertyBasic() throws IOException {
		URL url = new URL("http://35.240.192.212:8997/BNILifeService/get-token");
//		URL url = new URL("http://localhost:8080/BNILifeService/get-token");
		httpConn = (HttpURLConnection) url.openConnection();
		httpConn.setUseCaches(false);
		httpConn.setRequestMethod("POST");
		httpConn.setDoOutput(true);
		httpConn.setDoInput(true);
		
		model.mdlKey mdlKey = adapter.WebTokenAdapter.GetBasicToken();
		String originalInput = mdlKey.ClientID+";"+mdlKey.ClientSecret;
		String encodedString = Base64.getEncoder().encodeToString(originalInput.getBytes());
		String BasicAuthorization = "Basic "+encodedString;
		
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("grant_type", "client_credentials");

		StringBuilder postParam = new StringBuilder();
		for (Map.Entry<String,Object> param : params.entrySet()) {
			if (postParam.length() != 0) postParam.append('&');
			postParam.append(URLEncoder.encode(param.getKey(), "UTF-8"));
			postParam.append('=');
			postParam.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
		}
		byte[] postDataBytes = postParam.toString().getBytes("UTF-8");
		
		httpConn.setRequestProperty("Authorization", BasicAuthorization);
		httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		httpConn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length) );
		httpConn.getOutputStream().write(postDataBytes);
		
		return httpConn;
	}
	
	public static HttpURLConnection setRequestPropertyBearerPost(String requestURL, model.mdlToken BearerKeySet, String jsonString) throws IOException, NamingException {
		Context context = (Context) new InitialContext().lookup("java:comp/env");
		String base_url = (String) context.lookup("base_url");
		String method = "POST";
		URL url = new URL(base_url+requestURL);
		httpConn = (HttpURLConnection) url.openConnection();
		httpConn.setUseCaches(false);
		httpConn.setRequestMethod(method);
		httpConn.setDoOutput(true); // indicates POST method
		httpConn.setDoInput(true);
		
		String Authorization = "Bearer "+BearerKeySet.access_token;
		String apiKey = BearerKeySet.api_key;
		String Timestamp = helper.DateHelper.GetDateTimeNowCustomFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		String stringToSign = method.toUpperCase() + ";" + requestURL + ";" + BearerKeySet.access_token + ";" + Timestamp;
		String apiSignature = SHA256Adapter.generateHmacSHA256Signature(stringToSign, apiKey);
		
		httpConn.setRequestProperty("Accept", "application/json");
		httpConn.setRequestProperty("Authorization", Authorization);
		httpConn.setRequestProperty("Key", apiKey);
		httpConn.setRequestProperty("Timestamp", Timestamp);
		httpConn.setRequestProperty("Signature", apiSignature);
		httpConn.setRequestProperty("Content-Type", "application/json; utf-8");
		
		if (jsonString != null) {
			httpConn.setRequestProperty("Content-length", jsonString.getBytes().length + "");
			OutputStream outputStream = httpConn.getOutputStream();
			outputStream.write(jsonString.getBytes("UTF-8"));
			outputStream.close();
		}
		
		return httpConn;
	}
	
	public static HttpURLConnection setRequestPropertyBearerPostMultipart(String requestURL, model.mdlToken BearerKeySet) throws IOException, NamingException {
		Context context = (Context) new InitialContext().lookup("java:comp/env");
		String base_url = (String) context.lookup("base_url");
		String method = "POST";
		URL url = new URL(base_url+requestURL);
		httpConn = (HttpURLConnection) url.openConnection();
		httpConn.setUseCaches(false);
		httpConn.setRequestMethod(method);
		httpConn.setDoOutput(true);
		httpConn.setDoInput(true);
		
		httpConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		
		String Authorization = "Bearer "+BearerKeySet.access_token;
		String apiKey = BearerKeySet.api_key;
		String Timestamp = helper.DateHelper.GetDateTimeNowCustomFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		String stringToSign = method.toUpperCase() + ";" + requestURL + ";" + BearerKeySet.access_token + ";" + Timestamp;
		String apiSignature = SHA256Adapter.generateHmacSHA256Signature(stringToSign, apiKey);
		
		httpConn.setRequestProperty("Accept", "application/json");
		httpConn.setRequestProperty("Authorization", Authorization);
		httpConn.setRequestProperty("Key", apiKey);
		httpConn.setRequestProperty("Timestamp", Timestamp);
		httpConn.setRequestProperty("Signature", apiSignature);
		
		return httpConn;
	}
	
	public static HttpURLConnection setRequestPropertyBearerGet(String requestURL, model.mdlToken BearerKeySet) throws IOException, NamingException {
		Context context = (Context) new InitialContext().lookup("java:comp/env");
		String base_url = (String) context.lookup("base_url");
		String method = "GET";
		URL url = new URL(base_url+requestURL);
		httpConn = (HttpURLConnection) url.openConnection();
		httpConn.setUseCaches(false);
		httpConn.setRequestMethod(method);
		httpConn.setDoOutput(false);
		httpConn.setDoInput(true);
		
		httpConn.setRequestProperty("Content-Type", "application/json; utf-8");
		
		String Accept = "application/json";
		String Authorization = "Bearer "+BearerKeySet.access_token;
		String apiKey = BearerKeySet.api_key;
		String Timestamp = helper.DateHelper.GetDateTimeNowCustomFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		String stringToSign = method.toUpperCase() + ";" + requestURL + ";" + BearerKeySet.access_token + ";" + Timestamp;
		String apiSignature = SHA256Adapter.generateHmacSHA256Signature(stringToSign, apiKey);
		
		httpConn.setRequestProperty("Accept", Accept);
		httpConn.setRequestProperty("Authorization", Authorization);
		httpConn.setRequestProperty("Key", apiKey);
		httpConn.setRequestProperty("Timestamp", Timestamp);
		httpConn.setRequestProperty("Signature", apiSignature);
		
		return httpConn;
	}
	
	public MultipartUtility(HttpServletResponse response, String requestURL, String charset, String method, boolean IsMultipart, model.mdlToken BearerKeySet) throws IOException, NamingException {
		this.charset = charset;
		
		// creates a unique boundary based on time stamp
		boundary = "--" + System.currentTimeMillis() + "--";

		outputStream = setRequestPropertyBearerPostMultipart(requestURL, BearerKeySet).getOutputStream();
		writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);
	}
	
	public void addFormField(String name, String value) {
		writer.append("--" + boundary).append(LINE_FEED);
		writer.append("Content-Disposition: form-data; name=\"" + name + "\"").append(LINE_FEED);
		writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
		writer.append(LINE_FEED);
		writer.append(value).append(LINE_FEED);
		writer.flush();
	}

	public void addFilePart(String fieldName, File uploadFile) throws IOException {
		String fileName = uploadFile.getName();
		writer.append("--" + boundary).append(LINE_FEED);
		writer.append("Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"").append(LINE_FEED);
		writer.append("Content-Type: "+ URLConnection.guessContentTypeFromName(fileName)).append(LINE_FEED);
		writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
		writer.append(LINE_FEED);
		writer.flush();
		
		FileInputStream inputStream = new FileInputStream(uploadFile);
		byte[] buffer = new byte[4096];
		int bytesRead = -1;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
		}
		outputStream.flush();
		inputStream.close();
		
		writer.append(LINE_FEED);
		writer.flush();
	}

	public void addHeaderField(String name, String value) {
		writer.append(name + ": " + value).append(LINE_FEED);
		writer.flush();
	}

	public List<String> finish() throws IOException {
		List<String> response = new ArrayList<String>();
		
		writer.append(LINE_FEED).flush();
		writer.append("--" + boundary + "--").append(LINE_FEED);
		writer.close();
		
		// checks server's status code first
		int status = httpConn.getResponseCode();
		
		if (status == HttpURLConnection.HTTP_OK) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
			String line = null;
			while ((line = reader.readLine()) != null) {
				response.add(line);
			}
			reader.close();
			httpConn.disconnect();
		}
		else {
			throw new IOException("Server returned non-OK status: " + status);
		}
		return response;
	}
	
}