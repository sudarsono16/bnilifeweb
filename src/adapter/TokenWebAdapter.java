package adapter;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.representation.Form;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import model.Globals;

public class TokenWebAdapter {
    final static Logger logger = Logger.getLogger(TokenWebAdapter.class);
    static Client client = Client.create();

    public static model.mdlToken GetTokenWeb() {
	model.mdlToken mdlToken = new model.mdlToken();

	if (Globals.token == null || Globals.token.equalsIgnoreCase("")) {
	    try {
		mdlToken = GetNewToken();
	    } catch (Exception ex) {
		Globals.token = "";
	    }
	} else {
	    try {
		Date dateNow = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date dateToken = df.parse(Globals.tokenDate.replace("T", " "));
		long toMinute = 1000 * 60;
		int dateDiff = (int) (dateNow.getTime() - dateToken.getTime());
		int diffMinute = (int) (dateDiff / toMinute);

		if (diffMinute >= 25) {
		    mdlToken = GetNewToken();
		} else {
		    mdlToken.access_token = Globals.token;
		}
	    } catch (Exception ex) {

	    }
	}
	return mdlToken;
    }

    public static model.mdlToken GetNewToken() throws KeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, GeneralSecurityException, IOException, NamingException {
	model.mdlToken mdlToken = new model.mdlToken();
	String jsonOut = "";

	// Get the base naming context from web.xml
	Context context = (Context) new InitialContext().lookup("java:comp/env");

	// Get a single value from web.xml
	String urlWebServer = (String) context.lookup("base_url");
	String clientID = (String) context.lookup("client_id");
	String clientSecret = (String) context.lookup("client_secret");

	String urlAPI = urlWebServer + "/get-token";
	String authorizationKey = ClientIdAdapter.getAuthorizationKey(clientID, clientSecret);
	WebResource webResource = client.resource(urlAPI);

	// add form variables
	Form form = new Form();
	form.add("grant_type", "client_credentials");

	try {
	    Builder builder = webResource.type("application/x-www-form-urlencoded").header("Authorization", "Basic " + authorizationKey);
	    ClientResponse response = builder.post(ClientResponse.class, form);

	    jsonOut = response.getEntity(String.class);
	    Gson gson = new Gson();
	    model.mdlAPIObjectResult mdlAPIObjectResult = gson.fromJson(jsonOut, model.mdlAPIObjectResult.class);
	    String outputString = gson.toJson(mdlAPIObjectResult.output_schema);
	    mdlToken = gson.fromJson(outputString, model.mdlToken.class);
	    logger.info("SUCCESS GetNewToken = URL :" + urlAPI + ", authorizationKey:" + authorizationKey + ", ClientID:" + clientID + ", ClientSecret:" + clientSecret + ", jsonOut:" + jsonOut);
	} catch (Exception ex) {
	    mdlToken.access_token = "";
	    logger.error("FAILED = URL :" + urlAPI + ", authorizationKey:" + authorizationKey + ", ClientID:" + clientID + ", ClientSecret:" + clientSecret + ", Exception:" + ex.toString(), ex);
	}
	Globals.token = mdlToken.access_token;
	Globals.tokenDate = LocalDateTime.now().toString();
	return mdlToken;
    }

}
