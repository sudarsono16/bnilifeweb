

<%
String user_id = "";
String role_id = "";
if(session.getAttribute("user_id") == null){
	response.sendRedirect("/digiclaim/login");
}
else{
	user_id = (String) session.getAttribute("user_id");
	role_id = (String) session.getAttribute("role_id");
}
%>

<base href="./">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<meta name="description" content="BNI Life Mobile CMS">
<meta name="author" content="MobileCom">
<title>BNI Life Mobile CMS</title>

<!-- Icons-->
<link href="mainform/css/flag-icon.min.css" rel="stylesheet">
<!-- 
<script src="https://kit.fontawesome.com/a93e0a542a.js"></script>
 -->
<link href="mainform/css/simple-line-icons.css" rel="stylesheet">
<link href="mainform/vendors/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">

<!-- Main styles for this application-->
<link href="mainform/css/style.css" rel="stylesheet">
<link href="mainform/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
<link href="mainform/css/custom.css" rel="stylesheet">