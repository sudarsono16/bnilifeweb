<div class="sidebar">
	<nav class="sidebar-nav">
		<ul class="nav">
			<!-- 
			<li class="nav-item">
				<a class="nav-link" href="index.jsp">
					<i class="nav-icon fas fa-file-contract"></i> Claim
					<span class="badge badge-warning">123</span>
				</a>
			</li>
			 -->
			<li id="M001" class="nav-item" style="display: none"><a class="nav-link"
				href="${pageContext.request.contextPath}/dashboard"> <i class="nav-icon fa fa-book"></i> Dashboard
			</a></li>
			<li id="M007" class="nav-item" style="display: none"><a class="nav-link"
				href="${pageContext.request.contextPath}/claimlist"> <i class="nav-icon fa fa-file"></i> Claim List
			</a></li>
			<li id="M003" class="nav-item" style="display: none"><a class="nav-link"
				href="${pageContext.request.contextPath}/providers"> <i class="nav-icon fa fa-map"></i> Providers
			</a></li>
			<li id="M002" class="nav-item nav-dropdown" style="display: none"><a class="nav-link nav-dropdown-toggle" style="cursor: pointer;" >
					<i class="nav-icon fa fa-image"></i> Contents
			</a>
				<ul class="nav-dropdown-items">
					<li id="M002_1" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/news"> <i class="nav-icon fa fa-book"></i> News
					</a></li>
					<li id="M002_2" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/help"> <i class="nav-icon fa fa-info"></i> Help
					</a></li>
					<li id="M002_3" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/termsandcondition"> <i class="nav-icon fa fa-book"></i> Terms Condition
					</a></li>
				</ul></li>
			<li id="M014" class="nav-item nav-dropdown" style="display: none"><a class="nav-link nav-dropdown-toggle" style="cursor: pointer;" >
					<i class="nav-icon fa fa-file"></i> Products
			</a>
				<ul class="nav-dropdown-items">
					<li id="M014_1" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/product_category"> <i class="nav-icon fa fa-file"></i> Product Category
					</a></li>
					<li id="M014_2" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/product"> <i class="nav-icon fa fa-file"></i> Product
					</a></li>
				</ul></li>
			<li id="M015" class="nav-item" style="display: none"><a class="nav-link"
				href="${pageContext.request.contextPath}/services"> <i class="nav-icon fa fa-map"></i> Services
			</a></li>
			<li id="M016" class="nav-item" style="display: none"><a class="nav-link"
				href="${pageContext.request.contextPath}/broadcast"> <i class="nav-icon fa fa-bullhorn"></i> Broadcast
			</a></li>
			<li id="M008" class="nav-item nav-dropdown" style="display: none"><a class="nav-link nav-dropdown-toggle" style="cursor: pointer;" >
					<i class="nav-icon fa fa-image"></i> Customizations
			</a>
				<ul class="nav-dropdown-items">
					<li id="M008_1" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/backgrounds"> <i class="nav-icon fa fa-picture-o"></i> Backgrounds
					</a></li>
					<li id="M008_2" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/menuicons"> <i class="nav-icon fa fa-picture-o"></i> Menu Icons
					</a></li>
					<li id="M008_3" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/virtualcards"> <i class="nav-icon fa fa-picture-o"></i> Virtual Cards
					</a></li>
				</ul></li>
			<li id="M010" class="nav-item" style="display: none"><a class="nav-link"
				href="${pageContext.request.contextPath}/rating"> <i class="nav-icon fa fa-star"></i> Rating
			</a></li>
			<li id="M004" class="nav-item nav-dropdown" style="display: none"><a class="nav-link nav-dropdown-toggle" style="cursor: pointer;" >
					<i class="nav-icon fa fa-cog"></i> Settings
			</a>
				<ul class="nav-dropdown-items">
					<li id="M004_1" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/user_mobile"> <i class="nav-icon fa fa-user"></i> Mobile User List
					</a></li>
					<li id="M004_2" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/user_cms"> <i class="nav-icon fa fa-user"></i> CMS User List
					</a></li>
					<li id="M004_3" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/mobileconfig"> <i class="nav-icon fa fa-cog"></i> Mobile Config
					</a></li>
					<li id="M004_4" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/userrole"> <i class="nav-icon fa fa-cog"></i> User Role
					</a></li>
					<li id="M004_5" class="nav-item ml-3" style="display: none"><a class="nav-link"
						href="${pageContext.request.contextPath}/menu_member"> <i class="nav-icon fa fa-cog"></i> Mobile Menu List
					</a></li>
				</ul></li>
			<li id="M013" class="nav-item" style="display: none"><a class="nav-link"
				href="${pageContext.request.contextPath}/api_log"> <i class="nav-icon fa fa-book"></i> API Log
			</a></li>
		</ul>
	</nav>
	<button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
<script src="mainform/plugins/jquery-3.4.1.min.js"></script>
<script>
	$(function() {
		//get restricted menu
		$.ajax({
		url : '${pageContext.request.contextPath}/site_master',
		type : 'GET',
		data : {
			"key" : "AllowedMenu"
		},
		dataType : 'json'
		}).done(function(data) {
			//console.log(data);

			//for json data type
			for ( var i in data) {
				var obj = data[i];
				var index = 0;
				var menuid;
				for ( var prop in obj) {
					switch (index++) {
					case 0:
						menuid = obj[prop];
						break;
					default:
						break;
					}
				}

				//$("#"+menuid).hide();
				$("#" + menuid).show();
			}
		}).fail(function() {
			console.log('Service call failed!');
		});

		return true;
	})
</script>