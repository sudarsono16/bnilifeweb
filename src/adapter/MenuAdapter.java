package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;

public class MenuAdapter {
    public static List<model.mdlMenu> LoadMenu() {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.Query.mdlQueryExecute> listParam;
	String sql = "";
	CachedRowSet rowset = null;
	List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
	Globals.gCommand = "";
	try {
	    sql = "SELECT MenuID, Name, Url, [Type], [Level] FROM ms_menu WHERE [Level] =0 ORDER BY MenuID ";
	    rowset = QueryExecuteAdapter.QueryExecute(sql, functionName);
	    Globals.gCommand = sql;

	    while (rowset.next()) {
		model.mdlMenu mdlMenu = new model.mdlMenu();
		mdlMenu.setMenuID(rowset.getString("MenuID"));
		mdlMenu.setMenuName(rowset.getString("Name"));
		mdlMenu.setMenuUrl(rowset.getString("Url"));
		mdlMenu.setType(rowset.getString("Type"));
		mdlMenu.setLevel(rowset.getString("Level"));
		listmdlMenu.add(mdlMenu);

		CachedRowSet rowset2 = null;
		listParam = new ArrayList<model.Query.mdlQueryExecute>();
		String sqlLoadMenuLv1 = "SELECT MenuID, Name, Url, [Type], [Level] FROM ms_menu WHERE [Type] = ? AND [Level] = 1 ORDER BY MenuID ";
		Globals.gCommand = sqlLoadMenuLv1;
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlMenu.getType()));
		rowset2 = QueryExecuteAdapter.QueryExecute(sqlLoadMenuLv1, listParam, functionName);

		while (rowset2.next()) {
		    model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
		    mdlMenuLv1.setMenuID(rowset2.getString("MenuID"));
		    mdlMenuLv1.setMenuName("- " + rowset2.getString("Name"));
		    mdlMenuLv1.setMenuUrl(rowset2.getString("Url"));
		    mdlMenuLv1.setType(rowset2.getString("Type"));
		    mdlMenuLv1.setLevel(rowset2.getString("Level"));

		    listmdlMenu.add(mdlMenuLv1);
		}
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, Globals.gCommand, Globals.user_id);
	}

	return listmdlMenu;
    }

    public static List<model.mdlMenu> LoadMenuByID(String lMenuID, String lCommand) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
	String sql = "";
	CachedRowSet rowset = null;
	List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
	Globals.gCommand = "";

	try {
	    sql = "SELECT MenuID, Name, Url, [Type], [Level] FROM ms_menu WHERE MenuID = ? ORDER BY MenuID ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", lMenuID));

	    rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    Globals.gCommand = sql;

	    while (rowset.next()) {

		String menuLevel = rowset.getString("Level");
		CachedRowSet rowset2 = null;
		listParam = new ArrayList<model.Query.mdlQueryExecute>();
		if (menuLevel.contentEquals("0")) {
		    String sqlLoadMenuLv0 = "SELECT MenuID, Name, Url, [Type], [Level] FROM ms_menu WHERE [Type] = ? ORDER BY MenuID;";
		    listParam.add(QueryExecuteAdapter.QueryParam("string", rowset.getString("Type")));

		    rowset2 = QueryExecuteAdapter.QueryExecute(sqlLoadMenuLv0, listParam, functionName);
		    Globals.gCommand = sqlLoadMenuLv0;

		    while (rowset2.next()) {
			model.mdlMenu mdlMenuLv0 = new model.mdlMenu();

			mdlMenuLv0.setMenuID(rowset2.getString("MenuID"));

			if (rowset2.getString("Level").contentEquals("0"))
			    mdlMenuLv0.setMenuName(rowset2.getString("Name"));
			else
			    mdlMenuLv0.setMenuName("- " + rowset2.getString("Name"));

			mdlMenuLv0.setMenuUrl(rowset2.getString("Url"));
			mdlMenuLv0.setType(rowset2.getString("Type"));
			mdlMenuLv0.setLevel(rowset2.getString("Level"));

			listmdlMenu.add(mdlMenuLv0);
		    }
		} else if (menuLevel.contentEquals("1")) {
		    String sqlLoadMenuLv1 = "";
		    if (lCommand.contentEquals("Add")) {
			sqlLoadMenuLv1 = "SELECT MenuID, Name, Url, [Type], [Level] FROM ms_menu WHERE (MenuID LIKE ?) OR ([Type] = ? AND [Level] = 0) ORDER BY MenuID ";
			listParam.add(QueryExecuteAdapter.QueryParam("string", "%" + rowset.getString("MenuID") + "%"));
			listParam.add(QueryExecuteAdapter.QueryParam("string", rowset.getString("Type")));

			rowset2 = QueryExecuteAdapter.QueryExecute(sqlLoadMenuLv1, listParam, functionName);
		    } else {
			sqlLoadMenuLv1 = "SELECT MenuID, Name, Url, [Type], [Level] FROM ms_menu WHERE (MenuID LIKE ? )  ORDER BY MenuID }";
			listParam.add(QueryExecuteAdapter.QueryParam("string", "%" + rowset.getString("MenuID") + "%"));

			rowset2 = QueryExecuteAdapter.QueryExecute(sqlLoadMenuLv1, listParam, functionName);
		    }
		    Globals.gCommand = sqlLoadMenuLv1;

		    while (rowset2.next()) {
			model.mdlMenu mdlMenuLv1 = new model.mdlMenu();

			mdlMenuLv1.setMenuID(rowset2.getString("MenuID"));

			if (rowset2.getString("Level").contentEquals("0"))
			    mdlMenuLv1.setMenuName(rowset2.getString("Name"));
			else
			    mdlMenuLv1.setMenuName("- " + rowset2.getString("Name"));

			mdlMenuLv1.setMenuUrl(rowset2.getString("Url"));
			mdlMenuLv1.setType(rowset2.getString("Type"));
			mdlMenuLv1.setLevel(rowset2.getString("Level"));

			listmdlMenu.add(mdlMenuLv1);
		    }
		}
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, Globals.gCommand, Globals.user_id);
	}
	return listmdlMenu;
    }

    public static String InsertUserRule(String lRoleID, String lRoleName, String[] llistAllMenuID, String[] llistAllowedMenuID, String[] llistEditableMenuID) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
	String sql = "";
	CachedRowSet rowset = null;
	Boolean check = false;
	Boolean success = false;

	try {
	    sql = "SELECT RoleID FROM ms_role WHERE RoleID = ?";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID));

	    rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    Globals.gCommand = sql;

	    while (rowset.next()) {
		check = true;
	    }
	    // if no duplicate
	    if (check == false) {
		List<model.Query.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.Query.mdlQueryTransaction>();

		// store 'InsertRole' parameter for transaction
		model.Query.mdlQueryTransaction mdlQueryTransaction = new model.Query.mdlQueryTransaction();
		mdlQueryTransaction.sql = "  INSERT INTO ms_role(RoleID, Name) VALUES (?, ?)";
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
		mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleName)));
		Globals.gCommand = mdlQueryTransaction.sql;
		listmdlQueryTransaction.add(mdlQueryTransaction);

		// store 'InsertAccessRole' parameter for transaction
		for (String lmenuid : llistAllMenuID) {
		    mdlQueryTransaction = new model.Query.mdlQueryTransaction();
		    mdlQueryTransaction.sql = "BEGIN TRAN UPDATE ms_access_role SET IsAccess = ?, IsModify = ? WHERE RoleID = ? AND MenuID = ? "
			    + "IF @@rowcount = 0 BEGIN INSERT INTO ms_access_role (RoleID, MenuID, IsAccess, IsModify) VALUES (?, ?, ?, ?) END COMMIT TRAN";
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
		    
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
		    
		    Globals.gCommand = mdlQueryTransaction.sql;
		    listmdlQueryTransaction.add(mdlQueryTransaction);
		}

		// store 'UpdateIsAccess' parameter for transaction
		for (String lmenuid : llistAllowedMenuID) {
		    mdlQueryTransaction = new model.Query.mdlQueryTransaction();
		    mdlQueryTransaction.sql = "BEGIN TRAN UPDATE ms_access_role SET IsAccess = ? WHERE RoleID = ? AND MenuID = ? "
			    + "IF @@rowcount = 0 BEGIN INSERT INTO ms_access_role (RoleID, MenuID, IsAccess, IsModify) VALUES (?, ?, ?, ?) END COMMIT TRAN";
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
		    
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
		    Globals.gCommand = mdlQueryTransaction.sql;
		    listmdlQueryTransaction.add(mdlQueryTransaction);
		}

		// store 'UpdateIsModify' parameter for transaction
		for (String lmenuid : llistEditableMenuID) {
		    mdlQueryTransaction = new model.Query.mdlQueryTransaction();
		    mdlQueryTransaction.sql = "UPDATE ms_access_role SET IsModify= ? WHERE RoleID = ? AND MenuID = ?";
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
		    Globals.gCommand = mdlQueryTransaction.sql;
		    listmdlQueryTransaction.add(mdlQueryTransaction);
		}

		success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, functionName);
		if (success)
		    Globals.gReturn_Status = "SuccessInsert";
		else
		    Globals.gReturn_Status = "Database Error";
	    }
	    // if duplicate
	    else {
		Globals.gReturn_Status = "The Role Id Already Exist";
	    }
	} catch (Exception e) {
	    LogAdapter.InsertLogExc(e.toString(), functionName, Globals.gCommand, Globals.user_id);
	    Globals.gReturn_Status = "Database Error";
	}

	return Globals.gReturn_Status;
    }

    public static String UpdateUserRule(String lRoleID, String lRoleName, String[] llistAllMenuID, String[] llistAllowedMenuID, String[] llistEditableMenuID) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Boolean success = false;
	List<model.Query.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.Query.mdlQueryTransaction>();
	model.Query.mdlQueryTransaction mdlQueryTransaction = new model.Query.mdlQueryTransaction();

	try {
	    mdlQueryTransaction.sql = "UPDATE ms_access_role SET IsModify= ? WHERE RoleID = ? ;"
	    			    + "UPDATE ms_role SET Name = ? WHERE RoleID = ? ;";
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleName)));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
	    
	    Globals.gCommand = mdlQueryTransaction.sql;
	    listmdlQueryTransaction.add(mdlQueryTransaction);

	    mdlQueryTransaction = new model.Query.mdlQueryTransaction();
	    mdlQueryTransaction.sql = "UPDATE ms_access_role SET IsAccess = ? WHERE RoleID = ? ";
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
	    Globals.gCommand = mdlQueryTransaction.sql;
	    listmdlQueryTransaction.add(mdlQueryTransaction);

	    success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, functionName);
	    if (success)
		Globals.gReturn_Status = "SuccessUpdate";
	    else
		Globals.gReturn_Status = "Database Error";
	} catch (Exception e) {
	    LogAdapter.InsertLogExc(e.toString(), functionName, Globals.gCommand, Globals.user_id);
	    Globals.gReturn_Status = "Database Error";
	}

	try {
	    listmdlQueryTransaction = new ArrayList<model.Query.mdlQueryTransaction>();
	    if (llistAllowedMenuID.length != 0) {
		for (String lmenuid : llistAllowedMenuID) {
		    mdlQueryTransaction = new model.Query.mdlQueryTransaction();
		    mdlQueryTransaction.sql = "BEGIN TRAN UPDATE ms_access_role SET IsAccess = ? WHERE RoleID = ? AND MenuID = ? "
		    			+ "IF @@rowcount = 0 BEGIN INSERT INTO ms_access_role (RoleID, MenuID, IsAccess, IsModify) VALUES (?, ?, ?, ?) END COMMIT TRAN";
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
		    
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", false));
		    Globals.gCommand = mdlQueryTransaction.sql;
		    listmdlQueryTransaction.add(mdlQueryTransaction);
		}
	    }

	    // store 'UpdateIsModify' parameter for transaction
	    if (llistEditableMenuID.length != 0) {
		for (String lmenuid : llistEditableMenuID) {
		    mdlQueryTransaction = new model.Query.mdlQueryTransaction();
		    mdlQueryTransaction.sql = "UPDATE ms_access_role SET IsModify= ? WHERE RoleID = ? AND MenuID = ?";
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("boolean", true));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
		    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lmenuid)));
		    Globals.gCommand = mdlQueryTransaction.sql;
		    listmdlQueryTransaction.add(mdlQueryTransaction);
		}
	    }

	    success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, functionName);
	    if (success)
		Globals.gReturn_Status = "SuccessUpdate";
	    else
		Globals.gReturn_Status = "Database Error";
	} catch (Exception e) {
	    LogAdapter.InsertLogExc(e.toString(), functionName, Globals.gCommand, Globals.user_id);
	    Globals.gReturn_Status = "Database Error";
	}

	return Globals.gReturn_Status;
    }

    public static String DeleteUserRule(String lRoleID) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Boolean success = false;
	List<model.Query.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.Query.mdlQueryTransaction>();
	model.Query.mdlQueryTransaction mdlQueryTransaction = new model.Query.mdlQueryTransaction();

	try {
	    // store 'DeleteRole' parameter for transaction
	    mdlQueryTransaction = new model.Query.mdlQueryTransaction();
	    mdlQueryTransaction.sql = "DELETE FROM ms_role WHERE RoleID = ? ";
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
	    Globals.gCommand = mdlQueryTransaction.sql;
	    listmdlQueryTransaction.add(mdlQueryTransaction);

	    // store 'DeleteAccessRole' parameter for transaction
	    mdlQueryTransaction = new model.Query.mdlQueryTransaction();
	    mdlQueryTransaction.sql = "DELETE FROM ms_access_role WHERE RoleID = ? ";
	    mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", ValidateNull.NulltoStringEmpty(lRoleID)));
	    Globals.gCommand = mdlQueryTransaction.sql;
	    listmdlQueryTransaction.add(mdlQueryTransaction);

	    success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, functionName);
	    if (success)
		Globals.gReturn_Status = "SuccessDelete";
	    else
		Globals.gReturn_Status = "Database Error";
	} catch (Exception e) {
	    LogAdapter.InsertLogExc(e.toString(), functionName, Globals.gCommand, Globals.user_id);
	    Globals.gReturn_Status = "Database Error";
	}

	return Globals.gReturn_Status;
    }

    public static List<model.mdlMenu> LoadAllowedMenu(String lRoleID) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
	List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
	String sql = "";
	CachedRowSet rowset = null;
	try {
	    sql = "SELECT a.MenuID,a.Name,a.Url,a.[Type],a.[Level] FROM ms_menu a "
	    	+ "INNER JOIN ms_access_role b ON b.MenuID=a.MenuID AND b.IsAccess = 1 AND b.RoleID = ? WHERE a.[Level] = 0 ORDER BY a.MenuID ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID));

	    rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    Globals.gCommand = sql.toString();

	    while (rowset.next()) {
		model.mdlMenu mdlMenu = new model.mdlMenu();
		mdlMenu.setMenuID(rowset.getString("MenuID"));
		mdlMenu.setMenuName(rowset.getString("Name"));
		mdlMenu.setMenuUrl(rowset.getString("Url"));
		mdlMenu.setType(rowset.getString("Type"));
		mdlMenu.setLevel(rowset.getString("Level"));
		listmdlMenu.add(mdlMenu);

		CachedRowSet rowset2 = null;
		listParam = new ArrayList<model.Query.mdlQueryExecute>();
		String sql2 = "SELECT a.MenuID,a.Name,a.Url,a.[Type],a.[Level] FROM ms_menu a "
			+ "INNER JOIN ms_access_role b ON b.MenuID = a.MenuID AND b.IsAccess = 1 AND b.RoleID = ? "
			+ "WHERE a.[Type] = ?  AND a.[Level] = 1 ORDER BY a.MenuID ";
		listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID));
		listParam.add(QueryExecuteAdapter.QueryParam("string", mdlMenu.getType()));
		rowset2 = QueryExecuteAdapter.QueryExecute(sql2, listParam, functionName);
		Globals.gCommand = sql2;

		while (rowset2.next()) {
		    model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
		    mdlMenuLv1.setMenuID(rowset2.getString("MenuID"));
		    mdlMenuLv1.setMenuName("- " + rowset2.getString("Name"));
		    mdlMenuLv1.setMenuUrl(rowset2.getString("Url"));
		    mdlMenuLv1.setType(rowset2.getString("Type"));
		    mdlMenuLv1.setLevel(rowset2.getString("Level"));
		    listmdlMenu.add(mdlMenuLv1);
		}
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, Globals.gCommand, Globals.user_id);
	}

	return listmdlMenu;
    }

    public static List<model.mdlMenu> LoadCRUDMenu() {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
	String sql = "";
	CachedRowSet rowset = null;
	try {
	    sql = "SELECT MenuID, Name, Url, [Type], [Level], CRUD FROM ms_menu WHERE CRUD=1 ORDER BY MenuID ";
	    rowset = QueryExecuteAdapter.QueryExecute(sql, functionName);

	    while (rowset.next()) {
		model.mdlMenu mdlMenu = new model.mdlMenu();
		mdlMenu.setMenuID(rowset.getString("MenuID"));
		mdlMenu.setMenuName(rowset.getString("Name"));
		mdlMenu.setMenuUrl(rowset.getString("Url"));
		mdlMenu.setType(rowset.getString("Type"));
		mdlMenu.setLevel(rowset.getString("Level"));
		mdlMenu.setCRUD(rowset.getBoolean("CRUD"));
		mdlMenu.setIsModify(false);

		listmdlMenu.add(mdlMenu);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, sql, Globals.user_id);
	}

	return listmdlMenu;
    }

    public static List<model.mdlMenu> LoadEditableMenu() {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
	String sql = "";
	try {
	    // sql = "{call sp_LoadEditableMenu()}";
	    // rowset = QueryExecuteAdapter.QueryExecute(sql,"LoadEditableMenu");
	    //
	    // while(rowset.next()){
	    // model.mdlMenu mdlMenu = new model.mdlMenu();
	    // mdlMenu.setMenuID(rowset.getString("MenuID"));
	    // mdlMenu.setMenuName(rowset.getString("Name"));
	    // mdlMenu.setIsModify(rowset.getBoolean("IsModify"));
	    //
	    // listmdlMenu.add(mdlMenu);
	    // }
	    // if(listmdlMenu.size() == 0)
	    // {
	    listmdlMenu = LoadCRUDMenu();
	    // }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, sql, Globals.user_id);
	}

	return listmdlMenu;
    }

    public static List<model.mdlMenu> LoadEditableMenu(String lRoleID) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
	List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
	String sql = "";
	CachedRowSet rowset = null;

	try {
	    sql = "SELECT a.MenuID,a.Name, b.IsModify, b.RoleID FROM ms_menu a INNER JOIN ms_access_role b ON b.MenuID = a.MenuID "
	    	+ "WHERE a.CRUD = 1 AND b.IsModify = 1 AND b.RoleID = ? ORDER BY a.MenuID;";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", lRoleID));

	    rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);
	    Globals.gCommand = sql;

	    while (rowset.next()) {
		model.mdlMenu mdlMenu = new model.mdlMenu();
		mdlMenu.setMenuID(rowset.getString("MenuID"));
		mdlMenu.setMenuName(rowset.getString("Name"));
		mdlMenu.setIsModify(rowset.getBoolean("IsModify"));

		listmdlMenu.add(mdlMenu);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, Globals.gCommand, Globals.user_id);
	}

	return listmdlMenu;
    }

    public static Boolean CheckMenu(String MenuURL, String username) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
	String sql = "";
	CachedRowSet rowset = null;
	Boolean CheckMenu = false;
	try {
	    sql = "SELECT a.IsAccess FROM ms_access_role a INNER JOIN ms_menu b ON b.MenuID = a.MenuID INNER JOIN ms_user_cms c ON c.RoleID = a.RoleID "
	    	+ "WHERE b.Url = ? AND c.username = ?";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", MenuURL));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));

	    rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (rowset.next()) {
		if (rowset.getBoolean("IsAccess") == true) {
		    CheckMenu = true;
		}
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, sql, Globals.user_id);
	}

	return CheckMenu;
    }

    public static String SetMenuButtonStatus(String MenuURL, String username) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
	String sql = "";
	CachedRowSet rowset = null;
	String ButtonStatus = "enabled";
	try {
	    sql = "SELECT a.IsModify FROM ms_access_role a INNER JOIN ms_menu b ON b.MenuID = a.MenuID INNER JOIN ms_user_cms c ON c.RoleID = a.RoleID "
	    	+ "WHERE b.Url = ? AND c.username = ?";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", MenuURL));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", username));

	    rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (rowset.next()) {
		if (rowset.getBoolean("IsModify") == false) {
		    ButtonStatus = "disabled";
		}
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, sql, Globals.user_id);
	}

	return ButtonStatus;
    }
}
