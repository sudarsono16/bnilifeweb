<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="mainform/plugins/datatables/extensions/Responsive/css/responsive.bootstrap4.min.css">
<style>
table.dataTable tbody td {
	word-break: break-word;
	vertical-align: top;
}
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
		<div class="container-fluid">
			<div class="animated fadeIn">
				<div class="search-container row">
					<div class="col-lg-3 col-md-3 col-12">
						<button class="btn btn-block btn-warning" id="SubmitNew" type="button" data-toggle="modal" data-target="#addModal">
							<i class="fa fa-plus-circle"></i> Add New CMS User
						</button>
					</div>
				</div>
				<table class="table table-striped table-bordered sortable" id="main_table">
					<thead>
						<tr>
							<th>Username</th>
							<th>Role</th>
							<th>Status</th>
							<th>Status Menu</th>
							<th></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
		</main>
		
		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form id="FormSubmit" >
					<div class="modal-content">
						<div class="modal-header">
	<!-- 						<h5 class="modal-title claim-num" id="addProviderLabel">Add New Provider</h5> -->
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col">
									<input type="hidden" id="provider_id" name="provider_id" readonly>
									<div class="input-group input-group-sm ">
										<div class="input-group-prepend">
											<span class="input-group-text">username</span>
										</div>
										<input type="text" id="username" name="username" class="form-control">
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Password</span>
										</div>
										<input type="text" id="password" name="password" class="form-control" >
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Role</span>
										</div>
										<select class="custom-select form-control" name="roleid" id="roleid"></select>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" id="SubmitButton" type="submit" >Save changes</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script>
	var datatable = {};
	$(document).ready(function() {
		datatable = $('#main_table').DataTable({
			language : {
				"processing" : "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
			},
			autoWidth : false,
			processing : true,
			searching : true,
			searchDelay : 500,
			destroy : true,
			cache : true,
			contentType : "application/json; charset=utf-8",
			PaginationType : "full",
			responsive : true,
			serverSide : true,
			ajax : {
			"url" : url_local_web + "/APICall",
			"type" : "POST",
			"headers" : {
				'Content-Type' : 'application/json'
			},
			"data" : function(param) {
				var pageNumber = $('#main_table').DataTable().page.info().page + 1;
				var pageSize = $('#main_table').DataTable().page.info().length;
				var input_parameter = {};
				var searchstring = "";
				if (param.search.value.trim()) {
					searchstring = '&search=' + encodeURIComponent(param.search.value);
				}

				input_parameter.APIUrl = "/cms/get-cms-user-list?pageNumber=" + pageNumber + "&pageSize=" + pageSize + '&draw=' + param.draw + searchstring;
				input_parameter.method = "GET";
				return JSON.stringify(input_parameter);
			},
			"dataSrc" : "output_schema.data"
			},
			responsive : {
				breakpoints : [ {
					name : 'desktop',
					width : Infinity
				}, {
					name : 'tablet-l',
					width : 1024
				}, {
					name : 'tablet-p',
					width : 768
				}, {
					name : 'mobile-l',
					width : 480
				}, {
					name : 'mobile-p',
					width : 320
				} ]
			},
			columns : [ 
				{
					"data" : "username"
				},
				{
					"data" : "RoleID",
					"render" : function(data, type, row) {
						if (data == 'R001') {
							return 'Administrator';
						} else if (data == 'R002') {
							return 'Content Creator';
						} else if (data == 'R003') {
							return 'Staff';
						} else {
							return '-';
						}
					}
				},
				{
					"data" : "is_active",
					"render" : function(data, type, row) {
						if (data == '1') {
							return 'Active';
						} else {
							return 'Inactive';
						}
					}
				},
				{
					"data" : {
						username : "username"
					},
					"render" : function(data, type, row) {
						if (data.is_active == '1') {
							return '<span class="input-group-btn"> <button type="button" class="btn btn-danger btn-xs data_status_edit" data-username="'+data.username+'" data-is_active="0" ><i class="fa fa-ban "></i></button>';
						} else {
							return '<span class="input-group-btn"> <button type="button" class="btn btn-success btn-xs data_status_edit" data-username="'+data.username+'" data-is_active="1" ><i class="fa fa-check "></i></button>';
						}
					}
				},
				{
					"data": "username",
					"render": function (data, type, row) {
						var button_string = "";
						button_string += '<span class="input-group-btn">';
						//button_string += '<button type="button" class="btn btn-success btn-sm data_detail" style="width: 35px;" data-id="'+data+'"><i class="fa fa-search-plus"></i></button>';
						button_string += '<button type="button" class="btn btn-info btn-sm data_update" style="width: 35px;" data-id="'+data+'"><i class="fa fa-edit"></i></button>';
						//button_string += '<button type="button" class="btn btn-danger btn-sm data_delete" style="width: 35px;" data-id="'+data+'"><i class="fa fa-trash-o"></i></button>';
						button_string += '</span>';
						return button_string;
					}
				}
			],
			columnDefs : [ {
				targets : [ 0, 1, 2 ],
				searchable : true,
				sortable : false,
				visible : true,
				defaultContent : ""
			}, {
				targets : [ 3, 4 ],
				searchable : false,
				sortable : false,
				visible : true,
				defaultContent : ""
			} ]
		}).on('xhr.dt', function(e, settings, json, xhr) {
			json.recordsTotal = json.output_schema.recordsTotal;
			json.recordsFiltered = json.output_schema.recordsFiltered;
			json.draw = json.output_schema.draw;
			json.data = json.output_schema.data;
		});

		$('#main_table tbody').on('click', '.data_status_edit', function(event) {
			var username = $(this).data("username");
			var is_active = $(this).data("is_active");

			var input_parameter = {};
			input_parameter.APIUrl = "/cms/insert-update-cms-user";
			input_parameter.method = "POST";
			input_parameter.username = username;
			input_parameter.is_active = is_active;

			$.ajax({
			url : url_local_web + "/APICall",
			type : 'POST',
			dataType : 'JSON',
			data : JSON.stringify(input_parameter),
			headers : {
				"Content-Type" : "application/json"
			},
			beforeSend : function() {
			},
			}).done(function(data) {
				console.log(data);
				datatable.ajax.reload();
			}).fail(function(jqXHR, textStatus) {
				console.log("failed " + jqXHR + " " + textStatus);
			});

		});

	});
	
	

	$('#main_table tbody').on('click', '.data_detail',function () {
		$('#SubmitButton').hide();
		
		var current_data = "";
		if ($(this).parent().hasClass("dtr-data")) {
			current_data = datatable.row($(this)).data();
		}
		else {
			current_data = datatable.row($(this).closest('tr')).data();
		}
		populate_select(current_data.RoleID);
		fillData(current_data);
		
		$('#addModal').modal({
			backdrop: 'static',
			keyboard: false, 
			show: true
		});
		
	});
	
	$('#main_table tbody').on('click', '.data_update',function () {
		$('#SubmitButton').show();
		$('#username').prop('readonly',true)
		validate_start_function('update');
		
		var current_data = "";
		if ($(this).parent().hasClass("dtr-data")) {
			current_data = datatable.row($(this)).data();
		}
		else {
			current_data = datatable.row($(this).closest('tr')).data();
		}
		populate_select(current_data.RoleID);
		fillData(current_data);
		$('#password').val('');
		$('#password').prev().children().text('New Password');
		$('#addModal').modal({
			backdrop: 'static',
			keyboard: false, 
			show: true
		});
		
	});
	
	function fillData(current_data){
		console.log(current_data);
		$('#password').prev().children().text('Password');
		$('#username').val(current_data.username);
		$('#password').val(current_data.password);
		$('#roleid').val(current_data.roleid);
	}
	
	$('#addModal').on('hide.bs.modal', function () {
		reset_form();
	});
	
	$('#SubmitNew').on('click', function () {
		reset_form();
		$('#username').prop('readonly',false);
		$('#password').prev().children().text('Password');
		validate_start_function('new');
	});
	
	function reset_form(){
		$('#SubmitButton').show();
		$("#FormSubmit")[0].reset();
		$form = $('form');
		$form.find('.control-group').removeClass('.error');
		$form.find('.has-error').removeClass("has-error");
		$form.find('.has-success').removeClass("has-success");
		$form.find('.form-control-feedback').remove();
		
		populate_select();
	}
	
	function populate_select(roleid){
		$('#roleid').removeAttr('selected').find('[value=""]').attr('selected','selected').prop("selected",true);
		if(!$("#roleid").find("option:selected").length){
			var data = [];
			data.push({"ID":"","Text":"Pilih provider"});
			data.push({"ID":"R001","Text":"Administrator"});
			data.push({"ID":"R002","Text":"Content Creator"});
			data.push({"ID":"R003","Text":"Staff"});
			
			$.each(data, function (key, value) {
				$('#roleid').append($('<option>', {
					value: value.ID,
					text: value.Text
				}));
			});
		}
		if(typeof roleid != 'undefined' || roleid != null){
			$('#roleid').removeAttr('selected').find('[value="'+roleid+'"]').attr('selected','selected').prop("selected",true);
		}
	}
	
	function validate_start_function(SubmitType){
		var validate_start = $("#FormSubmit").validate({
			ignore: "",
			rules: {
				username: {
					required: true
				},
				password: {
					required: false
				},
				roleid: {
					required: true
				}
			},
			messages: {},
			highlight: function (element, errorClass) {
				$(element).closest(".form-group").addClass("has-error").css("color", "");
			},
			unhighlight: function (element, errorClass) {
				$(element).closest(".form-group").removeClass("has-error").css("color", "");
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function (error, element) {
				error.css({
					'font-size': '10px',
					'display': 'inline',
					'color': 'red'
				});
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				}
				else {
					error.insertAfter(element);
				}
			},
			submitHandler: function (form) {
				
				var input_parameter = {};
				input_parameter.username = $('#username').val();
				if(SubmitType == 'new' || (typeof SubmitType != 'undefined' && SubmitType != null) ){
					input_parameter.password = $('#password').val();
				}
				input_parameter.RoleID = $('#roleid').val();
				input_parameter.is_active = '1';
				
				input_parameter.APIUrl = "/cms/insert-update-cms-user";
				input_parameter.method = "POST";
				
				var message_type_success = "";
				var message_type_failed = "";
				if(SubmitType == 'new'){
					message_type_success = "Insert Data berhasil.";
					message_type_failed = "Insert Data gagal.";
				}
				else{
					message_type_success = "Update Data berhasil.";
					message_type_failed = "Update Data gagal.";
				}
				
				$.ajax({
					url : url_local_web+"/APICall",
					type: 'POST',
					dataType: 'JSON',
					data: JSON.stringify(input_parameter),
					headers: {
						"Content-Type": "application/json"
					},
					beforeSend: function() {},
				}).done(function (data) {
					reset_form();
					$('#addModal').modal('hide');
					
					if(data.error_schema.error_code == "ERR-00-000"){
						Swal.fire({
							type: 'success',
							title: 'Alert',
							text: message_type_success
						});
					}
					else{
						Swal.fire({
							type: 'error',
							title: 'Alert',
							text: message_type_failed
						});
					}
					
					datatable.ajax.reload();
				}).fail(function (jqXHR, textStatus) {
					reset_form();
					console.log("failed " + jqXHR + " " + textStatus);
					datatable.ajax.reload();
				});
			},
			invalidHandler: function (event, validator) {
				var errors = validator.numberOfInvalids();
				console.log("error " + errors);
			}
		});
		jQuery.extend(jQuery.validator.messages, {
			number: "Please enter only number."
		});
		validate_start.resetForm();
	}
	
	</script>
</body>
</html>
