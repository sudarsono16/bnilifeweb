package com.bnilife;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = { "/api_log" }, name = "api_log")
public class APILogServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public APILogServlet() {
	super();
	// TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	HttpSession session = request.getSession();
	String username = (String) session.getAttribute("user_id");

	request.setAttribute("username", username);

	RequestDispatcher dispatcher = request.getRequestDispatcher("/mainform/api_log.jsp");
	dispatcher.forward(request, response);
    }
}
