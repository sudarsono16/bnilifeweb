package com.bnilife;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = { "/menu_member" }, name = "menu_member")
public class MenuMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MenuMemberServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("user_id");  
		
		request.setAttribute("username", username);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/mainform/menu_member.jsp");
		dispatcher.forward(request, response);
	}
}
