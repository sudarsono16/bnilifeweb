<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>

<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="mainform/plugins/datatables/extensions/Responsive/css/responsive.bootstrap4.min.css">
<link href="mainform/css/quill.snow.css" rel="stylesheet">

<style>
table.dataTable tbody td {
	word-break: break-word;
	vertical-align: top;
}
</style>

</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<table class="table table-striped table-bordered sortable" id="main_table">
						<thead>
							<tr>
								<th>Claim No</th>
								<th>Claim Status</th>
								<th>Username</th>
								<th>Member ID</th>
								<th>Member Type</th>
								<th>Member Child ID</th>
								<th>Member Name</th>
								<th>Claim Type</th>
								<th>Claim Type Name</th>
								<th>Claim Date</th>
								<th>Hospital Name</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Amount</th>
								<th>Policy Period</th>
								<th>Phone Number</th>
								<th>Bank Name</th>
								<th>Account No</th>
								<th>Account Name</th>
								<th></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</main>
		<!-- Edit Information Modal -->
		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addProviderLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form id="FormSubmit" >
					<div class="modal-content">
						<div class="modal-header">
	<!-- 						<h5 class="modal-title claim-num" id="addProviderLabel">Add New Provider</h5> -->
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<input type="hidden" id="help_id" name="help_id" readonly>
								<div class="col">
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Claim_no</span>
										</div>
										<input type="text" id="Claim_no" name="Claim_no" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Claim_status</span>
										</div>
										<input type="text" id="Claim_status" name="Claim_status" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Username</span>
										</div>
										<input type="text" id="Username" name="Username" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Member_Employee_no</span>
										</div>
										<input type="text" id="Member_Employee_no" name="Member_Employee_no" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Member_type</span>
										</div>
										<input type="text" id="Member_type" name="Member_type" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Member_no</span>
										</div>
										<input type="text" id="Member_no" name="Member_no" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Member_name</span>
										</div>
										<input type="text" id="Member_name" name="Member_name" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Type_code</span>
										</div>
										<input type="text" id="Type_code" name="Type_code" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Type_name</span>
										</div>
										<input type="text" id="Type_name" name="Type_name" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Claim_date</span>
										</div>
										<input type="text" id="Claim_date" name="Claim_date" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Hospital_name</span>
										</div>
										<input type="text" id="Hospital_name" name="Hospital_name" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Startdate</span>
										</div>
										<input type="text" id="Startdate" name="Startdate" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Enddate</span>
										</div>
										<input type="text" id="Enddate" name="Enddate" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Amount</span>
										</div>
										<input type="text" id="Amount" name="Amount" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Policy_period</span>
										</div>
										<input type="text" id="Policy_period" name="Policy_period" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Mobile_phone_number</span>
										</div>
										<input type="text" id="Mobile_phone_number" name="Mobile_phone_number" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Bank</span>
										</div>
										<input type="text" id="Bank" name="Bank" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Account_no</span>
										</div>
										<input type="text" id="Account_no" name="Account_no" class="form-control" style="background:white" disabled>
									</div>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">Account_name</span>
										</div>
										<input type="text" id="Account_name" name="Account_name" class="form-control" style="background:white" disabled>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary" id="SubmitButton" type="submit" >Save changes</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- MODAL -->
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script>
	var datatable = {};
	
	$(document).ready(function() {
		datatable = $('#main_table').DataTable({
			language: {
				"processing": "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
			},
			autoWidth: false,
			processing: true,
			searching: true,
			searchDelay: 500,
			destroy: true,
			cache: true,
			contentType: "application/json; charset=utf-8",
			PaginationType: "full",
			responsive: true,
			serverSide: true,
			ajax: {
				"url": url_local_web+"/APICall",
				"type": "POST",
				"headers": {
					'Content-Type': 'application/json'
				},
				"data": function (param) {
					var pageNumber = $('#main_table').DataTable().page.info().page + 1;
					var pageSize = $('#main_table').DataTable().page.info().length;
					var username = '${username}';
					var input_parameter = {};
					var searchstring = "";
					if (param.search.value.trim()) {
						searchstring = '&search='+encodeURIComponent(param.search.value);
					}
					
					input_parameter.APIUrl = "/claim/get-claim-list-by-date?username="+username+"&pageNumber="+pageNumber+"&pageSize="+pageSize+'&draw='+param.draw+searchstring;
					input_parameter.method = "GET";
					return JSON.stringify(input_parameter);
				},
				"dataSrc": "output_schema.data"
			},
			responsive: {
				breakpoints: [
					{ name: 'desktop',  width: Infinity },
					{ name: 'tablet-l', width: 1024 },
					{ name: 'tablet-p', width: 768 },
					{ name: 'mobile-l', width: 480 },
					{ name: 'mobile-p', width: 320 }
				]
			},
			columns: [
				{"data": "Claim_no"}, //0
				{"data": "Claim_status"},//1
				{"data": "Username"},//2
				{"data": "Member_Employee_no"},//3
				{"data": "Member_type"},//4
				{"data": "Member_no"},//5
				{"data": "Member_name"},//6
				{"data": "Type_code"},//7
				{"data": "Type_name"},//8
				{"data": "Claim_date"},//9
				{"data": "Hospital_name"},
				{"data": "Startdate"},
				{"data": "Enddate"},
				{"data": "Amount",
				"render": function (data, type, row) {
						return formatter.format(data);
					}
				},
				{"data": "Policy_period"},
				{"data": "Mobile_phone_number"},
				{"data": "Bank"},
				{"data": "Account_no"},
				{"data": "Account_name"},
				{
					"data": "Claim_no",
					"render": function (data, type, row) {
						var button_string = "";
						button_string += '<span class="input-group-btn">';
						button_string += '<button type="button" class="btn btn-success btn-sm data_detail" style="width: 35px;" data-id="'+data+'"><i class="fa fa-search-plus"></i></button>';
						button_string += '</span>';
						return button_string;
					}
				}
			],
			columnDefs: [
				{
					targets: [0, 18],
					searchable: true,
					sortable: false,
					visible: true,
					defaultContent: ""
				},
				{
					targets: [4, 6, 8, 9, 11, 12, 13],
					searchable: false,
					sortable: false,
					visible: true,
					defaultContent: ""
				},
				{
					targets: [1, 2, 3, 5, 7, 10, 14, 15, 16, 17],
					searchable: false,
					sortable: false,
					visible: false,
					defaultContent: ""
				}
// 				{
// 					targets: '_all',
// 					searchable: false,
// 					sortable: false,
// 					visible: false,
// 					defaultContent: ""
// 				}
			]
		}).on('xhr.dt', function (e, settings, json, xhr) {
			if(json != null || typeof json != 'undefined'){
				json.recordsTotal = json.output_schema.recordsTotal;
			    json.recordsFiltered = json.output_schema.recordsFiltered;
			    json.draw = json.output_schema.draw;
			    json.data = json.output_schema.data;
			}
			else{
				json.recordsTotal = 0;
			    json.recordsFiltered = 0;
			    json.data = [];
			}
		});
		
		
	});
	
	var formatter = new Intl.NumberFormat('en-US', {
	  style: 'currency',
	  currency: 'IDR',
	});

	$('#main_table tbody').on('click', '.data_detail',function () {
		$('#SubmitButton').hide();
		
		var current_data = "";
		if ($(this).parent().hasClass("dtr-data")) {
			current_data = datatable.row($(this)).data();
		}
		else {
			current_data = datatable.row($(this).closest('tr')).data();
		}

		fillData(current_data);
		
		$('#addModal').modal({
			backdrop: 'static',
			keyboard: false, 
			show: true
		});
		
	});
	
	function fillData(current_data){
		console.log(current_data);
		$('#Claim_no').val(current_data.Claim_no);
		$('#Claim_status').val(current_data.Claim_status);
		$('#Username').val(current_data.Username);
		$('#Member_Employee_no').val(current_data.Member_Employee_no);
		$('#Member_type').val(current_data.Member_type);
		$('#Member_no').val(current_data.Member_no);
		$('#Member_name').val(current_data.Member_name);
		$('#Type_code').val(current_data.Type_code);
		$('#Type_name').val(current_data.Type_name);
		$('#Claim_date').val(current_data.Claim_date);
		$('#Hospital_name').val(current_data.Hospital_name);
		$('#Startdate').val(current_data.Startdate);
		$('#Enddate').val(current_data.Enddate);
		$('#Amount').val(current_data.Amount);
		$('#Policy_period').val(current_data.Policy_period);
		$('#Mobile_phone_number').val(current_data.Mobile_phone_number);
		$('#Bank').val(current_data.Bank);
		$('#Account_no').val(current_data.Account_no);
		$('#Account_name').val(current_data.Account_name);
		
	}
	
	$('#addProvider').on('hide.bs.modal', function () {
		reset_form();
	});
	
	
	function reset_form(){
		$("#FormSubmit")[0].reset();
		$form = $('form');
		$form.find('.control-group').removeClass('.error');
		$form.find('.has-error').removeClass("has-error");
		$form.find('.has-success').removeClass("has-success");
		$form.find('.form-control-feedback').remove();
	}
	</script>
</body>
</html>
