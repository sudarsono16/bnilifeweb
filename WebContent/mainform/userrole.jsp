<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="mainform/plugins/datatables/extensions/Responsive/css/responsive.bootstrap4.min.css">
<style>
table.dataTable tbody td {
	word-break: break-word;
	vertical-align: top;
}
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main"> <c:if test="${condition == 'SuccessInsertUserRole'}">
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4>
					<i class="icon fa fa-check"></i> Success
				</h4>
				Sukses menambahkan hak akses.
			</div>
		</c:if> <c:if test="${condition == 'SuccessUpdateUserRole'}">
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4>
					<i class="icon fa fa-check"></i> Success
				</h4>
				Sukses memperbaharui hak akses.
			</div>
		</c:if> <c:if test="${condition == 'SuccessDeleteUserRole'}">
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4>
					<i class="icon fa fa-check"></i> Success
				</h4>
				Sukses menghapus hak akses.
			</div>
		</c:if> <c:if test="${condition == 'FailedDeleteUserRole'}">
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4>
					<i class="icon fa fa-ban"></i> Failed
				</h4>
				Gagal menghapus hak akses.
				<c:out value="${conditionDescription}" />
				.
			</div>
		</c:if>
		<div class="container-fluid">
			<div class="animated fadeIn">
				<!-- 				<div class="search-container row"> -->
				<!-- 					<div class="col-lg-3 col-md-3 col-12"> -->
				<!-- 						<button class="btn btn-light btn-warning" id="SubmitNew" type="button" data-toggle="modal" data-target="#addModal"> -->
				<!-- 							<i class="fa fa-plus-circle"></i> Add New Mobile Config -->
				<!-- 						</button> -->
				<!-- 					</div> -->
				<!-- 				</div> -->
				<!-- SEARCH-CONTAINER -->
			</div>
		</div>
		<div class="container-fluid">
			<div class="animated fadeIn">
				<table class="table table-striped table-bordered sortable" id="tb_userrole">
					<thead style="background-color: #d2d6de;">
						<tr>
							<th>Role ID</th>
							<th>Role Name</th>
							<!-- <th></th> -->
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listuserrole}" var="userrole">
							<tr>
								<td><a href="" style="color: #144471; font-weight: bold;" data-toggle="modal" data-target="#ModalUpdateInsert"
									data-luserroleid='<c:out value="${userrole.roleID}" />' data-luserrolename='<c:out value="${userrole.roleName}" />'
									data-lkey="Update"> <c:out value="${userrole.roleID}" />
								</a></td>
								<td><a href="" style="color: #144471; font-weight: bold;" data-toggle="modal" data-target="#ModalUpdateInsert"
									data-luserroleid='<c:out value="${userrole.roleID}" />' data-luserrolename='<c:out value="${userrole.roleName}" />'
									data-lkey="Update"> <c:out value="${userrole.roleName}" />
								</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		</main>
		<!-- Edit Information Modal -->
		<!-- 		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addProviderLabel" aria-hidden="true"> -->
		<!-- 			<div class="modal-dialog" role="document"> -->
		<!-- 				<form id="FormSubmit" > -->
		<!-- 					<div class="modal-content"> -->
		<!-- 						<div class="modal-header"> -->
		<!-- 	<!-- 						<h5 class="modal-title claim-num" id="addProviderLabel">Add New Provider</h5> -->
		<!-- 							<button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
		<!-- 								<span aria-hidden="true">&times;</span> -->
		<!-- 							</button> -->
		<!-- 						</div> -->
		<!-- 						<div class="modal-body"> -->
		<!-- 							<div class="row"> -->
		<!-- 								<input type="hidden" id="mobile_config_id" name="mobile_config_id" readonly> -->
		<!-- 								<div class="col"> -->
		<!-- 									<div class="input-group input-group-sm mt-3"> -->
		<!-- 										<div class="input-group-prepend"> -->
		<!-- 											<span class="input-group-text">Config Type</span> -->
		<!-- 										</div> -->
		<!-- 										<input type="text" id="config_type" name="config_type" class="form-control" style="text-transform: capitalize;"> -->
		<!-- 									</div> -->
		<!-- 									<div class="input-group input-group-sm mt-3"> -->
		<!-- 										<div class="input-group-prepend"> -->
		<!-- 											<span class="input-group-text">Config Value</span> -->
		<!-- 										</div> -->
		<!-- 										<input type="text" id="config_value" name="config_value" class="form-control" style="text-transform: capitalize;"> -->
		<!-- 									</div> -->
		<!-- 								</div> -->
		<!-- 							</div> -->
		<!-- 						</div> -->
		<!-- 						<div class="modal-footer"> -->
		<!-- 							<button class="btn btn-primary" id="SubmitButton" type="submit" >Save changes</button> -->
		<!-- 						</div> -->
		<!-- 					</div> -->
		<!-- 				</form> -->
		<!-- 			</div> -->
		<!-- 		</div> -->
		<!-- MODAL -->
		<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4>
							<i class="icon fa fa-ban"></i> Failed
						</h4>
						<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
					</div>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="exampleModalLabel">
							<label id="lblTitleModalUpdateInsert" name="lblTitleModalUpdateInsert"></label>
						</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="input-group input-group-sm col-md-6">
								<div class="input-group-prepend">
									<span class="input-group-text">Role Id</span>
								</div>
								<input type="text" id="txtRoleId" name="txtRoleId" class="form-control">
							</div>
							<div class="input-group input-group-sm col-md-6">
								<div class="input-group-prepend">
									<span class="input-group-text">Role Name</span>
								</div>
								<input type="text" id="txtRoleName" name="txtRoleName" class="form-control">
							</div>
							<div class="col-md-12">
								<hr>
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<label for="lblMenu" class="control-label">Menu :</label> <br> <select multiple id="slMenu" name="slMenu"
										style="height: 350px; width: 100%;">
										<c:forEach items="${listmenu}" var="menu">
											<c:if test="${menu.level == '0'}">
												<option value="<c:out value="${menu.menuID}" />"><c:out value="${menu.menuName}" /></option>
											</c:if>
											<c:if test="${menu.level == '1'}">
												<option value="<c:out value="${menu.menuID}" />">&nbsp
													<c:out value="${menu.menuName}" /></option>
											</c:if>
											<c:if test="${menu.level == '2'}">
												<option value="<c:out value="${menu.menuID}" />">&nbsp&nbsp&nbsp&nbsp
													<c:out value="${menu.menuName}" /></option>
											</c:if>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<br> <br>
								<div class="input-group" style="width: 150px">
									<button type="button" class="btn btn-light btn-block" onclick="FuncAddMenu()">Add ></button>
									<button type="button" class="btn btn-light btn-block" onclick="FuncAddAllMenu()">Add All >></button>
									<button type="button" class="btn btn-light btn-block" onclick="FuncRemoveMenu()">< Remove</button>
									<button type="button" class="btn btn-light btn-block" onclick="FuncRemoveAllMenu()"><< Remove All</button>
								</div>
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<label for="lblAllowedMenu" class="control-label">Allowed Menu :</label> <select multiple id="slAllowedMenu"
										name="slAllowedMenu" class="form-control" style="height: 350px; width: 100%;">
										<c:forEach items="${listallowedmenu}" var="allowedmenu">
											<c:if test="${allowedmenu.level == '0'}">
												<option value="<c:out value="${allowedmenu.menuID}" />"><c:out value="${allowedmenu.menuName}" /></option>
											</c:if>
											<c:if test="${allowedmenu.level == '1'}">
												<option value="<c:out value="${allowedmenu.menuID}" />">&nbsp
													<c:out value="${allowedmenu.menuName}" /></option>
											</c:if>
											<c:if test="${allowedmenu.level == '2'}">
												<option value="<c:out value="${allowedmenu.menuID}" />">&nbsp&nbsp&nbsp&nbsp
													<c:out value="${allowedmenu.menuName}" /></option>
											</c:if>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="input-group">
									<label for="lblMenuLeverage" class="control-label">Editable Menu :</label> <br>
									<div class="panel panel-primary" id="EditableMenuPanel" style="overflow: auto; height: 350px;">
										<div class="panel-body fixed-panel">
											<c:forEach items="${listeditablemenu}" var="crudmenu">
												<div class="checkbox" id="cbmenu">
													<label><input type="checkbox" name="cbEditableMenu" id="<c:out value="${crudmenu.menuID}" />"
														<%-- name="<c:out value="${crudmenu.menuID}" />" --%>
																				value="<c:out value="${crudmenu.menuID}" />">
														<c:out value="${crudmenu.menuName}" /></label>
												</div>
											</c:forEach>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6" style="weight: 200px">
											<button type="button" class="btn btn-info btn-default" onclick="FuncSelectAll()">Select All</button>
										</div>
										<div class="col-md-6" style="margin-left: -20px">
											<button type="button" class="btn btn-light btn-default" style="width: 100px" onclick="FuncUnselectAll()">Unselect
												All</button>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<hr>
								<button <c:out value="${buttonstatus}"/> type="button" id="btnSave" name="btnSave" class="btn btn-primary"
									onclick="FuncSaveRule()">Save Role</button>
								<button <c:out value="${buttonstatus}"/> type="button" id="btnUpdate" name="btnUpdate" class="btn btn-primary"
									onclick="FuncUpdateRule(this)">Update Role</button>
							</div>
						</div>
					</div>
					<div class="modal-footer"></div>
				</div>
			</div>
		</div>
		<!--modal Delete -->
		<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Alert Delete Role</h4>
					</div>
					<div class="modal-body">
						<input type="hidden" id="temp_txtRoleID" name="temp_txtRoleID" />
						<p>Are you sure to delete this role ?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
						<button type="submit" id="btnDelete" name="btnDelete" class="btn btn-outline">Delete</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script>
		$('#ModalDelete').on('show.bs.modal', function(event) {
			var button = $(event.relatedTarget);
			var lRoleID = button.data('luserroleid');
			$("#temp_txtRoleID").val(lRoleID);
		});

		$('#ModalUpdateInsert').on('shown.bs.modal', function(event) {
			//FuncClearStyleNone();
			$("#dvErrorAlert").hide();

			var button = $(event.relatedTarget);
			var lRoleID = button.data('luserroleid');
			var lRoleName = button.data('luserrolename');
			var lKey = button.data('lkey');
			var modal = $(this);
			$('#btnSave').show();
			$('#btnUpdate').hide();
			$("#txtRoleId").prop('disabled', false);
			$("#txtRoleName").prop('disabled', false);

			//Clear list
			$('#slAllowedMenu option').prop('selected', true);
			$("#slAllowedMenu option:selected").remove();

			$("#EditableMenuPanel :input").prop("checked", false);

			modal.find(".modal-body #txtRoleId").val(lRoleID);
			modal.find(".modal-body #txtRoleName").val(lRoleName);

			if (lKey == "Update") {
				$('#btnSave').hide();
				$('#btnUpdate').show();
				$("#txtRoleId").prop('disabled', true);
				$("#txtRoleName").prop('disabled', true);

				//<<Javascript For 1 json
				$.ajax({
				url : '${pageContext.request.contextPath}/getMenu',
				type : 'POST',
				data : {
				menuid : lRoleID,
				command : lKey
				},
				dataType : 'json'
				}).done(function(data) {
					console.log(data);
					var data1 = data[0];
					var data2 = data[1];

					for ( var i in data1) {
						var obj = data1[i];
						var index = 0;
						var id, name, url, type, level;
						for ( var prop in obj) {
							switch (index++) {
							case 0:
								id = obj[prop];
								break;
							case 1:
								name = obj[prop];
								break;
							case 2:
								url = obj[prop];
								break;
							case 3:
								type = obj[prop];
								break;
							case 4:
								level = obj[prop];
								break;
							default:
								break;
							}
						}
						if ($("#slAllowedMenu option[value=" + id + "]").length > 0) {
						} else {
							if (level == '0')
								$('#slAllowedMenu').append("<option id=\"" + id + "\" value=\"" + id + "\">" + name + "</option>");
							if (level == '1')
								$('#slAllowedMenu').append("<option id=\"" + id + "\" value=\"" + id + "\">&nbsp" + name + "</option>");
							if (level == '2')
								$('#slAllowedMenu').append("<option id=\"" + id + "\" value=\"" + id + "\">&nbsp&nbsp&nbsp&nbsp" + name + "</option>");
						}
					}

					for ( var i in data2) {
						var obj = data2[i];
						modal.find(".modal-body #" + obj.MenuID).prop('checked', true);

						var el = document.getElementById(obj.MenuID);
						//if (el.style.display = 'none')
						//el.style.display = '';

					}
				}).fail(function() {
					console.log('Service call failed!');
				})
			}
		})

		function FuncSelectAll() {
			$("#EditableMenuPanel :input").prop("checked", true);
		}

		function FuncUnselectAll() {
			$("#EditableMenuPanel :input").prop("checked", false);
		}

		function FuncSaveRule() {
			var listMenu = document.getElementById('slMenu');
			var listAllowedMenu = document.getElementById('slAllowedMenu');
			var optionAllMenu = new Array();
			var optionVal = new Array();
			var checkVal = new Array();
			var txtRoleId = document.getElementById('txtRoleId').value;
			var txtRoleName = document.getElementById('txtRoleName').value;

			for (x = 0; x < listMenu.length; x++) {
				optionAllMenu.push(listMenu.options[x].value);
			}

			for (i = 0; i < listAllowedMenu.length; i++) {
				optionVal.push(listAllowedMenu.options[i].value);
			}

			$('input[name="cbEditableMenu"]:checked').each(function() {
				checkVal.push(this.value);
			});

			jQuery.ajax({
			url : '${pageContext.request.contextPath}/userrole',
			type : 'POST',
			dataType : 'text',
			data : {
			"key" : 'saverule',
			"listAllMenu" : optionAllMenu,
			"listAllowedMenu" : optionVal,
			"listEditableMenu" : checkVal,
			"txtRoleId" : txtRoleId,
			"txtRoleName" : txtRoleName
			},
			success : function(data, textStatus, jqXHR) {
				if (data.split("--")[0] == 'FailedInsertUserRole') {
					$("#dvErrorAlert").show();
					document.getElementById("lblAlert").innerHTML = "Gagal menambahkan hak akses";
					document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
					$("#txtRoleId").focus();
					$("#ModalUpdateInsert").animate({
						scrollTop : 0
					}, 'slow');
					return false;
				} else {
					var url = '${pageContext.request.contextPath}/userrole';
					$(location).attr('href', url);
				}
			},
			error : function(data, textStatus, jqXHR) {
				console.log('Service call failed!');
			}
			});
			return true;
		}

		function FuncUpdateRule() {

			// 	FuncClearStyleNone()
			var listMenu = document.getElementById('slMenu');
			var listAllowedMenu = document.getElementById('slAllowedMenu');
			var optionAllMenu = new Array();
			var optionVal = new Array();
			var checkVal = new Array();
			var txtRoleId = $("#txtRoleId").val();
			var txtRoleName = $("#txtRoleName").val();

			for (x = 0; x < listMenu.length; x++) {
				optionAllMenu.push(listMenu.options[x].value);
			}

			for (i = 0; i < listAllowedMenu.length; i++) {
				optionVal.push(listAllowedMenu.options[i].value);
			}

			$('input[name="cbEditableMenu"]:checked').each(function() {
				checkVal.push(this.value);
			});
			jQuery.ajax({
			url : '${pageContext.request.contextPath}/userrole',
			type : 'POST',
			dataType : 'text',
			data : {
			"key" : 'updaterule',
			"listAllMenu" : optionAllMenu,
			"listAllowedMenu" : optionVal,
			"listEditableMenu" : checkVal,
			"txtRoleId" : txtRoleId,
			"txtRoleName" : txtRoleName
			},
			success : function(data, textStatus, jqXHR) {
				if (data.split("--")[0] == 'FailedUpdateUserRole') {
					$("#dvErrorAlert").show();
					document.getElementById("lblAlert").innerHTML = "Gagal memperbaharui hak akses";
					document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
					$("#txtRoleName").focus();
					$("#ModalUpdateInsert").animate({
						scrollTop : 0
					}, 'slow');
					return false;
				} else {
					var url = '${pageContext.request.contextPath}/userrole';
					$(location).attr('href', url);
				}
			},
			error : function(data, textStatus, jqXHR) {
				alert('zxczxczxc')
				console.log('Service call failed!');
			}
			});
			return true;

		}

		function FuncAddMenu() {
			var MenuID = document.getElementById('slMenu').value;
			var Command = "Add";

			$.ajax({
			url : '${pageContext.request.contextPath}/getMenu',
			type : 'POST',
			data : {
			menuid : MenuID,
			command : Command
			},
			dataType : 'json'
			}).done(function(data) {
				console.log(data);
				for ( var i in data) {
					var obj = data[i];
					var index = 0;
					var id, name, url, type, level;
					for ( var prop in obj) {
						switch (index++) {
						case 0:
							id = obj[prop];
							break;
						case 1:
							name = obj[prop];
							break;
						case 2:
							url = obj[prop];
							break;
						case 3:
							type = obj[prop];
							break;
						case 4:
							level = obj[prop];
							break;
						default:
							break;
						}
					}

					if ($("#slAllowedMenu option[value=" + id + "]").length > 0) {
					} else {
						if (level == '0')
							$('#slAllowedMenu').append("<option id=\"" + id + "\" value=\"" + id + "\">" + name + "</option>");
						if (level == '1')
							$('#slAllowedMenu').append("<option id=\"" + id + "\" value=\"" + id + "\">&nbsp" + name + "</option>");
						if (level == '2')
							$('#slAllowedMenu').append("<option id=\"" + id + "\" value=\"" + id + "\">&nbsp&nbsp&nbsp&nbsp" + name + "</option>");
					}

				}

			}).fail(function() {
				console.log('Service call failed!');
			})
		}

		function FuncAddAllMenu() {

			var MenuID = "All";
			var Command = "Add";

			$.ajax({
			url : '${pageContext.request.contextPath}/getMenu',
			type : 'POST',
			data : {
			menuid : MenuID,
			command : Command
			},
			dataType : 'json'
			}).done(function(data) {
				console.log(data);
				for ( var i in data) {
					var obj = data[i];
					var index = 0;
					var id, name, url, type, level;
					for ( var prop in obj) {
						switch (index++) {
						case 0:
							id = obj[prop];
							break;
						case 1:
							name = obj[prop];
							break;
						case 2:
							url = obj[prop];
							break;
						case 3:
							type = obj[prop];
							break;
						case 4:
							level = obj[prop];
							break;
						default:
							break;
						}
					}

					if ($("#slAllowedMenu option[value=" + id + "]").length > 0) {

					} else {
						if (level == '0')
							$('#slAllowedMenu').append("<option id=\"" + id + "\" value=\"" + id + "\">" + name + "</option>");
						if (level == '1')
							$('#slAllowedMenu').append("<option id=\"" + id + "\" value=\"" + id + "\">&nbsp" + name + "</option>");
						if (level == '2')
							$('#slAllowedMenu').append("<option id=\"" + id + "\" value=\"" + id + "\">&nbsp&nbsp&nbsp&nbsp" + name + "</option>");
					}

				}

			}).fail(function() {

			})
		}

		function FuncRemoveMenu() {
			var MenuID = document.getElementById('slAllowedMenu').value;
			var Command = "Remove";

			$.ajax({
			url : '${pageContext.request.contextPath}/getMenu',
			type : 'POST',
			data : {
			menuid : MenuID,
			command : Command
			},
			dataType : 'json'
			}).done(function(data) {
				console.log(data);
				for ( var i in data) {
					var obj = data[i];
					var index = 0;
					var id, name, url, type, level;
					for ( var prop in obj) {
						switch (index++) {
						case 0:
							id = obj[prop];
							break;
						case 1:
							name = obj[prop];
							break;
						case 2:
							url = obj[prop];
							break;
						case 3:
							type = obj[prop];
							break;
						case 4:
							level = obj[prop];
							break;
						default:
							break;
						}
					}

					$("#slAllowedMenu option[value=" + id + "]").remove();
				}

			}).fail(function() {
				console.log('Service call failed!');
			})
		}

		function FuncRemoveAllMenu() {
			$('#slAllowedMenu option').prop('selected', true);
			$("#slAllowedMenu option:selected").remove();
		}
	</script>
</body>
</html>
