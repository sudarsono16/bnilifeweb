package com.bnilife;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.MenuAdapter;
import adapter.RoleAdapter;
import adapter.ValidateNull;
import model.Globals;

@WebServlet(urlPatterns = { "/userrole" }, name = "userrole")
public class UserRoleServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public UserRoleServlet() {
	super();
	// TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	Boolean CheckMenu;
	String MenuURL = "userrole";
	
	HttpSession session = request.getSession();
	String user_id = (String) session.getAttribute("user_id");
	Globals.user_id = user_id;
	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user_id);
	
	

	if (CheckMenu == false) {
	    RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
	    dispacther.forward(request, response);
	} else {
	    // User Role
	    List<model.mdlRole> UserRoleList = new ArrayList<model.mdlRole>();
	    UserRoleList.addAll(RoleAdapter.LoadRole());
	    request.setAttribute("listuserrole", UserRoleList);

	    // -- load all menu list
	    List<model.mdlMenu> mdlMenuList = new ArrayList<model.mdlMenu>();
	    mdlMenuList.addAll(MenuAdapter.LoadMenu());
	    request.setAttribute("listmenu", mdlMenuList);
	    // end of load all menu list --

	    // -- load editable menu list
	    List<model.mdlMenu> EditableMenulist = new ArrayList<model.mdlMenu>();
	    EditableMenulist.addAll(MenuAdapter.LoadEditableMenu());
	    request.setAttribute("listeditablemenu", EditableMenulist);
	    // end of load editable menu list --

	    String ButtonStatus;
	    ButtonStatus = RoleAdapter.SetMenuButtonStatus(MenuURL, Globals.user_id);

	    request.setAttribute("condition", Globals.gCondition);
	    request.setAttribute("buttonstatus", ButtonStatus);
	    request.setAttribute("conditionDescription", Globals.gConditionDesc);

	    RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/userrole.jsp");
	    dispacther.forward(request, response);
	}

	Globals.gCondition = "";
	Globals.gConditionDesc = "";
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	HttpSession session = request.getSession();
	Globals.user_id = (String) session.getAttribute("user_id");
	String lResult = "";

	if (Globals.user_id == null || Globals.user_id == "") {
	    return;
	}

	// Declare button
	String keyBtn = ValidateNull.NulltoStringEmpty(request.getParameter("key"));
	String btnDelete = request.getParameter("btnDelete");

	if (keyBtn.equals("saverule")) {
	    // Declare parameter
	    String[] listAllMenu = request.getParameterValues("listAllMenu[]");
	    String[] listAllowedMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listAllowedMenu[]"));
	    String[] listEditableMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listEditableMenu[]"));
	    String txtRoleId = request.getParameter("txtRoleId");
	    String txtRoleName = request.getParameter("txtRoleName");

	    lResult = MenuAdapter.InsertUserRule(txtRoleId, txtRoleName, listAllMenu, listAllowedMenu, listEditableMenu);

	    if (lResult.contains("SuccessInsert")) {
		Globals.gCondition = "SuccessInsertUserRole";
	    } else {
		Globals.gCondition = "FailedInsertUserRole";
		Globals.gConditionDesc = lResult;
	    }
	}

	if (keyBtn.equals("updaterule")) {
	    // Declare parameter
	    String[] listAllMenu = request.getParameterValues("listAllMenu[]");
	    String[] listAllowedMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listAllowedMenu[]"));
	    String[] listEditableMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listEditableMenu[]"));
	    String txtRoleId = request.getParameter("txtRoleId");
	    String txtRoleName = request.getParameter("txtRoleName");

	    lResult = MenuAdapter.UpdateUserRule(txtRoleId, txtRoleName, listAllMenu, listAllowedMenu, listEditableMenu);

	    if (lResult.contains("SuccessUpdate")) {
		Globals.gCondition = "SuccessUpdateUserRole";
	    } else {
		Globals.gCondition = "FailedUpdateUserRole";
		Globals.gConditionDesc = lResult;
	    }
	}

	if (btnDelete != null) {
	    String temp_txtRoleID = ValidateNull.NulltoStringEmpty(request.getParameter("temp_txtRoleID"));
	    lResult = MenuAdapter.DeleteUserRule(temp_txtRoleID);

	    if (lResult.contains("SuccessDelete")) {
		Globals.gCondition = "SuccessDeleteUserRole";
	    } else {
		Globals.gCondition = "FailedDeleteUserRole";
		Globals.gConditionDesc = lResult;
	    }

	    doGet(request, response);

	    return;
	}

	response.setContentType("application/text");
	response.setCharacterEncoding("UTF-8");
	response.getWriter().write(Globals.gCondition + "--" + Globals.gConditionDesc);

	return;
    }
}
