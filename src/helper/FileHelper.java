package helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileHelper {
    public static boolean writeToFile(InputStream uploadedInputStream, String directory, String uploadedFileLocation) {
	File direct = new File(directory);
	if (!direct.exists()) {
	    direct.mkdir();
	}
	try {
	    int read = 0;
	    byte[] bytes = new byte[1024];

	    OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
	    while ((read = uploadedInputStream.read(bytes)) != -1) {
		out.write(bytes, 0, read);
	    }
	    out.flush();
	    out.close();
	    return true;
	} catch (IOException e) {
	    e.printStackTrace();
	    return false;
	}
    }

    public static boolean RenameFile(String directory, String oldFilePath, String newFilePath) {
	File direct = new File(directory);
	if (!direct.exists()) {
	    direct.mkdir();
	}

	File oldFile = new File(oldFilePath);
	File newFile = new File(newFilePath);

	return oldFile.renameTo(newFile);
    }

    public static boolean RenameDirectory(String oldFilePath, String newFilePath) {
	File oldFile = new File(oldFilePath);
	File newFile = new File(newFilePath);

	return oldFile.renameTo(newFile);
    }
}
