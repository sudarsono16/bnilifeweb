package adapter;

import adapter.Base64Adapter;

public class ClientIdAdapter {
	public static String getAuthorizationKey(String ClientID, String ClientSecret){
		String stringToSign = ClientID + ";" + ClientSecret;
		String authorizationKey  = Base64Adapter.EncryptBase64(stringToSign); //generate apiKey
		return authorizationKey;
	}
}
