package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;

public class RoleAdapter {
    public static String SetMenuButtonStatus(String MenuURL, String UserID) {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
	String sql = "";
	CachedRowSet rowset = null;
	String ButtonStatus = "enabled";
	try {
	    sql = "SELECT a.IsModify FROM ms_access_role a INNER JOIN ms_menu b ON b.MenuID = a.MenuID INNER JOIN ms_user_cms c ON c.RoleID = a.RoleID "
	    	+ "WHERE b.Url = ? AND c.username = ? ";
	    listParam.add(QueryExecuteAdapter.QueryParam("string", MenuURL));
	    listParam.add(QueryExecuteAdapter.QueryParam("string", UserID));
	    rowset = QueryExecuteAdapter.QueryExecute(sql, listParam, functionName);

	    while (rowset.next()) {
		if (rowset.getBoolean("IsModify") == false) {
		    ButtonStatus = "disabled";
		}
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, sql, "");
	}

	return ButtonStatus;
    }

    public static List<model.mdlRole> LoadRole() {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String sql = "";
	CachedRowSet rowset = null;
	List<model.mdlRole> mdlRoleList = new ArrayList<model.mdlRole>();
	try {
	    sql = "SELECT RoleID, Name FROM ms_role ORDER BY RoleID ASC";
	    rowset = QueryExecuteAdapter.QueryExecute(sql, functionName);

	    while (rowset.next()) {
		model.mdlRole mdlRole = new model.mdlRole();
		mdlRole.setRoleID(rowset.getString("RoleID"));
		mdlRole.setRoleName(rowset.getString("Name"));
		mdlRoleList.add(mdlRole);
	    }
	} catch (Exception ex) {
	    LogAdapter.InsertLogExc(ex.toString(), functionName, sql, Globals.user_id);
	}

	return mdlRoleList;
    }
}
