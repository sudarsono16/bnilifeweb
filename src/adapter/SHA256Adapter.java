package adapter;

import java.security.MessageDigest;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class SHA256Adapter {
	public static String sha256(String base) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if (hex.length() == 1) {
					hexString.append('0');
				}
				hexString.append(hex);
			}
			return hexString.toString();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public static String generateHmacSHA256Signature(String data, String key) { /* throws GeneralSecurityException, IOException */ 
		String algorithm = "HmacSHA256"; // OPTIONS= HmacSHA512, HmacSHA256, HmacSHA1, HmacMD5
		String hash = "";
		try {
			// 1. Get an algorithm instance.
			Mac sha256_hmac = Mac.getInstance(algorithm);
	
			// 2. Create secret key.
			SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);
	
			// 3. Assign secret key algorithm.
			sha256_hmac.init(secret_key);
	
			// Encode the string into bytes using utf-8 and digest it
			byte[] digest = sha256_hmac.doFinal(data.getBytes("UTF-8"));
	
			// Convert digest into a hex string
			hash = byteArrayToHex(digest);
	
		} catch (Exception e) {
	
		}
		return hash;
	}

	public static String byteArrayToHex(byte[] a) {
		StringBuilder sb = new StringBuilder(a.length * 2);
		for (byte b : a) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}
}
