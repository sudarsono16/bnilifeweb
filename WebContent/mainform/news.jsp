<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
<link href="mainform/css/quill.snow.css" rel="stylesheet">
<style>
#sortable {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 60%;
}

#sortable li {
	margin: 0 3px 3px 3px;
	padding: 0.4em;
	padding-left: 1.5em;
	font-size: 1.4em;
	height: 18px;
}

#sortable li span {
	position: absolute;
	margin-left: -1.3em;
}
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<div class="main">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="search-container row">
						<div class="col-lg-3 col-md-3 col-12">
							<button class="btn btn-block btn-warning" type="button" id="ShowAddDoc">
								<i class="fa fa-plus-circle"></i> Create Post
							</button>
						</div>
						<div class="col-lg-3 col-md-3 col-12"></div>
						<div class="col-lg-3 col-md-3 col-12"></div>
					</div>
					<!-- SEARCH-CONTAINER -->
					<div class="animated fadeIn">
						<div class="row dragdrop"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="addDoc" tabindex="-1" role="dialog" aria-labelledby="addDocLabel" aria-hidden="true"
			data-backdrop="static">
			<div class="modal-dialog modal-lg" role="document">
				<form id='FormSubmit'>
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="addDocLabel">Create New Post</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<input type="hidden" id="news_id"> <input type="hidden" id="submit_type">
							<div class="card-body">
								<div class="form-group">
									<div class="container">
										<!-- 										<label for="news_link">File</label> -->
										<div class="row">
											<div class="col-md-4">
												<img id="image-template" class="rounded float-left img-fluid"
													src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_164e3f66702%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_164e3f66702%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274.4296875%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
													data-holder-rendered="true" style="margin: 5px;">
											</div>
											<div class="col-md-8 my-auto">
												<div class="input-group">
													<input type="file" class="custom-file-input " name="customFile" id="customFile" onchange="change_image(this)">
													<label class="custom-file-label" for="customFile">Choose file</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Tipe Berita</span>
									</div>
									<select class="custom-select form-control" name="news_type" id="news_type"></select>
								</div>
								<br>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">News Link</span>
									</div>
									<input type="text" class="form-control" name="news_link" id="news_link" placeholder="Link">
								</div>
								<br>
								<ul class="nav nav-tabs" role="tablist">
									<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#add-ID" role="tab"
										aria-controls="Indonesia">Indonesia</a></li>
									<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#add-EN" role="tab" aria-controls="English">English</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="add-ID" role="tabpanel">
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text">Title</span>
											</div>
											<input type="text" name="title_id" id="title_id" class="form-control" placeholder="Title" required>
										</div>
										<br>
										<div id="title_id_validation"></div>
										<div id="editor_id"></div>
										<div id="editor_id_validation"></div>
									</div>
									<div class="tab-pane" id="add-EN" role="tabpanel">
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text">Title</span>
											</div>
											<input type="text" name="title_en" id="title_en" class="form-control" placeholder="Title" required>
										</div>
										<br>
										<div id="title_en_validation"></div>
										<div id="editor_en"></div>
										<div id="editor_en_validation"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button type="submit" class="btn btn-primary" id="save_news">Save changes</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- MODAL -->
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script src="mainform/js/jquery-ui.min.js"></script>
	<script src="mainform/plugins/quill.min.js"></script>
	<script type="text/javascript">
	var toolbarOptions = [
		['bold', 'italic', 'underline', 'strike'],
		['blockquote', 'code-block'], 
		[{'header': 1},{'header': 2}],
		[{'list': 'ordered'},{'list': 'bullet'}],
		[{'script': 'sub'},{'script': 'super'}],
		[{'indent': '-1'}, {'indent': '+1'}],
		[{'direction': 'rtl'}],
		[{'size': ['small', false, 'large', 'huge']}],
		[{'header': [1, 2, 3, 4, 5, 6, false]}],
		[{'color': []}, {'background': []}],
		[{'font': []}],
		[{'align': []}],
		['clean'],
		['link', 'image','video']
	];
	
	var quill_id = new Quill('#editor_id', {
		modules: {
			toolbar: toolbarOptions
		},
		theme: 'snow'
	});
	
	var quill_en = new Quill('#editor_en', {
		modules: {
			toolbar: toolbarOptions
		},
		theme: 'snow'
	});
	
	function update_function(ID){
		var input_parameter = {};
		input_parameter.APIUrl = "/news/get-news?type=ALL&id="+ID;
		input_parameter.method = "GET";

		$.ajax({
			url : url_local_web+"/APICall",
			type: 'POST',
			data: JSON.stringify(input_parameter),
			dataType: "json",
			headers: {
				'Content-Type': 'application/json'
			},
			beforeSend: function() {},
		}).done(function (data) {
			var data_selected = data.output_schema.items[0];
			console.log(data_selected);
			$('#news_id').val(data_selected.news_id);
			$('#news_link').val(data_selected.news_link);
			$('#title_id').val(data_selected.news_title_id);
			$('#title_en').val(data_selected.news_title_en);
			
			$('#image-template').attr('src', data_selected.news_image);
			populate_select_news_type(data_selected.news_type);
			quill_id.root.innerHTML = data_selected.news_content_id;
			quill_en.root.innerHTML = data_selected.news_content_en;
			
			validate_start_function('update');
			
			$('#addDoc').modal('show');
		}).fail(function (jqXHR, textStatus) {
			console.log("failed " + jqXHR + " " + textStatus);
		});
		
	}
	
	function populate_select_news_type(news_type){
		$('#news_type').removeAttr('selected').find('[value=""]').attr('selected','selected').prop("selected",true);
		if(!$("#news_type").find("option:selected").length){
			var types = [];
			types.push({"ID":"","Text":"Pilih Tipe Berita"});
			types.push({"ID":"OGH","Text":"OGH"});
			types.push({"ID":"OGS","Text":"OGS"});
			
			$.each(types, function (key, value) {
				$('#news_type').append($('<option>', {
					value: value.ID,
					text: value.Text
				}));
			});
		}
		if(typeof news_type != 'undefined' && news_type != null){
			$('#news_type').removeAttr('selected').find('[value="'+news_type+'"]').attr('selected','selected').prop("selected",true);
		}
	}
	
	function DeleteNews(ID) {
		Swal.fire({
			title: 'Are you sure?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete News'
		}).then((result) => {
			if (result.value) {
				var input_parameter = {};
				input_parameter.APIUrl = "/news/delete-news?id="+ID;
				input_parameter.method = "GET";
				
				$.ajax({
					url : url_local_web+"/APICall",
					type: 'POST',
					data: JSON.stringify(input_parameter),
					dataType: "json",
					headers: {
						'Content-Type': 'application/json'
					},
					beforeSend: function() {
					},
				}).done(function (data) {
					console.log(data);
					if(data.error_schema.error_code == "ERR-00-000"){
						Swal.fire({
							type: 'success',
							title: 'Alert',
							text: "Delete success"
						});
					}
					else{
						Swal.fire({
							type: 'error',
							title: 'Alert',
							text: "Delete failed"
						});
					}
					drawCards();
				}).fail(function (jqXHR, textStatus) {
					console.log("failed " + jqXHR + " " + textStatus);
					Swal.fire({
						type: 'error',
						title: 'Alert',
						text: "Delete error"
					});
				});
				
			}
		});
		
	}
	
	function drawCards(ID){
		var input_parameter = {};
		input_parameter.APIUrl = "/news/get-news?type=ALL";
		input_parameter.method = "GET";
		if(ID != null){
			input_parameter.news_id = ID;
		}
		
		$.ajax({
			url : url_local_web+"/APICall",
			type: 'POST',
			data: JSON.stringify(input_parameter),
			dataType: "json",
			headers: {
				'Content-Type': 'application/json'
			},
			beforeSend: function() {},
		}).done(function (data) {
			console.log(data);
			if(typeof data.output_schema != "undefined"){
				var title_array = [];
				var body_array = [];
				$.each(data.output_schema.items, function (index, value) {
					title_array.push({news_id: value.news_id, news_title_en: value.news_title_en, news_title_id: value.news_title_id });
					body_array.push({news_id: value.news_id, news_content_en: value.news_content_en, news_content_id: value.news_content_id });
				});
				printTemplate(title_array, body_array);
			}
		}).fail(function (jqXHR, textStatus) {
			console.log("failed " + jqXHR + " " + textStatus);
		});
	}
	
	function printTemplate(title_array, body_array) {
		$('.dragdrop').empty();
		var title_array_string = "";
		$.each(title_array, function (index, value) {
			if(index == 0){
				var active_string_body = 'active show';
			}
			
			title_array_string += '<a class="list-group-item list-group-item-action '+active_string_body+'" id="content-list-'+index+'" data-toggle="tab" href="#content-'+index+'" role="tab" aria-controls="content-home-'+index+'" aria-selected="true">';
			title_array_string += '<div class="row h-100">'
			title_array_string += '<div class="col-lg-10">'+value.news_title_en+' <br><small>'+value.news_title_id+"</div>";
			title_array_string += '<div class="col-lg-2 my-auto">';
			title_array_string += '<button type="button" class="btn btn-danger btn-sm button_delete" style="width: 35px;" onclick="DeleteNews('+value.news_id+')" ><i class="fa fa-trash-o"></i></button>';
			title_array_string += '</div>';
			title_array_string += '</div>'
			title_array_string += '</small> </a>';
			
		});
		
		var body_array_string = "";
		$.each(body_array, function (index, value) {
			if(index == 0){
				var active_string_body = 'active show';
			}
			body_array_string += 
				'<div class="tab-pane fade '+active_string_body+' " id="content-'+index+'" role="tabpanel" aria-labelledby="content-'+index+'">' + 
					'<ul class="nav nav-tabs" role="tablist">' + 
						'<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#content-ID-'+index+'" role="tab" aria-controls="Bahasa">Bahasa</a></li>' + 
						'<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#content-EN-'+index+'" role="tab" aria-controls="English">English</a></li>' + 
					'</ul>' + 
					'<div class="tab-content">' + 
						'<div class="tab-pane active" id="content-ID-'+index+'" role="tabpanel">' + 
							value.news_content_id + 
							'<button type="button" class="btn btn-secondary" data-toggle="modal" onclick=update_function('+value.news_id+')><i class="fa fa-edit mr-2"></i>Edit</button>' + 
						'</div>' + 
						'<div class="tab-pane" id="content-EN-'+index+'" role="tabpanel">' + 
							value.news_content_en + 
							'<button type="button" class="btn btn-secondary" data-toggle="modal" onclick=update_function('+value.news_id+')><i class="fa fa-edit mr-2"></i>Edit</button>' + 
						'</div>' + 
					'</div>' + 
				'</div>' ;
		});
		
		var template = 
			'<div class="col-md-4 content_title_div ">' + 
				'<div class="list-group" id="title-list-tab" role="tablist">' + 
				title_array_string + 
				'</div>' + 
			'</div>' + 
			'<div class="col-8 content_body_div ">' + 
				'<div class="tab-content" id="nav-tabContent">' + 
				body_array_string + 
				'</div>' + 
			'</div>';
		$('.dragdrop').append(template);
		
	}
	
	function reset_form(){
		console.log('reset starts');
		
		$('#image-template').attr('src', 'data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_164e3f66702%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_164e3f66702%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274.4296875%22%20y%3D%22104.5%22%3EIMAGE%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E');
		quill_id.setContents([]);
		quill_en.setContents([]);
		
		$('.custom-file-label').empty();
		
		$("#FormSubmit")[0].reset();
		$('#news_id').val('');
		populate_select_news_type();
		$form = $('form');
		$form.find('.control-group').removeClass('.error');
		$form.find('.has-error').removeClass("has-error");
		$form.find('.has-success').removeClass("has-success");
		$form.find('.form-control').css({
			'border-color': ''
		});
		$form.find('.form-control-feedback').remove();
	}
	
	function validate_start_function(SubmitType){
		console.log('validation starts: '+SubmitType);
		var SubmitTypeHandler = SubmitType;
		var validate_start = $("#FormSubmit").validate({
			ignore: [],
			focusCleanup: true,
			rules: {
				title_id: {
					required: true
				},
				title_en: {
					required: true
				},
				news_type: {
					required: true
				}
			},
			messages: {},
			highlight: function (element, errorClass) {
				$(element).css({
					'border-color': 'red',
					'display': 'inline',
					'color': 'red'
				});
			},
			unhighlight: function (element, errorClass) {
				$(element).css({
					'border-color': '',
					'display': 'inline',
					'color': ''
				});
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function (error, element) {
				error.css({
					'font-size': '10px',
					'display': 'inline',
					'color': 'red'
				});
				
				element.css({
					'border-color': 'red'
				});
				
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				}
				else {
					error.insertAfter(element);
				}
			},
			submitHandler: function (form) {
				if(!quill_id.editor.isBlank() && !quill_en.editor.isBlank() ){
					var input_parameter = {};
					input_parameter.news_title_id = $('#title_id').val();
					input_parameter.news_title_en = $('#title_en').val();
					input_parameter.news_content_id = quill_id.root.innerHTML;
					input_parameter.news_content_en = quill_en.root.innerHTML;
					input_parameter.news_link = $('#news_link').val();
					input_parameter.news_type = $('#news_type').val();
	
					console.log("SubmitType: "+SubmitTypeHandler);
					if(SubmitTypeHandler == "update"){
						input_parameter.news_id = $('#news_id').val();
					}

					input_parameter.APIUrl = "/news/insert-update-news";
					input_parameter.method = "POST";
	
					var formData = new FormData();
					formData.append('json', JSON.stringify(input_parameter));
					if (($("#customFile"))[0].files.length > 0) {
						formData.append('file', $('#customFile')[0].files[0]);
					}
	
					$.ajax({
						url : url_local_web+"/APICall",
						type: 'POST',
						enctype: 'multipart/form-data',
						cache: false,
						contentType: false,
						processData: false,
						data: formData,
						dataType: "json",
						beforeSend: function() {
						},
					}).done(function (data) {
						console.log(data);
						if(data.error_schema.error_code == "ERR-00-000"){							
							Swal.fire({
								allowOutsideClick: false,
								type: 'success',
								title: 'Alert',
								text: "Penyimpanan data berhasil"
							}).then((result) => {
								if (result.value) {
									location.reload();
								}
							});
						}
						else{
							Swal.fire({
								type: 'error',
								title: 'Alert',
								text: "Penyimpanan data gagal"
							});
						}
						drawCards();
						$('#addDoc').modal('hide');
					}).fail(function (jqXHR, textStatus) {
						console.log("failed " + jqXHR + " " + textStatus);
						Swal.fire({
							type: 'error',
							title: 'Alert',
							text: message_type_failed
						});
					});
				}
			},
			invalidHandler: function (event, validator) {
				var errors = validator.numberOfInvalids();
				console.log("error " + errors);
			}
		});
		jQuery.extend(jQuery.validator.messages, {
			number: "Please enter only number."
		});
		validate_start.resetForm();
		console.log("E");
	}
	
	$(document).ready(function() {
		var news_id = null;
		
		drawCards();
		
		$('#addDoc').on('hide.bs.modal', function() { 
			reset_form();
		});

		$('#ShowAddDoc').on("click", function() {
			reset_form();
			validate_start_function('new');
			$('#addDoc').modal('show');
		});
		
	});
	
	function change_image(input) {
		var file = input.files && input.files[0];
		if (file) {
            $(input).next('.custom-file-label').html(file.name);
            
			var img = new Image();
	        img.src = window.URL.createObjectURL( file );
// 			console.log(file.type);
	        img.onload = function() {
            	loadMime(file, function(mime) {
            		//console.log("image type: "+mime);
            		if(mime == "image/jpeg"){
            			var reader = new FileReader();
    	    			reader.onload = function (e) {
    	    				$(input).closest('.row').find('#image-template').attr('src', e.target.result);
    	    			};
    	    			reader.readAsDataURL(file);
            		}
            		else{
            			alert("Format gambar harus JPG");
            		}
                });
	        };
			
		}
	}
	
	function loadMime(file, callback) {
	    
	    //List of known mimes
	    var mimes = [
	        {
	            mime: 'image/jpeg',
	            pattern: [0xFF, 0xD8, 0xFF],
	            mask: [0xFF, 0xFF, 0xFF],
	        },
	        {
	            mime: 'image/png',
	            pattern: [0x89, 0x50, 0x4E, 0x47],
	            mask: [0xFF, 0xFF, 0xFF, 0xFF],
	        }
	        // you can expand this list @see https://mimesniff.spec.whatwg.org/#matching-an-image-type-pattern
	    ];

	    function check(bytes, mime) {
	        for (var i = 0, l = mime.mask.length; i < l; ++i) {
	            if ((bytes[i] & mime.mask[i]) - mime.pattern[i] !== 0) {
	                return false;
	            }
	        }
	        return true;
	    }

	    var blob = file.slice(0, 4); //read the first 4 bytes of the file

	    var reader = new FileReader();
	    reader.onloadend = function(e) {
	        if (e.target.readyState === FileReader.DONE) {
	            var bytes = new Uint8Array(e.target.result);

	            for (var i=0, l = mimes.length; i<l; ++i) {
	                //if (check(bytes, mimes[i])) return callback("Mime: " + mimes[i].mime + " <br> Browser:" + file.type);
	            	if (check(bytes, mimes[i])) return callback(file.type);
	            }

	            //return callback("Mime: unknown <br> Browser:" + file.type);
	            return file.type;
	        }
	    };
	    reader.readAsArrayBuffer(blob);
	}
	</script>
</body>
</html>
