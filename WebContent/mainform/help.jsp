<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="mainform/plugins/datatables/extensions/Responsive/css/responsive.bootstrap4.min.css">
<link href="mainform/css/quill.snow.css" rel="stylesheet">
<style>
table.dataTable tbody td {
	word-break: break-word;
	vertical-align: top;
}
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
		<div class="container-fluid">
			<div class="animated fadeIn">
				<div class="search-container row">
					<div class="col-lg-3 col-md-3 col-12">
						<button class="btn btn-block btn-warning" id="SubmitNew" type="button" data-toggle="modal" data-target="#addModal">
							<i class="fa fa-plus-circle"></i> Add Help
						</button>
					</div>
				</div>
				<!-- SEARCH-CONTAINER -->
			</div>
		</div>
		<div class="container-fluid">
			<div class="animated fadeIn">
				<table class="table table-striped table-bordered sortable" id="main_table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Title</th>
							<th>Title EN</th>
							<th>Content</th>
							<th>Content EN</th>
							<th>Order</th>
							<th></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
		</main>
		<!-- Edit Information Modal -->
		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addProviderLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<form id="FormSubmit">
					<div class="modal-content">
						<div class="modal-header">
							<!-- 						<h5 class="modal-title claim-num" id="addProviderLabel">Add New Provider</h5> -->
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<input type="hidden" id="help_id" name="help_id" readonly>
								<div class="col">
									<br>
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#add-ID" role="tab"
											aria-controls="Indonesia">Indonesia</a></li>
										<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#add-EN" role="tab" aria-controls="English">English</a>
										</li>
									</ul>
									<div class="tab-content">
										<br>
										<div class="tab-pane active" id="add-ID" role="tabpanel">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">Title ID</span>
												</div>
												<input type="text" id="title_id" name="title_id" class="form-control" style="text-transform: capitalize;">
											</div>
											<br>
											<div id="editor_id" style="width: 100%"></div>
											<div id="editor_id_validation"></div>
										</div>
										<div class="tab-pane" id="add-EN" role="tabpanel">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">Title EN</span>
												</div>
												<input type="text" id="title_en" name="title_en" class="form-control" style="text-transform: capitalize;">
											</div>
											<br>
											<div id="editor_en" style="width: 100%"></div>
											<div id="editor_en_validation"></div>
										</div>
									</div>
									<br>
									<div class="input-group input-group-sm mt-3">
										<div class="input-group-prepend">
											<span class="input-group-text">order</span>
										</div>
										<input type="number" id="order" name="order" class="form-control" style="text-transform: capitalize;">
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button class="btn btn-primary" id="SubmitButton" type="submit">Save changes</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- MODAL -->
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script src="mainform/plugins/quill.min.js"></script>
	<script>
	var datatable = {};
	$(document).ready(function() {
		datatable = $('#main_table').DataTable({
			language: {
				"processing": "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
			},
			autoWidth: false,
			processing: true,
			searching: true,
			destroy: true,
			cache: true,
			contentType: "application/json; charset=utf-8",
			PaginationType: "full",
			responsive: true,
			ajax: {
				"url": url_local_web+"/APICall",
				"type": "POST",
				"headers": {
					'Content-Type': 'application/json'
				},
				"data": function () {
					var input_parameter = {};
					input_parameter.APIUrl = "/get-help";
					input_parameter.method = "GET";
					return JSON.stringify(input_parameter);
				},
				"dataSrc": function (json) {
					var output_array_list = [];
					console.log(json);
					$.each(json.output_schema.items, function (index, value) {
						var output_array = {};

						output_array.help_id = value.help_id;
						output_array.title = value.title;
						output_array.title_english = value.title_english;
						output_array.content = value.content;
						output_array.content_english = value.content_english;
						output_array.order = value.order;
						output_array_list.push(output_array);
					});
					
					return output_array_list;
				}
			},
			responsive: {
				breakpoints: [
					{ name: 'desktop',  width: Infinity },
					{ name: 'tablet-l', width: 1024 },
					{ name: 'tablet-p', width: 768 },
					{ name: 'mobile-l', width: 480 },
					{ name: 'mobile-p', width: 320 }
				]
			},
			order: [[ 5, "asc" ]],
			columns: [
				{"data": "help_id"},
				{"data": "title"},
				{"data": "title_english"},
				{"data": "content"},
				{"data": "content_english"},
				{"data": "order"},
				{
					"data": "help_id",
					"render": function (data, type, row) {
						var button_string = "";
						button_string += '<span class="input-group-btn">';
						button_string += '<button type="button" class="btn btn-success btn-sm button_detail" style="width: 35px;" data-id="'+data+'"><i class="fa fa-search-plus"></i></button>';
						button_string += '<button type="button" class="btn btn-info btn-sm button_update" style="width: 35px;" data-id="'+data+'"><i class="fa fa-edit"></i></button>';
						button_string += '<button type="button" class="btn btn-danger btn-sm button_delete" style="width: 35px;" data-id="'+data+'"><i class="fa fa-trash-o"></i></button>';
						button_string += '</span>';
						return button_string;
					}
				}
			],
			columnDefs: [
				{
					targets: [0, 1, 3, 5],
					searchable: true,
					sortable: true,
					visible: true,
					defaultContent: ""
				},
				{
					targets: [6],
					searchable: false,
					sortable: false,
					visible: true
				},
				{
					targets: [2, 4],
					searchable: false,
					sortable: false,
					visible: false
				}
// 				{
// 					targets: '_all',
// 					searchable: false,
// 					sortable: false,
// 					visible: false,
// 					defaultContent: ""
// 				}
			],
			preDrawCallback: function( settings ) {}
		});
		
		
	});
	
	var toolbarOptions = [
		['bold', 'italic', 'underline', 'strike'],
		['blockquote', 'code-block'], 
		[{'header': 1},{'header': 2}],
		[{'list': 'ordered'},{'list': 'bullet'}],
		[{'script': 'sub'},{'script': 'super'}],
		[{'indent': '-1'}, {'indent': '+1'}],
		[{'direction': 'rtl'}],
		[{'size': ['small', false, 'large', 'huge']}],
		[{'header': [1, 2, 3, 4, 5, 6, false]}],
		[{'color': []}, {'background': []}],
		[{'font': []}],
		[{'align': []}],
		['clean'],
		['link', 'image','video']
	];
	
	var quill_id = new Quill('#editor_id', {
		modules: {
			toolbar: toolbarOptions
		},
		theme: 'snow'
	});
	
	var quill_en = new Quill('#editor_en', {
		modules: {
			toolbar: toolbarOptions
		},
		theme: 'snow'
	});
	
	$('#main_table tbody').on('click', '.button_detail',function () {
		$('#SubmitButton').hide();
		
		var current_data = "";
		if ($(this).parent().hasClass("dtr-data")) {
			current_data = datatable.row($(this)).data();
		}
		else {
			current_data = datatable.row($(this).closest('tr')).data();
		}

		fillData(current_data);
		
		$('#addModal').modal({
			backdrop: 'static',
			keyboard: false, 
			show: true
		});
		
	});
	
	$('#main_table tbody').on('click', '.button_update',function () {
		$('#SubmitButton').show();
		validate_start_function('update');
		
		var current_data = "";
		if ($(this).parent().hasClass("dtr-data")) {
			current_data = datatable.row($(this)).data();
		}
		else {
			current_data = datatable.row($(this).closest('tr')).data();
		}
		
		$('#help_id').val(current_data.help_id);
		
		fillData(current_data);
		
		$('#addModal').modal({
			backdrop: 'static',
			keyboard: false, 
			show: true
		});
		
	});
	
	$('#main_table tbody').on('click', '.button_delete', function () {
		Swal.fire({
			title: 'Are you sure?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete'
		}).then((result) => {
			if (result.value) {
				var current_data = "";
				if ($(this).parent().hasClass("dtr-data")) {
					current_data = datatable.row($(this)).data();
				}
				else {
					current_data = datatable.row($(this).closest('tr')).data();
				}
				
				var input_parameter = {};
				input_parameter.APIUrl = "/delete-help?id="+current_data.help_id;
				input_parameter.method = "GET";
				
				$.ajax({
					url : url_local_web+"/APICall",
					type: 'POST',
					data: JSON.stringify(input_parameter),
					dataType: "json",
					headers: {
						'Content-Type': 'application/json'
					},
					beforeSend: function() {
					},
				}).done(function (data) {
					console.log(data);
					if(data.error_schema.error_code == "ERR-00-000"){						
						Swal.fire({
							allowOutsideClick: false,
							type: 'success',
							title: 'Alert',
							text: "Delete success"
						}).then((result) => {
							if (result.value) {
								location.reload();
							}
						});
					}
					else{
						Swal.fire({
							type: 'error',
							title: 'Alert',
							text: "Delete failed"
						});
					}
					datatable.ajax.reload();
				}).fail(function (jqXHR, textStatus) {
					console.log("failed " + jqXHR + " " + textStatus);
					Swal.fire({
						type: 'error',
						title: 'Alert',
						text: "Delete error"
					});
				});
				
			}
		});
	
	
	
	
		
		
	});
	
	function fillData(current_data){
		$('#title_id').val(current_data.title);
		$('#title_en').val(current_data.title_english);
		quill_id.root.innerHTML = current_data.content;
		quill_en.root.innerHTML = current_data.content_english;
		$('#order').val(current_data.order);
	}
	
	$('#addProvider').on('hide.bs.modal', function () {
		reset_form();
	});
	
	$('#SubmitNew').on('click', function () {
		reset_form();
		validate_start_function('new');
	});
	
	function reset_form(){
		$('#SubmitButton').show();
		quill_id.root.innerHTML = "";
		quill_en.root.innerHTML = "";
		$("#FormSubmit")[0].reset();
		$form = $('form');
		$form.find('.control-group').removeClass('.error');
		$form.find('.has-error').removeClass("has-error");
		$form.find('.has-success').removeClass("has-success");
		$form.find('.form-control-feedback').remove();
	}
	
	function validate_start_function(SubmitType){
		$.validator.addMethod("customLetterValidation", function (value, element) {
			return this.optional(element) || /^[a-zA-Z0-9 ]+$/i.test(value);
		}, "Kolom ini hanya boleh diisi alfanumerik dan spasi.");
		
		var validate_start = $("#FormSubmit").validate({
			ignore: [],
			rules: {
				title_id: {
					required: true
				},
				title_en: {
					required: true
				},
				order: {
					required: true
				}
			},
			messages: {},
			highlight: function (element, errorClass) {
				$(element).closest(".form-group").addClass("has-error").css("color", "");
			},
			unhighlight: function (element, errorClass) {
				$(element).closest(".form-group").removeClass("has-error").css("color", "");
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function (error, element) {
				error.css({
					'font-size': '10px',
					'display': 'inline',
					'color': 'red'
				});
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				}
				else {
					error.insertAfter(element);
				}
			},
			submitHandler: function (form) {
				console.log("A");
				var input_parameter = {};
				if(SubmitType == 'update' || (typeof SubmitType != 'undefined' && SubmitType != null) ){
					input_parameter.help_id = $('#help_id').val();
				}
				
				input_parameter.title = $('#title_id').val();
				input_parameter.title_english = $('#title_en').val();
				input_parameter.content = quill_id.root.innerHTML;
				input_parameter.content_english = quill_en.root.innerHTML;
				input_parameter.order = $('#order').val();
				
				input_parameter.APIUrl = "/insert-update-help";
				input_parameter.method = "POST";
				console.log(SubmitType);
				
				var message_type_success = "";
				var message_type_failed = "";
				if(SubmitType == 'new'){
					message_type_success = "Insert data berhasil.";
					message_type_failed = "Insert data gagal.";
				}
				else{
					message_type_success = "Update data berhasil.";
					message_type_failed = "Update data gagal.";
				}
				
				$.ajax({
					url : url_local_web+"/APICall",
					type: 'POST',
					dataType: 'JSON',
					data: JSON.stringify(input_parameter),
					headers: {
						"Content-Type": "application/json"
					},
					beforeSend: function() {},
				}).done(function (data) {
					console.log("CA");
					reset_form();
					$('#addModal').modal('hide');
					
					if(data.error_schema.error_code == "ERR-00-000"){
						Swal.fire({
							allowOutsideClick: false,
							type: 'success',
							title: 'Alert',
							text: message_type_success
						}).then((result) => {
							if (result.value) {
								location.reload();
							}
						});
					}
					else{
						Swal.fire({
							type: 'error',
							title: 'Alert',
							text: message_type_failed
						});
					}
					
					//console.log(data)
					datatable.ajax.reload();
				}).fail(function (jqXHR, textStatus) {
					console.log("CB");
					reset_form();
					console.log("failed " + jqXHR + " " + textStatus);
					datatable.ajax.reload();
				});
			},
			invalidHandler: function (event, validator) {
				var errors = validator.numberOfInvalids();
				console.log("error " + errors);
			}
		});
		jQuery.extend(jQuery.validator.messages, {
			number: "Please enter only number."
		});
		validate_start.resetForm();
		console.log("E");
	}
	
	</script>
</body>
</html>
