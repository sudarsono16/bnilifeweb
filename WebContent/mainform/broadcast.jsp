<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>
<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="mainform/plugins/datatables/extensions/Responsive/css/responsive.bootstrap4.min.css">
<link href="mainform/css/quill.snow.css" rel="stylesheet">
<style>
table.dataTable tbody td {
	word-break: break-word;
	vertical-align: top;
}
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
		<div class="container-fluid">
			<div class="animated fadeIn">
				<div class="search-container row">
					<div class="col-lg-3 col-md-3 col-12">
						<button class="btn btn-block btn-warning" id="SubmitNew" type="button" data-toggle="modal" data-target="#addModal">
							<i class="fa fa-plus-circle"></i> Add Broadcast Message
						</button>
					</div>
				</div>
				<!-- SEARCH-CONTAINER -->
			</div>
		</div>
		<div class="container-fluid">
			<div class="animated fadeIn">
				<table class="table table-striped table-bordered sortable" id="main_table">
					<thead>
						<tr>
							<th>Broadcast ID</th>
							<th>Broadcast Type</th>
							<th>Username</th>
							<th>Broadcast Title</th>
							<th>Broadcast Content</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
		</main>
		<!-- Edit Information Modal -->
		<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addProviderLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<form id='FormSubmit'>
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="addDocLabel">Create New Broadcast</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<input type="hidden" id="submit_type">
							<div class="card-body">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Broadcast Type</span>
									</div>
									<select class="custom-select form-control" name="broadcast_type" id="broadcast_type"></select>
								</div>
								<br>
								<div id="hide_username">
									<div class="input-group" >
										<div class="input-group-prepend">
											<span class="input-group-text">Username</span>
										</div>
										<select class="custom-select form-control" name="username" id="username"></select>
									</div>
									<br>
								</div>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Broadcast Title</span>
									</div>
									<input type="text" class="form-control" name="broadcast_title" id="broadcast_title" placeholder="Broadcast Title">
								</div>
								<br>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Broadcast Content</span>
									</div>
									<input type="text" class="form-control" name="broadcast_content" id="broadcast_content" placeholder="Broadcast Content">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<button type="submit" class="btn btn-primary" id="save_news">Send Broadcast</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- MODAL -->
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script>
	
	var datatable = {};
	$(document).ready(function() {
		datatable = $('#main_table').DataTable({
			language: {
				"processing": "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
			},
			autoWidth: false,
			processing: true,
			searching: true,
			destroy: true,
			cache: true,
			contentType: "application/json; charset=utf-8",
			PaginationType: "full",
			responsive: true,
			ajax: {
				"url": url_local_web+"/APICall",
				"type": "POST",
				"headers": {
					'Content-Type': 'application/json'
				},
				"data": function () {
					var input_parameter = {};
					input_parameter.APIUrl = "/message/get-broadcast-list";
					input_parameter.method = "GET";
					return JSON.stringify(input_parameter);
				},
				"dataSrc": function (json) {
					var output_array_list = [];
					console.log(json);
					$.each(json.output_schema.items, function (index, value) {
						var output_array = {};

						output_array.broadcast_id = value.broadcast_id;
						output_array.broadcast_type = value.broadcast_type;
						output_array.username = value.username;
						output_array.broadcast_title = value.broadcast_title;
						output_array.broadcast_content = value.broadcast_content;

						output_array_list.push(output_array);
					});
					
					return output_array_list;
				}
			},
			responsive: {
				breakpoints: [
					{ name: 'desktop',  width: Infinity },
					{ name: 'tablet-l', width: 1024 },
					{ name: 'tablet-p', width: 768 },
					{ name: 'mobile-l', width: 480 },
					{ name: 'mobile-p', width: 320 }
				]
			},
			columns: [
				{"data": "broadcast_id"}, // 0
				{"data": "broadcast_type"}, //1
				{"data": "username"},//2
				{"data": "broadcast_title"},//3
				{"data": "broadcast_content"},//4
			],
			columnDefs: [
				{
					targets: [0, 1, 2, 3, 4],
					searchable: false,
					sortable: false,
					visible: true,
 					defaultContent: ""
				}
				
			],
			preDrawCallback: function( settings ) {}
		});
		
		$('#broadcast_type').on('change', function () {
			if($(this).val() == "PERSONAL"){
				$('#hide_username').show();
				populate_select_username();
			}
			else{
				$('#username').removeAttr('selected').find('[value=""]').attr('selected','selected').prop("selected",true);
				$('#hide_username').hide();
			}
	        // reloadClericPositionIDDropdown(SubmitType, current_data.ClericPositionID);
	    });
		
	});
	
	$('#addModal').on('hide.bs.modal', function () {
		reset_form();
	});
	
	$('#SubmitNew').on('click', function () {
		reset_form();
		validate_start_function('new');
	});
	
	function reset_form(){
		$("#FormSubmit")[0].reset();
		$('#broadcast_id').val('');
		populate_select_broadcast_type();
		$('#hide_username').hide();
		
		$form = $('form');
		$form.find('.control-group').removeClass('.error');
		$form.find('.has-error').removeClass("has-error");
		$form.find('.has-success').removeClass("has-success");
		$form.find('.form-control-feedback').remove();
	}
	
	function populate_select_broadcast_type(broadcast_type){
		$('#broadcast_type').removeAttr('selected').find('[value=""]').attr('selected','selected').prop("selected",true);
		if(!$("#broadcast_type").find("option:selected").length){
			var types = [];
			types.push({"ID":"","Text":"Pilih Tipe Berita"});
			types.push({"ID":"ALL","Text":"ALL"});
			types.push({"ID":"OGH","Text":"OGH"});
			types.push({"ID":"OGS","Text":"OGS"});
			types.push({"ID":"PERSONAL","Text":"PERSONAL"});
			
			$.each(types, function (key, value) {
				$('#broadcast_type').append($('<option>', {
					value: value.ID,
					text: value.Text
				}));
			});
		}
		if(typeof broadcast_type != 'undefined' && broadcast_type != null){
			$('#broadcast_type').removeAttr('selected').find('[value="'+broadcast_type+'"]').attr('selected','selected').prop("selected",true);
		}
	}
	
	function populate_select_username(username){
		var input_parameter = {};
		input_parameter.APIUrl = "/user/get-username-list";
		input_parameter.method = "GET";
		
		$.ajax({
			url : url_local_web+"/APICall",
			type: 'POST',
			data: JSON.stringify(input_parameter),
			dataType: "json",
			headers: {
				'Content-Type': 'application/json'
			},
			beforeSend: function() {
			},
		}).done(function (data) {
			console.log(data);
			$('#username').append($('<option>', {
				value: '',
				text: 'Select username'
			}))
			$.each(data.output_schema, function (key, value) {
				$('#username').append($('<option>', {
					value: value,
					text: value
				}));
			});
		}).fail(function (jqXHR, textStatus) {
			console.log("failed " + jqXHR + " " + textStatus);
		});
	}
	
	function validate_start_function(SubmitType){
		$.validator.addMethod("customLetterValidation", function (value, element) {
			return this.optional(element) || /^[a-zA-Z0-9 ]+$/i.test(value);
		}, "Kolom ini hanya boleh diisi alfanumerik dan spasi.");
		
		var validate_start = $("#FormSubmit").validate({
			ignore: [],
			rules: {
				broadcast_type: {
					required: true
				},
				username: {
					required: false
				},
				broadcast_title: {
					required: true,
					maxlength: 65
				},
				broadcast_content: {
					required: true,
					maxlength: 255
				}
			},
			messages: {},
			highlight: function (element, errorClass) {
				$(element).closest(".form-group").addClass("has-error").css("color", "");
			},
			unhighlight: function (element, errorClass) {
				$(element).closest(".form-group").removeClass("has-error").css("color", "");
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function (error, element) {
				error.css({
					'font-size': '10px',
					'display': 'inline',
					'color': 'red'
				});
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				}
				else {
					error.insertAfter(element);
				}
			},
			submitHandler: function (form) {
				console.log("A");
				var input_parameter = {};
				if(SubmitType == 'update' || (typeof SubmitType != 'undefined' && SubmitType != null) ){
					input_parameter.product_id = $('#product_id').val();
				}
				
				input_parameter.broadcast_type = $('#broadcast_type').val();
				input_parameter.username = $('#username').val();
				input_parameter.broadcast_title = $('#broadcast_title').val();
				input_parameter.broadcast_content = $('#broadcast_content').val();
				
				input_parameter.APIUrl = "/message/send-broadcast-message";
				input_parameter.method = "POST";
				
				var message_type_success = "Pengiriman broadcast berhasil";
				var message_type_failed = "Pengiriman broadcast gagal";
				
				$.ajax({
						url : url_local_web+"/APICall",
						type: 'POST',
						dataType: 'JSON',
						data: JSON.stringify(input_parameter),
						headers: {
							"Content-Type": "application/json"
						},
						beforeSend: function() {
						},
					}).done(function (data) {
					reset_form();
					$('#addModal').modal('hide');
					
					if(data.error_schema.error_code == "ERR-00-000"){
						Swal.fire({
							allowOutsideClick: false,
							type: 'success',
							title: 'Alert',
							text: message_type_success
						}).then((result) => {
							if (result.value) {
								reset_form();
								location.reload();
							}
						});
					}
					else{
						Swal.fire({
							type: 'error',
							title: 'Alert',
							text: message_type_failed
						});
					}
					datatable.ajax.reload();
				}).fail(function (jqXHR, textStatus) {
					reset_form();
					console.log("failed " + jqXHR + " " + textStatus);
					datatable.ajax.reload();
				});
			},
			invalidHandler: function (event, validator) {
				var errors = validator.numberOfInvalids();
				console.log("error " + errors);
			}
		});
		jQuery.extend(jQuery.validator.messages, {
			number: "Please enter only number."
		});
		validate_start.resetForm();
		console.log("E");
	}
	
	</script>
</body>
</html>
