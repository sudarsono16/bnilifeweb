<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file='head.jsp'%>

<link rel="stylesheet" href="mainform/plugins/datatables/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="mainform/plugins/datatables/extensions/Responsive/css/responsive.bootstrap4.min.css">

<style>
table.dataTable tbody td {
	word-break: break-word;
	vertical-align: top;
}
</style>

</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
	<%@ include file='header.jsp'%>
	<div class="app-body">
		<%@ include file='sidebar.jsp'%>
		<main class="main">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<table class="table table-striped table-bordered sortable" id="main_table">
						<thead>
							<tr>
								<th>Username</th>
								<th>Email</th>
								<th>PhoneNumber</th>
								<th>DOB</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</main>
	</div>
	<%@ include file='footer.jsp'%>
	<%@ include file='scripts.jsp'%>
	<script>
	var datatable = {};
	$(document).ready(function() {
		datatable = $('#main_table').DataTable({
			language: {
				"processing": "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
			},
			autoWidth: false,
			processing: true,
			searching: true,
			searchDelay: 500,
			destroy: true,
			cache: true,
			contentType: "application/json; charset=utf-8",
			PaginationType: "full",
			responsive: true,
			serverSide: true,
			ajax: {
				"url": url_local_web+"/APICall",
				"type": "POST",
				"headers": {
					'Content-Type': 'application/json'
				},
				"data": function (param) {
					var pageNumber = $('#main_table').DataTable().page.info().page + 1;
					var pageSize = $('#main_table').DataTable().page.info().length;
					var input_parameter = {};
					var searchstring = "";
					if (param.search.value.trim()) {
						searchstring = '&search='+encodeURIComponent(param.search.value);
					}
					
					input_parameter.APIUrl = "/user/get-mobile-user-list?pageNumber="+pageNumber+"&pageSize="+pageSize+'&draw='+param.draw+searchstring;
					input_parameter.method = "GET";
					return JSON.stringify(input_parameter);
				},
				"dataSrc": "output_schema.data"
			},
			responsive: {
				breakpoints: [
					{ name: 'desktop',  width: Infinity },
					{ name: 'tablet-l', width: 1024 },
					{ name: 'tablet-p', width: 768 },
					{ name: 'mobile-l', width: 480 },
					{ name: 'mobile-p', width: 320 }
				]
			},
			columns: [
				{"data": "username"},
				{"data": "email"},
				{"data": "phone_number"},
				{"data": "date_of_birth"},
				{
					"data": "is_active",
					"render": function (data, type, row) {
						if(data == '1'){
							return 'Active';
						}
						else{
							return 'Inactive';
						}
					}
				},
				{
					"data": {username : "username"},
					"render": function (data, type, row) {
						if(data.is_active == '1'){
							return '<span class="input-group-btn"> <button type="button" class="btn btn-danger btn-xs data_status_edit" data-username="'+data.username+'" data-is_active="0" ><i class="fa fa-ban "></i></button>';
						}
						else{
							return '<span class="input-group-btn"> <button type="button" class="btn btn-success btn-xs data_status_edit" data-username="'+data.username+'" data-is_active="1" ><i class="fa fa-check "></i></button>';
						}
					}
				}
			],
			columnDefs: [
				{
					targets: [0, 1, 2, 3, 4],
					searchable: true,
					sortable: false,
					visible: true,
					defaultContent: ""
				},
				{
					targets: [5],
					searchable: false,
					sortable: false,
					visible: true,
					defaultContent: ""
				}
			]
		}).on('xhr.dt', function (e, settings, json, xhr) {
			json.recordsTotal = json.output_schema.recordsTotal;
		    json.recordsFiltered = json.output_schema.recordsFiltered;
		    json.draw = json.output_schema.draw;
		    json.data = json.output_schema.data;
		});
		
		$('#main_table tbody').on('click', '.data_status_edit', function (event) {
			var username = $(this).data("username");
			var is_active = $(this).data("is_active");
			
			var input_parameter = {};
			input_parameter.APIUrl = "/user/update-user-active";
			input_parameter.method = "POST";
			input_parameter.username = username;
			input_parameter.is_active = is_active;
			
			console.log(input_parameter);
			
			$.ajax({
				url : url_local_web+"/APICall",
				type: 'POST',
				dataType: 'JSON',
				data: JSON.stringify(input_parameter),
				headers: {
					"Content-Type": "application/json"
				},
				beforeSend: function() {},
			}).done(function (data) {
				console.log(data);
				datatable.ajax.reload();
			}).fail(function (jqXHR, textStatus) {
				console.log("failed " + jqXHR + " " + textStatus);
			});
			
		});
		
	});
	</script>
</body>
</html>
