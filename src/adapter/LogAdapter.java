package adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import database.QueryExecuteAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LogAdapter {
	public static void InsertLogExc(String lException,String lKey,String lQuery, String lCreated_by){
		List<model.Query.mdlQueryExecute> listParam = new ArrayList<model.Query.mdlQueryExecute>();
		String Query = "INSERT INTO `log_exception`(`Exception`, `Query`, `Key`, `Created_on`, `Created_by`) VALUES(?, ?, ?, ?, ?);";
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date Created_On = new Date();
		String lCreated_On = df.format(Created_On);
		try{
			if(lCreated_by == null || "".equals(lCreated_by)) {
				lCreated_by = "EMPTY";
			}
			
			listParam.add(QueryExecuteAdapter.QueryParam("string", lException));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lQuery));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lKey));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lCreated_On));
			listParam.add(QueryExecuteAdapter.QueryParam("string", lCreated_by));

			QueryExecuteAdapter.QueryManipulate(Query, listParam, "InsertLogExc");
		}
		catch(Exception ex){
			System.out.println(ex);
		}
	}
	
}
