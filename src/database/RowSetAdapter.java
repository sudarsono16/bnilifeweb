package database;

import java.sql.Connection;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class RowSetAdapter {
	private static Connection conn;
	public static Connection getConnection()throws Exception{
		try {
			InitialContext context = new InitialContext();
			DataSource dataSource = (DataSource) context.lookup("java:/comp/env/jdbc/bnilife");
			conn = dataSource.getConnection();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}